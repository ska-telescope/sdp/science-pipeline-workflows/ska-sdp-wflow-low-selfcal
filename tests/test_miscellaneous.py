""" Tests for functions in miscellanous module """

import os

import numpy as np
import pytest
from casacore.tables import taql

from ska_sdp_wflow_selfcal.pipeline.support.miscellaneous import (
    concatenate_ms,
    normalize_ra_dec,
)


@pytest.mark.parametrize("delete_input", {False, True})
def test_concatenate_ms(delete_input, create_environment):
    """Test concatenate_ms function"""

    single_ms = "tNDPPP-generic.MS"
    times_single = taql(f"select TIME from {single_ms}").getcol("TIME")

    concat_ms = "output.ms"
    concatenate_ms(
        [single_ms, single_ms, single_ms], concat_ms, delete_input=delete_input
    )
    times_concat = taql(f"select TIME from {concat_ms}").getcol("TIME")

    assert (times_concat == 3 * list(times_single)).all()

    assert os.path.exists(single_ms) == (not delete_input)


def ra_dec_to_vector(ra, dec):
    """
    Convert a ra, dec direction to a unit-length 3D-vector
    ra, dec are given in degrees
    """
    ra_rad = np.deg2rad(ra)
    dec_rad = np.deg2rad(dec)
    return np.array(
        (
            np.cos(ra_rad) * np.cos(dec_rad),
            np.sin(ra_rad) * np.cos(dec_rad),
            np.sin(dec_rad),
        )
    )


def test_normalize_ra_dec():
    """
    Test the functions for normalization of ra and dec
    """
    directions = ((10, 40), (400, -10), (-100, 80), (300, 100))
    for ra, dec in directions:
        v_reference = ra_dec_to_vector(ra, dec)
        ra_normalized, dec_normalized = normalize_ra_dec(ra, dec)

        # Check that the normalized coordinates are within the standard range
        assert (0.0 <= ra_normalized < 360) and (-90 <= dec_normalized <= 90)

        # Check that the normalization leaves the direction unchanged
        v_normalized = ra_dec_to_vector(ra_normalized, dec_normalized)
        assert np.allclose(v_reference, v_normalized, rtol=0.0, atol=1e-5)
