#  SPDX-License-Identifier: BSD-3-Clause

"""Tests for the `ChainRunner` class."""

import pytest

from ska_sdp_wflow_selfcal.pipeline.chain_runner import ChainRunner
from ska_sdp_wflow_selfcal.pipeline.dp3_helper import Dp3Runner
from ska_sdp_wflow_selfcal.pipeline.wsclean_helper import WSCleanRunner


@pytest.fixture
def chain_runner_instance(mock_settings):
    """Generates an instace of `ChainRunner` for testing purposes."""
    # The Runner classes need a settings object with a single input MS.
    single_input_settings = mock_settings
    single_input_settings.input_ms = mock_settings.input_ms[0]

    return ChainRunner(
        1,
        dp3_runner=Dp3Runner(single_input_settings),
        wsclean_runner=WSCleanRunner(single_input_settings),
        work_dir=mock_settings.work_dir,
        settings=mock_settings,
    )


def test_dummy(chain_runner_instance):
    """Dummy test to be further developed."""
    assert isinstance(chain_runner_instance, ChainRunner)
