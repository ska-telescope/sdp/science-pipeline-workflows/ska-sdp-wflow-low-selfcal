""" Tests for functions in main module """

import argparse
import dataclasses
import logging

import numpy as np
import pytest

from ska_sdp_wflow_selfcal.pipeline.main import (
    SplitAction,
    file_exists,
    get_filenames,
    get_run_matrix,
)


@dataclasses.dataclass
class MockSkipSettings:
    """Holds all settings that determine which operations should run."""

    configuration = {
        "selfcal_cycles": [
            {"calibrate": "foo", "predict": "foo", "image": "foo"},
            {"calibrate": "foo", "image": "foo"},
            {"calibrate": "foo", "predict": "foo", "image": "foo"},
            {"calibrate": "foo", "predict": "foo", "image": "foo"},
        ]
    }
    resume_from_operation: str
    run_single_operation: str  # "True" or "False"

    def __init__(self, resume_from, run_single):
        self.resume_from_operation = resume_from
        self.run_single_operation = "True" if run_single else "False"


@pytest.mark.parametrize(
    "operation",
    [  # resume_from, loop_index, operation_index, expected_true_count
        ["calibrate_1", 0, 0, 11],
        ["predict_1", 0, 1, 10],
        ["image_1", 0, 2, 9],
        ["calibrate_2", 1, 0, 8],
        ["image_2", 1, 2, 7],
        ["predict_3", 2, 1, 5],
        ["image_4", 3, 2, 1],
    ],
)
def test_get_run_matrix(operation):
    """Test that get_run_matrix yields the correct matrix."""

    # Test running a single operation.
    settings = MockSkipSettings(operation[0], True)
    run_matrix = get_run_matrix(settings)
    assert run_matrix[operation[1], operation[2]]
    assert np.count_nonzero(run_matrix) == 1

    # Test running resuming all operations from a given start.
    settings = MockSkipSettings(operation[0], False)
    run_matrix = get_run_matrix(settings)
    # Predict_2 is always False, since it's missing from the configuration.
    assert not run_matrix[1, 1]
    if operation[1] > 0:
        assert not run_matrix[: operation[1], :].any()
    if operation[2] > 0:
        assert not run_matrix[operation[1], : operation[2] - 1].any()
    assert np.count_nonzero(run_matrix) == operation[3]


@pytest.mark.parametrize(
    # predict_2 is invalid since the configuration does not contain it.
    "operation",
    ["invalid", "image_0", "calibrate_8", "predict_2"],
)
def test_get_run_matrix_invalid_operation(operation):
    """Test that get_run_matrix rejects invalid resume operations."""
    settings = MockSkipSettings(operation, False)
    with pytest.raises(ValueError):
        get_run_matrix(settings)


def test_file_exists(tmp_path):
    """Test that the file_exists works correclty"""

    logging.FileHandler(f"{tmp_path}/file_not_found.log")
    test_filename = f"{tmp_path}/dummy.txt"
    assert not file_exists(test_filename, "FILE NOT FOUND")

    with open(f"{tmp_path}/file_not_found.log", "r", encoding="utf-8") as log:
        for line in log.readlines():
            assert "FILE NOT FOUND" in line

    logging.FileHandler(f"{tmp_path}/file_exists.log")
    with open(test_filename, "w", encoding="utf-8") as test_file:
        test_file.write("I am a test file")

    assert file_exists(test_filename, "FILE EXISTS")
    with open(f"{tmp_path}/file_exists.log", "r", encoding="utf-8") as log:
        for line in log.readlines():
            assert "FILE EXISTS" in line


def test_get_filenames(tmp_path, mock_settings):
    """
    Test the get_filenames function.
    """

    test_suffix = "test"
    ms_list = get_filenames(
        tmp_path,
        mock_settings.input_ms[0],
        1.0,
        1.0,
        test_suffix,
    )

    assert ms_list == [
        f"{mock_settings.input_ms[0]}.mjd5020571443.{test_suffix}",
        f"{mock_settings.input_ms[0]}.mjd5020571451.{test_suffix}",
        f"{mock_settings.input_ms[0]}.mjd5020571459.{test_suffix}",
        f"{mock_settings.input_ms[0]}.mjd5020571468.{test_suffix}",
    ]


def test_split_action():
    """Test the SplitAction class for parsing  arguments with spaces."""
    parser = argparse.ArgumentParser(description="TestSplitAction")
    parser.add_argument("--without_spaces", type=str, action=SplitAction)
    parser.add_argument("--with_spaces", type=str, action=SplitAction)
    parsed = parser.parse_args(
        ["--without_spaces", "nospaces", "--with_spaces", "has some spaces"]
    )
    assert parsed.without_spaces == ["nospaces"]
    assert parsed.with_spaces == ["has", "some", "spaces"]
