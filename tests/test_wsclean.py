#  SPDX-License-Identifier: BSD-3-Clause

"""
Tests for the WSCleanRunner class. This file tests that the functions in the
WSCleanRunner class execute successfully and generate the expected outputs.
"""

import re
import shutil

import pytest


@pytest.mark.skip(reason="See comments inside test.")
def test_wsclean(
    wsclean_runner,
):
    """Test wsclean imaging"""

    # TODO-AST-1397: this test does not pass because wsclean returns with the
    # following message: 'Number of source directions in one of the h5 facet
    # solution files does not match the number of facets in the facet
    # definition file.'

    # 20240724: WSCleanRunner.run() also has more arguments now.

    wsclean_runner.run(
        wsclean_runner.settings.input_ms,
        "facets.reg",
        "fast_phase_0.h5parm",
        "mask.fits",
        "tmp_dir",
    )


def test_wsclean_restore(wsclean_runner, create_environment_support_functions):
    """Test wsclean restore"""
    shutil.copy("test_image.fits", "test_image-pb.fits")
    wsclean_runner.restore(
        "test_image",
        "empty.skymodel",
    )

    # The first line of the log file should contain a wsclean command for
    # test_image.fits. The log file should also contain a wsclean command for
    # test_image-pb.fits.
    with open("wsclean.tag.log", encoding="utf-8") as logfile:
        first_command = logfile.readline()
        assert re.match(
            r"^Running: .*/wsclean -restore-list test_image\.fits "
            + r"empty\.skymodel test_image\.fits$",
            first_command,
        )

        log_remainder = logfile.read()
        assert re.search(
            r"^Running: .*/wsclean -restore-list test_image-pb\.fits "
            + r"empty\.skymodel test_image-pb\.fits$",
            log_remainder,
            re.MULTILINE,
        )


def test_add_ms_based_options_default(wsclean_runner):
    """Test adding default options, based on the input MS."""
    wsclean_runner.ms_channels_out = 6
    configuration_file_arguments = []
    wsclean_config = {}

    wsclean_runner.add_ms_based_options(
        configuration_file_arguments, wsclean_config
    )

    assert configuration_file_arguments == [
        "-channels-out",
        "6",
        "-deconvolution-channels",
        "4",
        "-fit-spectral-pol",
        "3",
    ]


def test_add_ms_based_options_with_config(wsclean_runner):
    """Test add_ms_based_options, when all options are in the configuration."""
    wsclean_runner.ms_channels_out = 6
    configuration_file_arguments = []
    wsclean_config = {
        "channels-out": 10,
        "deconvolution-channels": 5,
        "fit-spectral-pol": 2,
    }

    wsclean_runner.add_ms_based_options(
        configuration_file_arguments, wsclean_config
    )

    # Add_ms_settings shouldn't add any settings.
    assert not configuration_file_arguments
