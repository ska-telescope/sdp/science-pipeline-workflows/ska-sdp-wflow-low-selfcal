#  SPDX-License-Identifier: BSD-3-Clause

"""Tests for the functions in read_data.py."""

import casacore.tables as pt
import numpy as np
import pytest

from ska_sdp_wflow_selfcal.pipeline.support.read_data import (
    calculate_n_chunks,
    calculate_n_times,
    compute_observation_width,
    get_imaging_channels_out,
    get_imaging_n_iterations,
    get_observation_params,
    get_total_bandwidth,
    get_total_time,
)


def test_read_observation_data(
    create_environment_support_functions,
):
    """Test that the observation data is read correctly from a
    measurement set"""

    ms_path = create_environment_support_functions["ms_path"]

    # Get RA/Dec pointing of measurement set under testing.
    observation = get_observation_params(ms_path)

    assert np.isclose(observation["mean_el_rad"], 1.47022)
    assert np.isclose(observation["ref_freq"], 143650817.871)
    assert np.isclose(observation["diam"], 31.26253)


def test_get_imaging_channels_out(
    create_environment_support_functions,
):
    """Test the get_imaging_channels_out function."""
    ms_path = create_environment_support_functions["ms_path"]
    assert get_imaging_channels_out(ms_path) == 1

    # Increasing the number of channels should increase the result.
    pt.taql(f"update {ms_path}::SPECTRAL_WINDOW set NUM_CHAN=420")
    assert get_imaging_channels_out(ms_path) == 9


def test_get_imaging_n_iterations(create_environment_support_functions):
    """Test that the parameter nmiter for WSClean is calculated correctly"""
    max_nmiter = 10
    peel_bright_sources = False

    ms_path = create_environment_support_functions["ms_path"]
    nmiter = get_imaging_n_iterations(
        [ms_path], max_nmiter, peel_bright_sources
    )
    assert nmiter == 2

    ms_path = create_environment_support_functions["ms_path"]

    # Assert that when the value computed for nmiter is higher than the
    # max_nmiter, the value chosen is max_nmiter
    max_nmiter = 1
    nmiter = get_imaging_n_iterations(
        [ms_path], max_nmiter, peel_bright_sources
    )
    assert nmiter == max_nmiter


def test_get_total_bandwidth(create_environment_support_functions):
    """Test that the total bandwidth is read correctly from a
    measurement set"""

    ms_path = create_environment_support_functions["ms_path"]
    total_bandwidth, n_channels = get_total_bandwidth(ms_path)
    assert total_bandwidth == 97656.25
    assert n_channels == 1


def test_get_total_time(create_environment_support_functions):
    """Test that the total time is read correctly from a list of
    measurement sets"""

    ms_path = create_environment_support_functions["ms_path"]

    expected_total_time = 32.044464
    total_time = get_total_time([ms_path])
    assert np.isclose(total_time, expected_total_time)

    total_time = get_total_time([ms_path, ms_path, ms_path])
    assert np.isclose(total_time, 3 * expected_total_time)


def test_compute_observation_width():
    """Regression test for compute_observation_width"""
    observation = {
        "mean_el_rad": np.pi / 2.0,  # sin(np.pi / 2) is 1.0
        "ref_freq": 84e6,
        "diam": 42.0,
    }
    expected_ra = 9.10451
    width = compute_observation_width(observation)
    assert np.isclose(width["ra"], expected_ra)
    assert np.isclose(width["dec"], expected_ra)

    # An elevation of 30 degrees affects dec only. Since the sine of the
    # elevation is 0.5, the "dec" value should be twice the "ra" value.
    observation["mean_el_rad"] = np.pi / 6.0
    width = compute_observation_width(observation)
    assert np.isclose(width["ra"], expected_ra)
    assert np.isclose(width["dec"], expected_ra * 2.0)

    # An elevation of 0 degrees is invalid (division by zero).
    observation["mean_el_rad"] = 0.0
    with pytest.raises(ZeroDivisionError):
        compute_observation_width(observation)


def test_calculate_n_chunks(create_environment_support_functions):
    """Test function calculate_n_chunks"""
    ms_path = create_environment_support_functions["ms_path"]

    # The test ms contains 32.04 seconds of data
    assert calculate_n_chunks(ms_path, 1.0, 32.0) == 1
    assert calculate_n_chunks(ms_path, 1.0, 16.0) == 2
    assert calculate_n_chunks(ms_path, 1.0, 4.0) == 4
    assert calculate_n_chunks(ms_path, 0.6, 4.0) == 2
    assert calculate_n_chunks(ms_path, 0.4, 4.0) == 1


def test_calculate_n_times(create_environment_support_functions):
    """Test function calculate_n_times"""
    ms_path = create_environment_support_functions["ms_path"]

    # The test ms contains 4 intervals of 8 seconds each -> 32 seconds
    assert calculate_n_times(ms_path, 1) == 1
    assert calculate_n_times(ms_path, 8) == 1
    assert calculate_n_times(ms_path, 9) == 2
    assert calculate_n_times(ms_path, 31) == 4
    assert calculate_n_times(ms_path, 32) == 4
