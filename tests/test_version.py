"""Test the version checks in version.py."""

import os

from ska_sdp_wflow_selfcal.pipeline.version import (
    EXPECTED_DP3_VERSION,
    EXPECTED_WSCLEAN_VERSION,
    check_dp3_version,
    check_wsclean_version,
)


def test_check_dp3_version(tmp_path):
    """Test checking DP3 version."""

    dummy_exe = tmp_path / "DP3dummy"

    # Run a dummy executable that correctly outputs the version.
    with open(dummy_exe, "w", encoding="UTF-8") as dummy:
        dummy.write("#!/bin/sh\n")
        dummy.write(f"echo DP3 {EXPECTED_DP3_VERSION}\n")
    os.chmod(dummy_exe, 0o755)
    assert check_dp3_version(dummy_exe)

    # Run a dummy executable that outputs a different version.
    with open(dummy_exe, "w", encoding="UTF-8") as dummy:
        dummy.write("#!/bin/sh\n")
        dummy.write("echo DP3 42.42.42\n")
    os.chmod(dummy_exe, 0o755)
    assert not check_dp3_version(dummy_exe)

    # Run a dummy executable that outputs nothing.
    with open(dummy_exe, "w", encoding="UTF-8") as dummy:
        dummy.write("#!/bin/sh\n")
    os.chmod(dummy_exe, 0o755)
    assert not check_dp3_version(dummy_exe)


def test_check_wsclean_version(tmp_path):
    """Test checking WSClean version."""

    dummy_exe = tmp_path / "WSCleandummy"

    # Run a dummy executable that correctly outputs the version.
    with open(dummy_exe, "w", encoding="UTF-8") as dummy:
        dummy.writelines(
            [
                "#!/bin/sh\n",
                "echo\n",
                'echo "WSClean version '
                + f'{EXPECTED_WSCLEAN_VERSION} (2025-01-10)"\n',
                "echo Dummy git commit hash line\n",
                "echo Dummy license line\n",
                "echo Dummy author line\n",
            ]
        )
    os.chmod(dummy_exe, 0o755)
    assert check_wsclean_version(dummy_exe)

    # Run a dummy executable that outputs a different version.
    with open(dummy_exe, "w", encoding="UTF-8") as dummy:
        dummy.writelines(
            [
                "#!/bin/sh\n",
                "echo\n",
                'echo "WSClean version 42.42.42 (2025-01-10)"\n',
                "echo Dummy git commit hash line\n",
                "echo Dummy license line\n",
                "echo Dummy author line\n",
            ]
        )
    os.chmod(dummy_exe, 0o755)
    assert not check_wsclean_version(dummy_exe)

    # Run a dummy executable that outputs nothing.
    with open(dummy_exe, "w", encoding="UTF-8") as dummy:
        dummy.write("#!/bin/sh\n")
    os.chmod(dummy_exe, 0o755)
    assert not check_wsclean_version(dummy_exe)
