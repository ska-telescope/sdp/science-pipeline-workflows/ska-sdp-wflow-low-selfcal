#  SPDX-License-Identifier: BSD-3-Clause

# -*- coding: utf-8 -*-

"""Tests for the DP3Runner class.
This file tests that the functions in the DP3Runner class execute
successfully and generate the expected outputs.
"""

import os

from ska_sdp_wflow_selfcal.pipeline.config.config import read_config_file
from ska_sdp_wflow_selfcal.pipeline.dp3_helper import SOLVER_TYPE, Dp3Runner

START_TIME = "29-Mar-2013/13:59:53.007"
SKYMODEL = "grouped.skymodel"
CWD = os.getcwd()


def test_calibrate_phaseonly_and_predict(
    dp3_runner, create_environment, tmp_path
):
    """Test DP3 phase only calibration and prediction."""

    config = read_config_file(f"{CWD}/config/LOFAR.averaged.yml")

    directions = (
        "[[Patch_0],[Patch_1],[Patch_10],[Patch_11],[Patch_12],"
        "[Patch_13],[Patch_14],[Patch_15],[Patch_16],[Patch_17],[Patch_18],"
        "[Patch_19],[Patch_2],[Patch_20],[Patch_21],[Patch_22],[Patch_23],"
        "[Patch_24],[Patch_25],[Patch_26],[Patch_27],[Patch_3],[Patch_4],"
        "[Patch_5],[Patch_6],[Patch_7],[Patch_8],[Patch_9]]"
    )
    solution_files = dp3_runner.calibrate(
        "test_phaseonly.calibrate",
        create_environment["ms_path"],
        START_TIME,
        SKYMODEL,
        config["selfcal_cycles"][0]["calibrate"]["dp3"],
    )

    assert os.path.isfile("logs/DP3.tag.test_phaseonly.calibrate.log")
    assert solution_files == [
        str(tmp_path / "test_phaseonly.calibrate.0.scalarphase.h5parm")
    ]
    assert os.path.isfile(solution_files[0])

    dp3_runner.predict(
        "test_phaseonly.predict",
        create_environment["ms_path"],
        "midbands.ms.mjd5020557063.outlier_1_modeldata",
        START_TIME,
        directions,
        SKYMODEL,
        solution_files[0],
        config["selfcal_cycles"][0]["predict"]["dp3"],
    )

    assert os.path.isfile("logs/DP3.tag.test_phaseonly.predict.log")


def test_calibrate_scalarphase_and_complexgain(
    dp3_runner, create_environment, tmp_path
):
    """Test DP3 calibration, using a combined scalarphase+complexgain run."""

    config = read_config_file(f"{CWD}/config/LOFAR.averaged.yml")
    dp3_config = config["selfcal_cycles"][2]["calibrate"]["dp3"]

    solution_files = dp3_runner.calibrate(
        "test_combined",
        create_environment["ms_path"],
        START_TIME,
        SKYMODEL,
        dp3_config,
    )

    assert os.path.isfile("logs/DP3.tag.test_combined.log")
    assert solution_files == [
        str(tmp_path / "test_combined.0.scalarphase.h5parm"),
        str(tmp_path / "test_combined.1.complexgain.h5parm"),
    ]
    assert os.path.isfile(solution_files[0])
    assert os.path.isfile(solution_files[1])


def test_antenna_constraint(
    create_environment_support_functions,
):
    """Test Dp3Runner::get_default_antenna_constraint()"""

    ms_name = create_environment_support_functions["ms_path"]

    # pylint: disable=W0123
    antenna_constraint = eval(
        Dp3Runner.get_default_antenna_constraint(ms_name)
    )

    # Assert the antenna_constraint consists of a single group of antennas
    assert len(antenna_constraint) == 1

    # Assert that this group contains 48 antennas
    assert len(antenna_constraint[0]) == 48

    # Assert that these antennas are all core stations,
    # i.e. all names start with "CS"
    assert all(ant.startswith("CS") for ant in antenna_constraint[0])


def test_solver_model_config(mocker):
    """Test Dp3Runner::get_solver_model_config()"""

    input_skymodel = "FOO.skymodel"
    solver_step = {"type": SOLVER_TYPE}
    other_step = {"type": "some_other_type"}

    def mocked_get_patches(skymodel):
        assert skymodel == input_skymodel
        return [["d0", "d42"]]

    mocker.patch(
        "ska_sdp_wflow_selfcal.pipeline.dp3_helper.get_patches",
        mocked_get_patches,
    )

    # A single step always uses the input skymodel.
    steps = {"single_step": solver_step}
    assert Dp3Runner.get_solver_model_config(input_skymodel, steps, 0) == {
        "single_step.sourcedb": input_skymodel
    }

    # With two successive solver steps, the second step reuses model data.
    steps = {
        "first_step": other_step,
        "first_solver_step": solver_step,
        "second_solver_step": solver_step,
        "last_step": other_step,
    }
    assert Dp3Runner.get_solver_model_config(input_skymodel, steps, 1) == {
        "first_solver_step.sourcedb": input_skymodel
    }
    reused_model = "[first_solver_step.d0,first_solver_step.d42]"
    assert Dp3Runner.get_solver_model_config(input_skymodel, steps, 2) == {
        "first_solver_step.keepmodel": True,
        "second_solver_step.reusemodel": reused_model,
    }

    # If there's a step in between two solver steps, reuse is disabled.
    # Both steps use the input skymodel.
    steps = {
        "first_solver_step": solver_step,
        "in_between_step": other_step,
        "second_solver_step": solver_step,
    }
    assert Dp3Runner.get_solver_model_config(input_skymodel, steps, 0) == {
        "first_solver_step.sourcedb": input_skymodel
    }
    assert Dp3Runner.get_solver_model_config(input_skymodel, steps, 2) == {
        "second_solver_step.sourcedb": input_skymodel
    }
