"""
Test that example pipeline config files provided in the dedicated directory
are all valid.
"""

import os
from pathlib import Path

import pytest

from ska_sdp_wflow_selfcal.pipeline.config.config import (
    read_config_file,
    render_dp3_config,
    render_dp3_option,
    render_wsclean_option,
    validate_config,
)


def get_config_dir() -> Path:
    """
    Path where the example config files are stored.
    """
    module_basedir = Path(__file__).parent.parent
    return module_basedir / "config"


CONFIG_DIR = get_config_dir()


def get_config_filenames() -> list[str]:
    """
    List of config filenames (without leading path) to be tested.
    """
    return [path.name for path in get_config_dir().glob("*.yml")]


CONFIG_FILENAMES = get_config_filenames()


@pytest.mark.parametrize("filename", CONFIG_FILENAMES, ids=CONFIG_FILENAMES)
def test_config_file_valid(filename: str):
    """Test that all example configurations are valid."""
    config_path = CONFIG_DIR / filename
    config = read_config_file(config_path)
    wsclean_config = config["selfcal_cycles"][0]["image"]["wsclean"]

    # Check that some fields are read correctly.
    # Note: those fields must be the same in all configuration files available
    # in the 'config' directory.
    assert wsclean_config["apply-facet-beam"]
    assert wsclean_config["auto-threshold"] == 1.0
    assert wsclean_config["multiscale"]
    assert wsclean_config["auto-mask"] == 5.0

    assert "image_scale" in config
    assert "image_size" in config
    assert "minimum_time" in config


def test_config_file_no_cycles():
    """
    Test that validate_config rejects configurations without self-cal cycles.
    """
    config = read_config_file(get_config_dir() / "LOFAR.averaged.yml")
    config["selfcal_cycles"] = []
    with pytest.raises(ValueError):
        validate_config(config)


def test_config_file_empty_predict():
    """
    Test that validate_config rejects predict steps where both
    subtract_outliers and subtract_bright_sources are disabled.
    """
    config = read_config_file(get_config_dir() / "LOFAR.averaged.yml")
    config["selfcal_cycles"][2]["predict"]["subtract_outliers"] = False
    config["selfcal_cycles"][2]["predict"]["subtract_bright_sources"] = False
    with pytest.raises(ValueError):
        validate_config(config)


def test_config_file_no_solver_steps():
    """Test that validate_config rejects calibrate without solver steps."""
    config = read_config_file(get_config_dir() / "LOFAR.averaged.yml")
    config["selfcal_cycles"][0]["calibrate"]["dp3"]["steps"] = {}
    with pytest.raises(ValueError):
        validate_config(config)


def test_config_file_too_many_solver_steps():
    """Test that validate_config rejects calibrate with three solver steps."""
    config = read_config_file(get_config_dir() / "LOFAR.averaged.yml")
    steps_config = config["selfcal_cycles"][3]["calibrate"]["dp3"]["steps"]
    steps_config["extra_solver"] = {
        "type": "ddecal",
        "mode": "complexgain",
    }
    with pytest.raises(ValueError):
        validate_config(config)


def test_config_file_invalid_first_solver_mode():
    """
    Test that validate_config rejects an invalid mode in the first solver step.
    """
    config = read_config_file(get_config_dir() / "LOFAR.averaged.yml")
    steps_config = config["selfcal_cycles"][1]["calibrate"]["dp3"]["steps"]
    steps_config["solve_scalarphase"]["mode"] = "invalid_mode"
    with pytest.raises(ValueError):
        validate_config(config)


def test_config_file_invalid_second_solver_mode():
    """
    Test that validate_config rejects an invalid mode in the second solver
    step.
    """
    config = read_config_file(get_config_dir() / "LOFAR.averaged.yml")
    steps_config = config["selfcal_cycles"][2]["calibrate"]["dp3"]["steps"]
    steps_config["solve_complexgain"]["mode"] = "invalid_mode"
    with pytest.raises(ValueError):
        validate_config(config)


def test_custom_tag(tmp_path):
    """
    Test that the custom tags !ref, !if and !include are parsed correcly
    """
    config_filename = os.path.join(tmp_path, "test_custom_tags.yml")
    with open(config_filename, "w", encoding="utf-8") as config_file:
        print(
            "foo: bar\n"
            "foobar: [foo, !ref foo]\n"
            "use_beam: true\n"
            "!if use_beam:\n"
            "   beammode: full\n"
            "not_true: false\n"
            "!if not_true:\n"
            "   result: 42\n"
            "!include test_custom_tag_include.yml:",
            file=config_file,
        )
    config_include_filename = os.path.join(
        tmp_path, "test_custom_tag_include.yml"
    )
    with open(
        config_include_filename, "w", encoding="utf-8"
    ) as config_include_file:
        print(
            # pylint: disable=implicit-str-concat
            "!if use_beam:\n" "   foobar2: [foo, !ref foo]\n",
            file=config_include_file,
        )
    config = read_config_file(config_filename, validate=False)

    # Test that !ref is evaluated correctly
    assert config["foobar"] == ["foo", "bar"]

    # Test that !if with true condition is evaluated correctly
    assert "beammode" in config.keys()
    assert config["beammode"] == "full"

    # Test that !if with false condition is evaluated correctly
    assert "result" not in config.keys()

    # Test that !include is evaluated correctly, including
    # the !ref and !if tag in the included file
    assert config["foobar2"] == ["foo", "bar"]


def test_render_wsclean_option():
    """
    Test that WSClean config is correctly converted to command line arguments.
    """

    assert render_wsclean_option("boolean-option", True) == ["-boolean-option"]
    assert render_wsclean_option("string-option", "dummy-string") == [
        "-string-option",
        "dummy-string",
    ]
    assert render_wsclean_option("float-option", 0.003) == [
        "-float-option",
        "0.003",
    ]
    assert render_wsclean_option(
        "list-option", ["list_element_string", 100.0]
    ) == ["-list-option", "list_element_string", "100.0"]


def test_render_dp3_option():
    """
    Tests rendering dp3 options with various value types.
    """
    none_config = None
    int_config = 10
    float_config = 10.0
    str_config = "tec"
    bool_config_true = True
    bool_config_false = False
    list_config = [["stn_000", "stn_001"], [True, False]]
    dict_config = {
        "key_0": none_config,
        "key_1": int_config,
        "key_2": float_config,
        "key_3": str_config,
        "key_4": bool_config_true,
        "key_5": bool_config_false,
        "key_6": list_config,
        "key_7": {
            "key_A": none_config,
            "key_B": int_config,
            "key_C": float_config,
            "key_D": str_config,
            "key_E": bool_config_true,
            "key_F": bool_config_false,
            "key_G": list_config,
            "key_H": {
                "key_alpha": int_config,
                "key_beta": list_config,
            },
        },
    }

    assert render_dp3_option("msout", none_config) == ["msout="]

    assert render_dp3_option("predict", int_config) == ["predict=10"]

    assert render_dp3_option("applycal", float_config) == ["applycal=10.0"]

    assert render_dp3_option("applybeam", str_config) == ["applybeam=tec"]

    assert render_dp3_option("ddecal", bool_config_true) == ["ddecal=True"]

    assert render_dp3_option("predict", bool_config_false) == ["predict=False"]

    assert render_dp3_option("solve", list_config) == [
        "solve=[[stn_000,stn_001],[True,False]]"
    ]

    assert render_dp3_option("solve", dict_config) == [
        "solve.key_0=",
        "solve.key_1=10",
        "solve.key_2=10.0",
        "solve.key_3=tec",
        "solve.key_4=True",
        "solve.key_5=False",
        "solve.key_6=[[stn_000,stn_001],[True,False]]",
        "solve.key_7.key_A=",
        "solve.key_7.key_B=10",
        "solve.key_7.key_C=10.0",
        "solve.key_7.key_D=tec",
        "solve.key_7.key_E=True",
        "solve.key_7.key_F=False",
        "solve.key_7.key_G=[[stn_000,stn_001],[True,False]]",
        "solve.key_7.key_H.key_alpha=10",
        "solve.key_7.key_H.key_beta=[[stn_000,stn_001],[True,False]]",
    ]


def test_render_dp3_config():
    """
    Tests rendering a complete DP3 configuration including "steps".
    """
    test_config = {
        "msin": "my/msin/directory.ms",
        "msout": "my/msout/directory.ms",
        "msin.nchan": 10,
        "msin.baseline": None,
        "steps": {
            "solve": {
                "type": "ddecal",
                "mode": "complexgain",
                "sourcedb": "my_skymodel.skymodel",
                "h5parm": "my_parmdb.h5",
                "directions": [
                    ["facet_0", "facet_1"],
                    ["facet_2", "facet_3"],
                ],
                "antennaconstraint": [
                    ["ant_000", "ant_001"],
                    ["ant_002", "ant_003"],
                ],
                "usebeammodel": True,
                "applycal": {
                    "steps": ["amp", "phase"],
                    "amp.correction": "amplitude000",
                    "phase.correction": "phase000",
                    "another_dict": {
                        "key_0": "str",
                        "key_1": 10,
                        "key_2": 10.0,
                        "key_3": [1, "2", 3.0, False],
                    },
                },
            },
            "applybeam": {
                "invert": True,
            },
            "step_with_none": None,  # passing test: this step NOT rendered!
            "step_with_int": 10,
            "step_with_float": 10.0,
            "step_with_str": "str",
            "step_with_bool_true": True,
            "step_with_bool_false": False,
            "step_with_list": [
                ["stn_000", "stn_001"],
                [True, False],
            ],
            "last_step.with_dot": "final_step",
        },
        "key_with_bool_true": True,
        "key_with_bool_false": False,
        "last_key": "final_value",
    }

    expected_output = [
        "msin=my/msin/directory.ms",
        "msout=my/msout/directory.ms",
        "msin.nchan=10",
        "msin.baseline=",
        (
            "steps=[solve,applybeam,step_with_none,step_with_int,"
            "step_with_float,step_with_str,step_with_bool_true,"
            "step_with_bool_false,step_with_list,last_step.with_dot]"
        ),
        "solve.type=ddecal",
        "solve.mode=complexgain",
        "solve.sourcedb=my_skymodel.skymodel",
        "solve.h5parm=my_parmdb.h5",
        "solve.directions=[[facet_0,facet_1],[facet_2,facet_3]]",
        "solve.antennaconstraint=[[ant_000,ant_001],[ant_002,ant_003]]",
        "solve.usebeammodel=True",
        "solve.applycal.steps=[amp,phase]",
        "solve.applycal.amp.correction=amplitude000",
        "solve.applycal.phase.correction=phase000",
        "solve.applycal.another_dict.key_0=str",
        "solve.applycal.another_dict.key_1=10",
        "solve.applycal.another_dict.key_2=10.0",
        "solve.applycal.another_dict.key_3=[1,2,3.0,False]",
        "applybeam.invert=True",
        "step_with_int=10",
        "step_with_float=10.0",
        "step_with_str=str",
        "step_with_bool_true=True",
        "step_with_bool_false=False",
        "step_with_list=[[stn_000,stn_001],[True,False]]",
        "last_step.with_dot=final_step",
        "key_with_bool_true=True",
        "key_with_bool_false=False",
        "last_key=final_value",
    ]

    assert render_dp3_config(test_config) == expected_output
