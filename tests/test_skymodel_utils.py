#  SPDX-License-Identifier: BSD-3-Clause

"""Tests for the functions in skymodel_utils.py"""

import os.path
import pickle

import lsmtool
import numpy as np
from shapely.geometry import Polygon

from ska_sdp_wflow_selfcal.pipeline.support.miscellaneous import (
    make_wcs,
    read_vertices_pixel_coordinates,
)
from ska_sdp_wflow_selfcal.pipeline.support.read_data import (
    get_patches,
    get_phase_center,
)
from ska_sdp_wflow_selfcal.pipeline.support.skymodel_utils import (
    create_full_sky_model,
    define_sector,
    discard_sources_outside_polygon,
    download_skymodel,
    get_bright_sources,
    get_outliers,
    group_sources,
    split_skymodel_file,
)


def assert_skymodels_are_equal(left_filename, right_filename):
    """
    Compares the contents of two skymodels.
    Ignores the comments in the skymodels, since they contain log messages
    which vary depending on run time.
    """
    left = lsmtool.load(left_filename)
    right = lsmtool.load(right_filename)

    assert left.getDefaultValues() == right.getDefaultValues()
    assert (left.getPatchNames() == right.getPatchNames()).all()
    assert left.getPatchPositions() == right.getPatchPositions()
    assert (left.getPatchSizes() == right.getPatchSizes()).all()
    assert left.getColNames() == right.getColNames()
    for name in left.getColNames():
        left_values = left.getColValues(name)
        right_values = right.getColValues(name)
        if np.issubdtype(left_values.dtype, np.inexact):
            assert np.isclose(left_values, right_values).all()
        else:
            assert (left_values == right_values).all()


def test_download_skymodel(create_environment_support_functions, mocker):
    """Tests downloading a skymodel given a ra,dec direction."""

    test_file = "test.skymodel"

    def mocked_get(url, timeout):
        assert (
            url == "http://tgssadr.strw.leidenuniv.nl/cgi-bin/gsmv5.cgi?"
            "coord=0.100000,0.200000&radius=5.000000&unit=deg&deconv=y"
        )
        assert timeout > 0

        # Create a 'response' object with a 'content' attribute.
        # An empty function is very suitable as a dummy object.
        def response():
            pass

        with open("initial.skymodel", "rb") as skymodel:
            response.content = skymodel.read()
        return response

    mocker.patch("requests.get", mocked_get)

    skymodel = download_skymodel(0.1, 0.2, test_file)

    # Assert that the skymodel contains information, and that it is correct
    assert len(skymodel.getColValues("Ra")) == 244
    assert os.path.isfile(test_file)
    assert_skymodels_are_equal(test_file, "initial.skymodel")


def test_group_sources(
    create_environment_support_functions,
):
    """Test that sources are grouped as expected"""

    skymodel = lsmtool.load(
        "initial.skymodel", create_environment_support_functions["ms_path"]
    )
    group_sources(skymodel, apply_beam=True)
    skymodel.write("grouped.skymodel", clobber=True)
    groups_expected = get_patches("expected_grouped.skymodel")[1]
    groups_calculated = get_patches("grouped.skymodel")[1]

    for group in groups_calculated:
        found = False
        i = 0
        while ~found and i < len(groups_expected):
            if np.array_equal(
                np.sort(group.flat), np.sort(groups_expected[i].flat)
            ):
                found = True
            i = i + 1
        assert found


def test_discard_sources_outside_polygon(create_environment_support_functions):
    """
    Test that discard_sources_outside_polygon correctly filters a skymodel.
    """

    ms_path = create_environment_support_functions["ms_path"]
    [phase_center_ra, phase_center_dec] = get_phase_center(ms_path)
    wcs = make_wcs(phase_center_ra, phase_center_dec)
    polygon = Polygon(
        read_vertices_pixel_coordinates("expected_sector_1_vertices.pkl", wcs)
    )

    # Load initial skymodel and remove sources lying outside the polygon
    skymodel = lsmtool.load("expected_grouped.skymodel")
    discard_sources_outside_polygon(skymodel, wcs, polygon)
    skymodel.write("skymodel_inside_polygon.skymodel", clobber=True)

    assert_skymodels_are_equal(
        "skymodel_inside_polygon.skymodel", "expected_sector.skymodel"
    )


def test_define_sector(
    create_environment_support_functions,
    mocker,
):
    """Test that the sector vertices are correct"""

    # The reference test files were generated with these widths.
    def mocked_width(_):
        return {"ra": 7.7, "dec": 7.7}

    mocker.patch(
        "ska_sdp_wflow_selfcal.pipeline.support.skymodel_utils."
        "compute_observation_width",
        mocked_width,
    )

    test_vertices_file = "test_vertices.pkl"
    expected_vertices_file = "expected_sector_1_vertices.pkl"

    sector_vertices = define_sector(
        "expected_grouped.skymodel",
        "sector.skymodel",
        create_environment_support_functions["ms_path"],
        test_vertices_file,
    )

    [phase_center_ra, phase_center_dec] = get_phase_center(
        create_environment_support_functions["ms_path"]
    )
    wcs = make_wcs(phase_center_ra, phase_center_dec)

    expected_vertices = read_vertices_pixel_coordinates(
        expected_vertices_file, wcs
    )
    assert np.isclose(list(expected_vertices), list(sector_vertices)).all()

    with open(expected_vertices_file, "rb") as file:
        expected_content = pickle.load(file)
    with open(test_vertices_file, "rb") as file:
        test_content = pickle.load(file)
    assert isinstance(test_content, list)
    assert len(test_content) == 2
    assert np.isclose(expected_content[0], test_content[0]).all()
    assert np.isclose(expected_content[1], test_content[1]).all()

    assert_skymodels_are_equal("sector.skymodel", "expected_sector.skymodel")


def test_get_outliers(create_environment_support_functions):
    """Test that the sources lying outside the sector are listed correclty"""

    get_outliers(
        "expected_grouped.skymodel",
        "expected_sector.skymodel",
        "outliers.skymodel",
    )

    assert_skymodels_are_equal(
        "expected_outliers.skymodel",
        "outliers.skymodel",
    )


def test_get_bright_sources(create_environment_support_functions):
    """Test that the sources lying outside the sector are listed correctly."""

    ms_path = create_environment_support_functions["ms_path"]
    [phase_center_ra, phase_center_dec] = get_phase_center(ms_path)
    wcs = make_wcs(phase_center_ra, phase_center_dec)
    poly_verts = read_vertices_pixel_coordinates(
        "expected_sector_1_vertices.pkl", wcs
    )

    # Somehow this test uses an apparent sky model as true sky model.
    skymodel = lsmtool.load("expected.apparent_sky.txt", ms_path)
    bright_source_apparent_skymodel = group_sources(
        skymodel,
        apply_beam=False,
    )
    bright_sources_skymodel = get_bright_sources(
        skymodel,
        bright_source_apparent_skymodel,
        ms_path,
        poly_verts,
    )
    assert len(bright_sources_skymodel) == 12

    skymodel_filenames = split_skymodel_file(bright_sources_skymodel, "part")
    assert len(skymodel_filenames) == 1
    assert len(lsmtool.load(skymodel_filenames[0])) == 12


def test_create_full_sky_model_with_itself(copy_test_data):
    """
    Test creating a sky model by combining the initial skymodel with itself.
    """
    copy_test_data("initial.skymodel")
    create_full_sky_model(
        "initial.skymodel", "initial.skymodel", "with_itself.skymodel"
    )
    assert_skymodels_are_equal("initial.skymodel", "with_itself.skymodel")


def test_create_full_sky_model(tmp_path):
    """Test that create_full_sky_model correctly combines skymodels."""
    sources = [
        {
            "Name": f"src{i}",
            "Type": "POINT",
            "Ra": f"12:0{i}:00.0",
            "Dec": f"20.42.{i}.0",
            "I": f"2.{i}",
        }
        for i in range(3)
    ]

    left = lsmtool.skymodel.SkyModel(sources[0])  # Only in left.
    right = lsmtool.skymodel.SkyModel(sources[1])  # Only in right.
    left.add(sources[2])  # In both left and right, with different values.
    sources[2]["I"] = "3.2"
    right.add(sources[2])

    # The combined model should contain sources[0], sources[1], and the
    # the 'right' value for sources[2].
    expected = lsmtool.skymodel.SkyModel(sources[0])
    expected.add(sources[1])
    expected.add(sources[2])

    # Create patches.
    left.group("every")
    right.group("every")
    expected.group("every")

    left_filename = os.path.join(tmp_path, "left.skymodel")
    right_filename = os.path.join(tmp_path, "right.skymodel")
    expected_filename = os.path.join(tmp_path, "expected.skymodel")
    combined_filename = os.path.join(tmp_path, "combined.skymodel")

    left.write(left_filename)
    right.write(right_filename)
    expected.write(expected_filename)

    create_full_sky_model(left_filename, right_filename, combined_filename)
    assert_skymodels_are_equal(combined_filename, expected_filename)
