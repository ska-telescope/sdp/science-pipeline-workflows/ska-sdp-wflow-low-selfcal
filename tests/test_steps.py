#  SPDX-License-Identifier: BSD-3-Clause

# -*- coding: utf-8 -*-

"""Tests for the single steps."""

import os

import pytest

from ska_sdp_wflow_selfcal.pipeline.operations import calibrate, image, predict


def test_calibrate_1(
    dp3_runner, extra_inputs, create_environment, tmp_path, mock_settings
):
    """
    Verify that calibrate_1(..) creates some specific files.
    The content of these files is not verified,
    because that is done in other tests.
    """

    # Get the full path to the measurement set.
    ms_path = create_environment["ms_path"]

    target_file = f"{tmp_path}/test_cal2.h5parm"

    # This is the method under testing.
    calibrate(
        dp3_runner,
        ms_path,
        f"{extra_inputs}/calibrate_1.skymodel",
        tmp_path,
        0,
        mock_settings,
        target_file,
    )

    # Verify that the target file exist in the returned path.
    assert os.path.isfile(target_file)


def test_calibrate_2(
    dp3_runner, extra_inputs, create_environment, tmp_path, mock_settings
):
    """
    Verify that calibrate_2(..) creates some specific files.
    The content of these files is not verified,
    because that is done in other tests.
    """

    # Get the full path to the measurement set.
    ms_path = create_environment["ms_path"]

    target_file = f"{tmp_path}/test_cal2.h5parm"

    # This is the method under testing.
    calibrate(
        dp3_runner,
        [ms_path],
        f"{extra_inputs}/calibrate_2.skymodel",
        tmp_path,
        1,
        mock_settings,
        target_file,
    )

    # Verify that the target file exist in the returned path.
    assert os.path.isfile(target_file)


def test_calibrate_3(
    dp3_runner, extra_inputs, create_environment, tmp_path, mock_settings
):
    """
    Verify that calibrate_3(..) creates some specific files.
    The content of these files is not verified,
    because that is done in other tests.
    """

    # Get the full path to the measurement set.
    ms_path = create_environment["ms_path"]

    target_file = f"{tmp_path}/test_cal3.h5parm"

    # This is the method under testing.
    calibrate(
        dp3_runner,
        [ms_path],
        f"{extra_inputs}/calibrate_3.skymodel",
        tmp_path,
        2,
        mock_settings,
        target_file,
    )

    assert os.path.isfile(target_file)


@pytest.mark.skip(reason="There is pending work in this test.")
def test_predict_1(dp3_runner, extra_inputs, tmp_path, mock_settings):
    """Test predict_1"""

    msin = "/var/scratch/csalvoni/data/midbands.ms"
    predict(
        dp3_runner,
        [msin],
        [f"{extra_inputs}/expected_outliers.skymodel"],
        tmp_path,
        "solutions.h5parm",
        0,
        "outlier",
        mock_settings,
    )


@pytest.mark.skip(reason="There is pending work in this test.")
def test_image_1(dp3_runner, wsclean_runner, tmp_path, mock_settings):
    """Test image_1"""

    msin = "/var/scratch/csalvoni/data/midbands.ms"
    skymodel = "/var/scratch/nslusare/working_pipeline/calibrate_1.skymodel"

    image(
        dp3_runner,
        wsclean_runner,
        [msin],
        "solutions.h5parm",
        skymodel,
        "todo: supply vertices",
        tmp_path,
        0,
        mock_settings,
    )
