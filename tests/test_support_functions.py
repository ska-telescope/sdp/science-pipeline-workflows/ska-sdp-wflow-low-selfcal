#  SPDX-License-Identifier: BSD-3-Clause

"""Tests for the ska_sdp_wflow_selfcal module."""

import math
import os.path
import re

import h5py
import lsmtool
import numpy as np

from ska_sdp_wflow_selfcal.pipeline.support.blank_image import blank_image
from ska_sdp_wflow_selfcal.pipeline.support.combine_h5parms import (
    combine_h5parms,
)
from ska_sdp_wflow_selfcal.pipeline.support.filter_skymodel import (
    filter_skymodel,
)
from ska_sdp_wflow_selfcal.pipeline.support.H5parm_collector import (
    collect_h5parms,
)
from ska_sdp_wflow_selfcal.pipeline.support.make_region_file import (
    make_region_file,
)
from ska_sdp_wflow_selfcal.pipeline.support.read_data import get_start_times
from ska_sdp_wflow_selfcal.pipeline.support.subtract_sector_models import (
    subtract_sector_models,
)
from ska_sdp_wflow_selfcal.pipeline.support.time import human_readable_time

from .test_skymodel_utils import assert_skymodels_are_equal


def is_float(string):
    """Check if a string contains a float number."""
    try:
        float(string)
        return True
    except ValueError:
        return False


def assert_regions_are_equal(regions_left, regions_right):
    """
    Compares the contents of two region files.
    Ignores the lines starting with a # as they contain time logs which vary
    depending on run time.
    Compares floats up to the 3rd decimal.

    Although skymodel files have a similar structure, this function does not
    work on skymodel files if one file has the default value in each column and
    the other does not. 'assert_skymodels_are_equal' does not have this limit.
    """

    with open(regions_left, encoding="utf-8") as file_1, open(
        regions_right, encoding="utf-8"
    ) as file_2:
        file_1_text = file_1.readlines()
        file_2_text = file_2.readlines()

    for word in file_1_text[:]:
        if word.startswith("#"):
            file_1_text.remove(word)

    for word in file_2_text[:]:
        if word.startswith("#"):
            file_2_text.remove(word)

    for line_1, line_2 in zip(file_1_text, file_2_text):
        for element_1, element_2 in zip(
            re.split(r",| |\)|\(", line_1), re.split(r",| |\)|\(", line_2)
        ):
            if is_float(element_1):
                element_1 = f"{float(element_1):.3f}"
                element_2 = f"{float(element_2):.3f}"

            assert element_1 == element_2


def create_h5_antennas():
    """Creates an array with compound "name" + "position" entries"""
    antenna_names = ["ANT1", "ANT2", "ANT3"]
    antenna_positions = [[1, 2, 3], [4, 5, 6], [-7, -8, -9]]

    # The workflow only supports H5 files with fixed-size strings.
    h5_antenna_names = list(
        map(lambda x: np.array(x, dtype="S"), antenna_names)
    )
    antenna_type = np.dtype(
        [("name", h5_antenna_names[0].dtype), ("position", "(3,)f4")]
    )
    return np.array(
        list(zip(h5_antenna_names, antenna_positions)), dtype=antenna_type
    )


def create_h5_sources(direction_names=None):
    """Creates an array with compound "name" + "dir" entries"""

    if not direction_names:
        direction_names = ["Dir01", "Dir02", "Dir03", "Dir04", "Dir05"]
    directions = [[11, -1], [12, -2], [13, -3], [14, -4], [15, -5]]
    h5_direction_names = list(
        map(lambda x: np.array(x, dtype="S"), direction_names)
    )
    source_type = np.dtype(
        [("name", h5_direction_names[0].dtype), ("dir", "(2,)f8")]
    )
    return np.array(
        list(zip(h5_direction_names, directions)), dtype=source_type
    )


def create_h5_file(filename, data, add_amplitude=False):
    """
    Creates a dummy H5 file for testing. The phase000 field is always present.

    Parameters
    ----------
    filename : str
        Filename of the output h5parm
    data: dict
        Data that must be written to the file.
        This function does not check the if the data is consistent.
        Mandatory keys: antennas, sources, times, frequencies, values, weights
        Optional keys: polarizations
    add_amplitude : bool
        Set to True if the output file should contain the amplitude000 field
    """
    with h5py.File(filename, "w") as h5file:
        # The workflow only supports H5 files with fixed-size strings.
        # -> Always use dtype="S" for string values.

        group = h5file.create_group("sol000")
        group.create_dataset("antenna", data=data["antennas"])
        group.create_dataset("source", data=data["sources"])

        if "polarizations" in data:
            axes = np.array("time,freq,ant,dir,pol", dtype="S")
        else:
            axes = np.array("time,freq,ant,dir", dtype="S")

        group = h5file.create_group("sol000/phase000")
        group.create_dataset("time", data=data["times"])
        group.create_dataset("freq", data=data["frequencies"])
        group.create_dataset("ant", data=data["antennas"]["name"])
        group.create_dataset("dir", data=data["sources"]["name"])
        if "polarizations" in data:
            group.create_dataset("pol", data=data["polarizations"])
        group.create_dataset("val", data=data["values"])
        group.create_dataset("weight", data=data["weights"])
        h5file["sol000/phase000"].attrs["TITLE"] = np.array("phase", dtype="S")
        h5file["sol000/phase000/val"].attrs["AXES"] = axes
        h5file["sol000/phase000/weight"].attrs["AXES"] = axes

        if add_amplitude:
            group = h5file.create_group("sol000/amplitude000")
            group.create_dataset("time", data=data["times"])
            group.create_dataset("freq", data=data["frequencies"])
            group.create_dataset("ant", data=data["antennas"]["name"])
            group.create_dataset("dir", data=data["sources"]["name"])
            if "polarizations" in data:
                group.create_dataset("pol", data=data["polarizations"])
            group.create_dataset("val", data=data["values"])
            group.create_dataset("weight", data=data["weights"])
            h5file["sol000/amplitude000"].attrs["TITLE"] = np.array(
                "amplitude", dtype="S"
            )
            h5file["sol000/amplitude000/val"].attrs["AXES"] = axes
            h5file["sol000/amplitude000/weight"].attrs["AXES"] = axes

        h5file["sol000"].attrs["h5parm_version"] = np.array("1.0", dtype="S")


def test_collect_h5(tmp_path):
    """Test the concatenation (in time) of multiple h5 files into one"""

    os.chdir(tmp_path)

    data = {
        "times": np.arange(42, 82, dtype=np.float64),
        "frequencies": np.array([42e6, 43e6, 44e6, 45e6], dtype=np.float64),
        "antennas": create_h5_antennas(),
        "sources": create_h5_sources(),
    }
    shape = (
        len(data["times"]),
        len(data["frequencies"]),
        len(data["antennas"]),
        len(data["sources"]),
    )
    data["values"] = np.empty(shape, dtype=np.float64)
    data["weights"] = np.empty(shape, dtype=np.float32)
    # Fill in data["values"] and data["weights"] with time varying values.
    # Below, verify that the concatenation works correctly using these values.
    for time in range(len(data["times"])):
        data["values"][time, :, :, :] = 42 + time
        data["weights"][time, :, :, :] = 1 + time / 100

    h5_list = []
    for i in range(4):
        filename = f"input{i}.h5"
        h5_list.append(filename)

        start_time_index = 10 * i
        end_time_index = 10 * (i + 1)
        file_data = {
            "times": data["times"][start_time_index:end_time_index],
            "frequencies": data["frequencies"],
            "antennas": data["antennas"],
            "sources": data["sources"],
            "values": data["values"][start_time_index:end_time_index, :, :, :],
            "weights": data["weights"][
                start_time_index:end_time_index, :, :, :
            ],
        }

        create_h5_file(filename, file_data)

    h5_combined = "combined.h5"
    collect_h5parms(h5_list, h5_combined)

    with h5py.File(h5_combined, "r") as combined_file:
        combined_antennas = combined_file["sol000"]["antenna"]
        assert (combined_antennas["name"] == data["antennas"]["name"]).all()
        assert (
            combined_antennas["position"] == data["antennas"]["position"]
        ).all()

        combined_sources = combined_file["sol000"]["source"]
        assert (combined_sources["name"] == data["sources"]["name"]).all()
        assert (combined_sources["dir"] == data["sources"]["dir"]).all()

        phase = combined_file["sol000/phase000"]
        assert (np.array(phase["ant"]) == data["antennas"]["name"]).all()
        assert (np.array(phase["freq"]) == data["frequencies"]).all()
        assert (np.array(phase["dir"]) == data["sources"]["name"]).all()
        assert (np.array(phase["time"]) == data["times"]).all()
        assert (np.array(phase["val"]) == data["values"]).all()
        assert (np.array(phase["weight"]) == data["weights"]).all()

    for filename in h5_list:
        assert not os.path.isfile(filename)


def test_combine_h5(
    create_environment_support_functions,
):  # pylint: disable=too-many-locals
    """This test checks that two H5 files with different fields and
    dimensions are correctly combined"""

    # Read facet list and flux per facet from calibrators skymodel
    skymodel_filename = "grouped.skymodel"
    skymodel = lsmtool.load(skymodel_filename)
    directions_list = list(set(skymodel.table["Patch"]))

    # Create dummy calibration files.
    fast_phase_solution_filename = "fast_phase.h5parm"
    slow_gain_solution_filename = "slow_gain.h5parm"
    combined_solution_filename = "combined.h5parm"

    # Fast phase only contains phase000, no polarization axis and the time
    # axis contains all time slots.
    data = {
        "times": np.arange(42, 82, dtype=np.float64),
        "frequencies": np.array([42.0e7, 43.0e7, 44.0e7], dtype=np.float64),
        "antennas": create_h5_antennas(),
        "sources": create_h5_sources(directions_list),
    }
    data["values"] = np.random.rand(
        len(data["times"]),
        len(data["frequencies"]),
        len(data["antennas"]),
        len(data["sources"]),
    )
    data["weights"] = data["values"]

    create_h5_file(fast_phase_solution_filename, data, False)

    # Slow gain contains phase000 and amplitude000, has a polarization axis
    # and the time axis contains a decimated version of the time slots.
    data["times"] = data["times"][::10]
    data["polarizations"] = np.array(["XX", "YY"], dtype="S")

    data["values"] = np.random.rand(
        len(data["times"]),
        len(data["frequencies"]),
        len(data["antennas"]),
        len(data["sources"]),
        len(data["polarizations"]),
    )
    data["weights"] = data["values"]

    create_h5_file(slow_gain_solution_filename, data, True)

    # Function to test
    combine_h5parms(
        fast_phase_solution_filename,
        slow_gain_solution_filename,
        combined_solution_filename,
        solset1="sol000",
        solset2="sol000",
    )

    with h5py.File(
        fast_phase_solution_filename, "r"
    ) as fast_phase_solution, h5py.File(
        slow_gain_solution_filename, "r"
    ) as slow_gain_solution, h5py.File(
        combined_solution_filename, "r"
    ) as combined_solution:
        keys_fast_phase = list(fast_phase_solution["sol000"].keys())
        keys_slow_gain = list(slow_gain_solution["sol000"].keys())
        keys_combined = list(combined_solution["sol000"].keys())

        # check that all the calibration fields (amplitude and phase) are
        # present in the combined h5
        assert sorted(keys_combined) == sorted(
            set(keys_fast_phase + keys_slow_gain)
        )

        # Check that the time axis matches the solution with the highest amount
        # of time intervals
        n_times_fast_phase = len(fast_phase_solution["sol000/phase000/time"])
        n_times_slow_gain = len(slow_gain_solution["sol000/phase000/time"])
        n_times_combined = len(combined_solution["sol000/phase000/time"])
        assert n_times_combined == np.max(
            [n_times_fast_phase, n_times_slow_gain]
        )


def test_blank_image(
    create_environment_support_functions,
):
    """Test generation of mask"""
    blank_image(
        "sector_1_mask.fits",
        input_image=None,
        vertices_file="expected_sector_1_vertices.pkl",
        reference_ra_deg="258.845708333",
        reference_dec_deg="57.4111944444",
        cellsize_deg=0.00034722222222222224,
        imsize=[24394, 24394],
    )


def test_filter_skymodel(
    create_environment_support_functions,
):
    """Test skymodel filtering"""

    beam_ms = create_environment_support_functions["ms_path"]
    image_name = "test_image.fits"
    skymodel_prefix = "output"

    filter_skymodel(
        image_name,
        image_name,
        "sector_1-sources-pb.txt",
        skymodel_prefix,
        "expected_sector_1_vertices.pkl",
        beam_ms=beam_ms,
        threshisl=4.0,
        threshpix=5.0,
    )

    assert os.path.exists(f"{image_name}.mask")
    assert os.path.exists(f"{skymodel_prefix}.apparent_sky.txt")
    assert os.path.exists(f"{skymodel_prefix}.true_sky.txt")

    assert_skymodels_are_equal(
        f"{skymodel_prefix}.apparent_sky.txt", "expected.apparent_sky.txt"
    )
    assert_skymodels_are_equal(
        f"{skymodel_prefix}.true_sky.txt", "expected.true_sky.txt"
    )


def test_filter_skymodel_empty(
    create_environment_support_functions,
):
    """Test skymodel filtering with too high thresholds"""
    beam_ms = create_environment_support_functions["ms_path"]
    image_name = "test_image.fits"
    skymodel_prefix = "output"

    filter_skymodel(
        image_name,
        image_name,
        "sector_1-sources-pb.txt",
        skymodel_prefix,
        "expected_sector_1_vertices.pkl",
        beam_ms=beam_ms,
        threshisl=400.0,
        threshpix=500.0,
    )

    assert os.path.exists(f"{skymodel_prefix}.apparent_sky.txt")
    assert os.path.exists(f"{skymodel_prefix}.true_sky.txt")

    assert_skymodels_are_equal(
        f"{skymodel_prefix}.apparent_sky.txt", "empty.skymodel"
    )
    assert_skymodels_are_equal(
        f"{skymodel_prefix}.true_sky.txt", "empty.skymodel"
    )


def test_subtract_sector_models(
    create_environment_support_functions,
):
    """Test subtract_sector_models"""

    # Get the path of the measurement set for testing.
    ms_in = create_environment_support_functions["ms_path"]
    ms_out = "test_output.ms"

    # This is the start time in the used measurement set.
    start_time = human_readable_time(get_start_times(ms_in)[0])

    subtract_sector_models(
        ms_in,
        ms_out,
        [ms_in],  # The model data is in the same measurement set.
        msin_column="DATA",
        model_column="MODEL_DATA",
        nr_outliers=1,
        nr_bright=0,
        reweight=False,
        starttime=start_time,
        phaseonly=True,
    )

    assert os.path.isdir(ms_out)


def test_make_region_file(
    tmp_path,
):
    """Test the generation of a region file from a skymodel"""
    ra_mid = 258.84570833
    dec_mid = 57.411194444
    width_ra = 10.0
    width_dec = 10.0
    region_file = f"{tmp_path}/regions.ds9"

    make_region_file(
        "tests/test_data/expected_grouped.skymodel",
        ra_mid,
        dec_mid,
        width_ra,
        width_dec,
        region_file,
    )
    assert_regions_are_equal(
        "tests/test_data/expected_facets.reg", region_file
    )


def test_get_start_times(create_environment):
    """
    Test the function get_start_times.
    Compares the read values with target values
    from a known measurement set.
    """

    target_times = [
        4871282393.00695,
        4871282413.034752,
        4871282433.062553,
    ]

    ms_path = create_environment["ms_path"]
    time_list = get_start_times(ms_path, 3)
    for value, target in zip(time_list, target_times):
        assert math.isclose(value, target, abs_tol=1e-6)
