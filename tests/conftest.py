#  SPDX-License-Identifier: BSD-3-Clause

""" In this module all fixtures to run the python tests are defined """
import dataclasses
import glob
import os
import shutil
import sys
import urllib.request
from subprocess import check_call

import yaml

try:
    import dp3
except ModuleNotFoundError:
    pass  # dp3 dependency is only needed if DP3 is not found, e.g., on CI.
import pytest

from ska_sdp_wflow_selfcal.pipeline.config.config import read_config_file
from ska_sdp_wflow_selfcal.pipeline.distribute import Distributor
from ska_sdp_wflow_selfcal.pipeline.dp3_helper import Dp3Runner
from ska_sdp_wflow_selfcal.pipeline.wsclean_helper import WSCleanRunner

TEST_DATA = "tests/test_data"
DP3_LOG_LINE = "Hello from mocked DP3!"
WSCLEAN_LOG_LINE = "Hello from mocked wsclean!"
CWD = os.getcwd()


@pytest.fixture(scope="session")
def get_wsrt_measures(tmp_path_factory):
    """Download WSRT measures to temporary directory if needed"""
    casarc = str(os.path.expanduser("~/.casarc"))
    if not os.path.isfile(casarc):
        measures_path = str(tmp_path_factory.mktemp("WSRT_Measures"))

        print(
            "Creating temporary ~/.casarc and "
            f"downloading WSRT measures to {measures_path}"
        )

        with open(casarc, "w", encoding="utf-8") as casarc_file:
            casarc_file.write(f"measures.directory: {measures_path}")

        measures_tar = "WSRT_Measures.ztar"
        urllib.request.urlretrieve(
            f"ftp://ftp.astron.nl/outgoing/Measures/{measures_tar}",
            f"{measures_path}/{measures_tar}",
        )
        check_call(["tar", "xf", measures_tar], cwd=measures_path)
        yield
        os.remove(casarc)
    else:
        yield


@pytest.fixture(autouse=False, name="create_environment")
def source_env(tmp_path, run_dp3=True, run_wsclean=True):
    """
    This fixture uncompresses tNDPPP-generic.MS
    into a temporary directory.
    """

    # Change to the temporary directory.
    os.chdir(tmp_path)

    # Uncompress the measurement set in the tgz file.
    ms_name = "tNDPPP-generic.MS"
    source = f"{CWD}/{TEST_DATA}/{ms_name}.tgz"
    if not os.path.isfile(source):
        raise FileNotFoundError(f"Not able to find {source}.")
    check_call(["tar", "xf", source])

    if run_dp3:
        copy_data_dp3()
    if run_wsclean:
        copy_data_wsclean()

    # The full path of the measurement set
    ms_path = os.path.join(tmp_path, ms_name)

    # Tests are executed here
    yield {"ms_path": ms_path}

    # Post-test: clean up
    os.chdir(CWD)


@pytest.fixture(autouse=False, name="create_environment_support_functions")
def source_env_support_functions(get_wsrt_measures, tmp_path):
    # pylint: disable=W0621
    """Define a temporary folder to run the test. The folder is deleted
    once the test has been run"""
    os.chdir(tmp_path)

    # Extract the additional folders needed by casacore to create a new fits
    # file and save the path to .casarc

    ms_name = "test_midbands.ms"
    source = f"{CWD}/{TEST_DATA}/{ms_name}.tgz"
    if not os.path.isfile(source):
        raise FileNotFoundError(f"Not able to find {source}.")
    check_call(["tar", "xf", source])

    copy_data_support_functions()

    # The full path of the measurement set
    ms_path = os.path.join(tmp_path, ms_name)

    # Fails if the directory does not exits.
    assert os.path.exists(ms_path)

    # Tests are executed here
    yield {"ms_path": ms_path}

    # Post-test: clean up
    os.chdir(CWD)


def do_copy_test_data(*files_to_copy):
    """Copies files from test_data into temporary folder."""
    for filename in files_to_copy:
        file_path = f"{CWD}/{TEST_DATA}/{filename}"
        if not os.path.isfile(file_path):
            raise FileNotFoundError(f"Not able to find {file_path}.")
        shutil.copy(file_path, filename)


@pytest.fixture
def copy_test_data(tmp_path):
    """Returns a function that copies test data files."""
    os.chdir(tmp_path)
    yield do_copy_test_data
    os.chdir(CWD)


def copy_data_support_functions():
    """Copy data needed for tests into temporary folder"""
    do_copy_test_data(
        "facets.reg",
        "fast_phase_0.h5parm",
        "grouped.skymodel",
        "expected_sector_1_vertices.pkl",
        "sector_1-sources-pb.txt",
        "calibration_3_fast_phase_0.h5parm",
        "initial.skymodel",
        "expected_facets.reg",
        "expected_grouped.skymodel",
        "expected_outliers.skymodel",
        "expected_sector.skymodel",
        "test_image.fits",
        "expected.true_sky.txt",
        "expected.apparent_sky.txt",
        "empty.skymodel",
    )


def copy_data_dp3():
    """Copy data needed for DP3 tests into temporary folder"""
    skymodel_path = f"{CWD}/{TEST_DATA}/grouped.skymodel"
    if not os.path.isfile(skymodel_path):
        raise FileNotFoundError(f"Not able to find {skymodel_path}.")
    shutil.copy(skymodel_path, "grouped.skymodel")


def copy_data_wsclean():
    """Copy data needed for WSClean tests into temporary folder"""
    facets_path = f"{CWD}/{TEST_DATA}/facets.reg"
    if not os.path.isfile(facets_path):
        raise FileNotFoundError(f"Not able to find {facets_path}.")
    shutil.copy(facets_path, "facets.reg")

    h5parm_path = f"{CWD}/{TEST_DATA}/fast_phase_0.h5parm"
    if not os.path.isfile(h5parm_path):
        raise FileNotFoundError(f"Not able to find {h5parm_path}.")
    shutil.copy(h5parm_path, "fast_phase_0.h5parm")


def create_mocked_executable(filename, log_line):
    """Creates a mocked executable that output the given log line."""
    with open(filename, "w", encoding="utf-8") as file:
        file.write("#!/bin/sh\n")
        file.write(f"echo {log_line}\n")
    os.chmod(filename, 0o755)  # Make the file executable


@pytest.fixture
def mock_settings(create_environment_support_functions, get_wsrt_measures):
    # pylint: disable=W0621
    """
    - Creates mocked executables for dp3 and wsclean.
    - Copies necessary input files into the current directory (a temporary
      pytest folder).
    - Returns an object with arguments/settings for the pipeline.
    """
    dp3_filename = os.getcwd() + "/mock-DP3"
    wsclean_filename = os.getcwd() + "/mock-wsclean"

    create_mocked_executable(dp3_filename, DP3_LOG_LINE)
    create_mocked_executable(wsclean_filename, WSCLEAN_LOG_LINE)

    shutil.copy(
        f"{CWD}/tests/test_data/expected_sector_1_vertices.pkl",
        "vertices.pkl",
    )
    shutil.copy(
        f"{CWD}/tests/test_data/initial.skymodel",
        "calibrate_1.skymodel",
    )
    shutil.copy(
        f"{CWD}/tests/test_data/fast_phase_0.h5parm",
        "calibrate1_solutions.h5",
    )
    config = read_config_file(f"{CWD}/config/LOFAR.averaged.yml")
    with open("LOFAR.averaged.yml", "w", encoding="utf-8") as config_file:
        yaml.dump(config, config_file, sort_keys=False)
    # Generate 4 fake no-outlier MSs, so functions like calculate_n_times work.
    times_mjd = ["5020571443", "5020571451", "5020571459", "5020571468"]
    ms_path = create_environment_support_functions["ms_path"]
    for time in times_mjd:
        os.symlink(
            ms_path,
            f"{ms_path}.mjd{time}.predict_1_no_outliers",
            target_is_directory=True,
        )

    @dataclasses.dataclass
    class MockSettings:  # pylint: disable=R0902
        """Arguments for the workflow"""

        input_ms: list[str] = dataclasses.field(
            default_factory=lambda: [
                create_environment_support_functions["ms_path"]
            ]
        )
        config_file: str = "LOFAR.averaged.yml"
        imaging_taper_gaussian: str = "0.0"
        dp3_path: str = dp3_filename
        dp3_environment: str = None
        wsclean_cmd: list[str] = dataclasses.field(
            default_factory=lambda: [wsclean_filename]
        )
        restore_args: list[str] = dataclasses.field(
            default_factory=lambda: ["-dummy", "restore", "arguments"]
        )
        logging_tag: str = None
        work_dir: str = os.getcwd()
        dask_scheduler: str = None
        ignore_version_errors: str = "True"
        # Store these values from run_pipeline() too, so tests can use them.
        configuration = read_config_file(config_file)
        data_fraction: float = 0.2
        minimum_time: float = 600.0
        # Store these test log lines too, so the tests can use them.
        dp3_log_line: str = DP3_LOG_LINE
        wsclean_log_line: str = WSCLEAN_LOG_LINE

    return MockSettings()


@pytest.fixture
def dp3_runner(get_wsrt_measures, tmp_path):
    # pylint: disable=W0621
    """Create a DP3Runner if DP3 is installed, and skip test if not."""

    config = {"minimum_time": 600.0}

    # Prioritise locally installed DP3 over binary from pip wheel
    path = shutil.which("DP3")
    environment = {}
    if not path:
        path = shutil.which("__DP3_from_pip__")

        # Update environment variables so the pip-installed DP3 can find
        # the libraries it depends on
        dp3_lib_path = os.path.abspath(
            os.path.join(dp3.__file__, "..", "..", "DP3.libs")
        )
        if not os.path.exists(dp3_lib_path):
            raise FileNotFoundError(
                "Unable to extract DP3 library path automatically, "
                "expected path does not exist."
            )

        python_version_str = (
            f"{sys.version_info.major}.{sys.version_info.minor}"
        )
        # os.path.realpath eliminates symbolic links, if any.
        python_exe_dir = os.path.dirname(os.path.realpath(sys.executable))

        filelist = glob.glob(
            os.path.abspath(os.path.join(python_exe_dir, ".."))
            + f"/**/libpython{python_version_str}.so",
            recursive=True,
        )
        if not filelist:
            raise FileNotFoundError(
                "File search to python library location unsuccessful."
            )
        python_lib_path = filelist[0]

        environment = {
            "LD_LIBRARY_PATH": dp3_lib_path,
            "LD_PRELOAD": python_lib_path,
        }

    if not path:
        raise FileNotFoundError(
            "Cannot find DP3 executable. Make sure it is installed and its "
            "location is included in the PATH."
        )

    @dataclasses.dataclass
    class DP3Settings:
        """Settings for initializing DP3Runner in tests"""

        dp3_path: str = path
        logging_tag: str = "tag"
        work_dir: str = tmp_path

        # 'dict' and 'list' fields require 'dataclasses.field'.
        dp3_environment: dict[str, str] = dataclasses.field(
            default_factory=lambda: environment
        )
        configuration: dict[str, any] = dataclasses.field(
            default_factory=lambda: config
        )

    run_distributed = False
    # pylint: disable=E1102
    return Distributor(Dp3Runner, run_distributed)(DP3Settings())


@pytest.fixture
def wsclean_runner(create_environment):
    """Create a WSCleanRunner if wsclean is installed, and skip test if not."""

    cmd = shutil.which("wsclean")
    if not cmd:
        pytest.skip("Skipping test as WSClean is not installed")

    @dataclasses.dataclass
    class WSCleanArgs:  # pylint: disable=R0902
        """Arguments for initializing WSCleanRunner in tests"""

        wsclean_cmd: list[str] = dataclasses.field(
            default_factory=lambda: [cmd]
        )
        restore_args: list[str] = dataclasses.field(default_factory=list)
        input_ms: str = create_environment["ms_path"]
        logging_tag: str = "tag"
        imaging_size: str = "50"
        imaging_scale: str = "0.01"
        imaging_taper_gaussian: str = "0.0"

    return WSCleanRunner(WSCleanArgs())


@pytest.fixture
def extra_inputs():
    """Return the full path of the extra_inputs directory."""

    path = f"{CWD}/{TEST_DATA}"
    if not os.path.isdir(path):
        raise IOError(f"Not able to find {path}.")

    return path
