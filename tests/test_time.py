"""Tests for the time functions."""

from ska_sdp_wflow_selfcal.pipeline.support.time import human_readable_time


def test_human_readable_time():
    """
    Test human_readable_time().
    """
    assert human_readable_time(None) is None
    assert human_readable_time(4000000000.00000) == "19Aug1985/07:06:40.000"
    assert human_readable_time(4320500001.00500) == "15Oct1995/18:53:21.005"
    assert human_readable_time(4453401600.00000) == "01Jan2000/00:00:00.000"
    assert human_readable_time(4800000030.00060) == "25Dec2010/13:20:30.000"
    assert human_readable_time(4900010500.10000) == "25Feb2014/02:01:40.100"
    assert human_readable_time(5050000064.00300) == "27Nov2018/01:47:44.003"
    assert human_readable_time(5084553600.00000) == "01Jan2020/00:00:00.000"
    assert human_readable_time(5187539500.26450) == "06Apr2023/23:11:40.264"
    assert human_readable_time(5242406400.00000) == "01Jan2025/00:00:00.000"
