#  SPDX-License-Identifier:  BSD-3-Clause

""" This module contains functions to read data from a measurement set
and skymodels """

import logging
import math

import casacore.tables as pt
import lsmtool
import numpy as np
from astropy import units
from astropy.coordinates import Angle
from casacore.tables import taql
from scipy.constants import speed_of_light

logger = logging.getLogger("ska-sdp-wflow-selfcal")


def get_phase_center(measurement_set):
    """Read phase center from given MS. Returns the ra, dec values
    in degrees"""
    phase_center = taql(
        f"select PHASE_DIR deg as PHASE_DIR from {measurement_set}::FIELD"
    )
    [ra_deg, dec_deg] = phase_center[0]["PHASE_DIR"][0]
    ra = Angle(ra_deg * units.deg).wrap_at(360 * units.deg).degree
    dec = dec_deg
    return [ra, dec]


def get_antennas(measurement_set):
    """Read antennas from given MS."""
    antennas = taql(f"select NAME from {measurement_set}::ANTENNA").getcol(
        "NAME"
    )
    return antennas


def get_patches(skymodel_filename):
    """Get patches from skymodel.
    Returns a list of numpy arrays, one array per patch."""
    skymodel = lsmtool.load(skymodel_filename)
    patch_names = skymodel.getColValues(["Patch"])
    ra_coordinate = skymodel.getColValues(["Ra"])
    dec_coordinate = skymodel.getColValues(["Dec"])
    data = np.column_stack((patch_names, ra_coordinate, dec_coordinate))

    groups = []
    for patch in set(patch_names):
        sources_in_patch = data[data[:, 0] == patch]
        groups.append(sources_in_patch[:, 1::])

    return [list(set(patch_names)), groups]


def get_observation_params(measurement_set):
    """Read observation info from given MS"""

    observation = {}
    # Calculate mean elevation
    query_elevation = f"SELECT gmean(mscal.azel()[1]) from {measurement_set}"
    observation["mean_el_rad"] = taql(query_elevation).getcol("Col_1")[0]

    # Read reference frequency
    query_ref_frequency = (
        f"select REF_FREQUENCY from {measurement_set}::SPECTRAL_WINDOW"
    )
    observation["ref_freq"] = taql(query_ref_frequency).getcol(
        "REF_FREQUENCY"
    )[0]

    # Read dish diameter
    query_antenna_diameter = (
        f"select DISH_DIAMETER from {measurement_set}::ANTENNA"
    )
    observation["diam"] = float(
        taql(query_antenna_diameter).getcol("DISH_DIAMETER")[0]
    )

    # Read telescope name
    query_telescope_name = (
        f"select TELESCOPE_NAME from {measurement_set}::OBSERVATION"
    )
    observation["telescope_name"] = taql(query_telescope_name).getcol(
        "TELESCOPE_NAME"
    )[0]

    return observation


def get_imaging_channels_out(measurement_set):
    """Calculate the default "-channels-out" parameter for WSClean runs."""

    spw = measurement_set + "::SPECTRAL_WINDOW"
    min_frequency = taql(
        f"select min(CHAN_FREQ) as MIN_CHAN_FREQ from {spw}"
    ).getcell("MIN_CHAN_FREQ", 0)

    # Set number of output channels to get ~ 4 MHz per channel equivalent at
    # 120 MHz (the maximum averaging allowed for typical dTEC values of
    # -0.5 < dTEC < 0.5)
    target_bandwidth = 4e6 * min_frequency / 120e6
    total_bandwidth, n_channels = get_total_bandwidth(measurement_set)
    bandwidth_channels = math.ceil(total_bandwidth / target_bandwidth)

    channels_out = min(n_channels, bandwidth_channels)
    return channels_out


def get_total_bandwidth(measurement_set):
    """Extract the total bandwidth and number of channels from the
    measurement set"""

    taql_result = taql(
        f"select NUM_CHAN, CHAN_WIDTH from {measurement_set}::SPECTRAL_WINDOW"
    )
    n_channels = taql_result.getcell("NUM_CHAN", 0)
    channel_width = taql_result.getcell("CHAN_WIDTH", 0)[0]
    total_bandwidth = n_channels * channel_width

    return total_bandwidth, n_channels


def get_total_time(measurement_set_list):
    """Returns the total time of the list of MS given, in seconds"""
    total_time = 0.0
    for measurement_set in measurement_set_list:
        taql_result = pt.taql(
            "select gmin(TIME) as START_TIME, gmax(TIME) as END_TIME from "
            + measurement_set
        )
        start_time = taql_result.getcell("START_TIME", 0)
        end_time = taql_result.getcell("END_TIME", 0)
        total_time += end_time - start_time
    return total_time


def get_imaging_n_iterations(
    measurement_set_list,
    max_nmiter,
    peel_bright_sources,
):
    """Calculate the number of major iterations needed in WSClean.
    This calculation is done based on the integration time and the distance
    of the sector vertices to the phase center. The number of iterations is
    also reduced if bright sources are peeled"""

    # Find total observation time in hours
    total_time_hr = get_total_time(measurement_set_list) / 3600.0

    total_bandwidth, _ = get_total_bandwidth(measurement_set_list[0])
    scaling_factor = np.sqrt(
        float(total_bandwidth / 2e6) * total_time_hr / 16.0
    )

    wsclean_nmiter = min(max_nmiter, max(2, int(round(8 * scaling_factor))))

    if peel_bright_sources:
        # If bright sources are peeled, reduce nmiter by 25% (since they
        # no longer
        # need to be cleaned)
        wsclean_nmiter = max(2, int(round(wsclean_nmiter * 0.75)))

    return wsclean_nmiter


def calculate_n_chunks(measurement_set, data_fraction=1.0, mintime=600.0):
    """
    Calculate the max number of chunks a measurement set can be split into,
    where each (fraction of) chunk should be at least mintime seconds .

    Parameters:
    - measurement_set (str): Path to the measurement set file.
    - data_fraction (float): Fraction of the total data to be processed.
    Default is 1.0 (all data is processed)
    - mintime (float): Minimum time duration for each chunk, in seconds.
    Default is 600.0.

    Returns:
    - n_chunks (int): Number of chunks required to process the measurement set.
    """
    # Read the total time of the measurement set, in seconds.
    total_time = get_total_time([measurement_set])

    # The minimum time cannot be smaller than the time interval of the MS,
    # i.e. the time between two consecutive time slots in the MS.
    table = pt.table(measurement_set, readonly=True, ack=False)
    interval = table.getcell("INTERVAL", 0)
    mintime = max(mintime, interval)
    minimum_data_fraction = min(1.0, mintime / float(total_time))
    if data_fraction < minimum_data_fraction:
        logger.warning(
            "The specified value of data_fraction (%0.3f) results in a total "
            "time for this observation that is less than the largest "
            "potential calibration timestep (%0.1f s). The data fraction will "
            "be increased to %0.3f to ensure the timestep requirement is met.",
            data_fraction,
            mintime,
            minimum_data_fraction,
        )
        data_fraction = minimum_data_fraction

    # Add a small number to avoid rounding errors.
    n_chunks = max(
        1, int(np.floor(data_fraction * total_time / mintime + 0.00001))
    )
    return n_chunks


def calculate_n_times(measurement_set, interval=600.0):
    """
    Calculates the number of time slots which cover a specified interval.

    Parameters:
    - measurement_set (str): The filename of the measurement set.
    - interval (float): The time interval in seconds. Default is 600.0 s.
      (default value from Rapthor, which allows enough data for calibration)

    Returns:
    - n_intervals_per_chunk (int): The number of intervals per chunk.
    """

    table = pt.table(measurement_set, readonly=True, ack=False)
    ms_interval = table.getcell("INTERVAL", 0)

    n_intervals_per_chunk = int(np.ceil(interval / ms_interval))

    return n_intervals_per_chunk


def get_start_times(ms_path, n_intervals=1):
    """
    Reads the values from the column TIME of main table of
    a measurement set, split the values in n_intervals
    and returns a list with the first time of each interval.
    """

    # Verify that number_of_intervals is an equal or
    # larger than one.
    if n_intervals < 1:
        raise RuntimeError(
            "number_of_intervals must be a positive "
            "integer equal or larger than 1, but it "
            "is:" + str(n_intervals)
        )

    # Read the unique values of column TIME.
    times = taql(f"SELECT UNIQUE(TIME) FROM {ms_path}").getcol("TIME")

    mod_jul_sec_list = []
    for i in range(n_intervals):
        index = int((i * len(times)) / n_intervals)
        mod_jul_sec_list.append(times[index])

    return mod_jul_sec_list


def compute_observation_width(observation):
    """
    Computes the width of an observation, using its
    reference frequency, data and elevation values.

    Returns: A dictionary containing "ra" and "dec" values, in degrees.
    """

    # Compute Full Width at Half Maximum (FWHM) value.
    fwhm_degrees = math.degrees(
        1.1 * speed_of_light / observation["ref_freq"] / observation["diam"]
    )

    # Derive observation widths from the FWHM.
    # 1.7 is an instrument-specific multiplier for the FWHM.
    # 1.7 is the value applicable to LOFAR.
    return {
        "ra": 1.7 * fwhm_degrees,
        "dec": 1.7 * fwhm_degrees / math.sin(observation["mean_el_rad"]),
    }
