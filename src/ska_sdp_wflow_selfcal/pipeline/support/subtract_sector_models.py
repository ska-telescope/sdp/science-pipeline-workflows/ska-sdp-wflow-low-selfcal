#  SPDX-License-Identifier: GPL-3.0-or-later
# Rapthor: LOFAR DDE Pipeline
# Copyright (C) 2023, Team Rapthor
# https://git.astron.nl/RD/rapthor/-/blob/master/LICENSE

# This file is copied from the Rapthor repository (with minor changes):
# https://git.astron.nl/RD/rapthor/-/tree/master/rapthor/scripts/subtract_sector_models.py

# pylint: skip-file

"""
Script to subtract sector model data
"""
import os
import subprocess
import sys

import casacore.tables as pt
import dateutil.parser
import numpy as np
from astropy.time import Time

from ska_sdp_wflow_selfcal.pipeline.support.miscellaneous import (
    approx_equal,
    delete_directory,
    string2bool,
)


def get_nchunks(
    msin, nsectors, fraction=1.0, reweight=False, compressed=False
):
    """
    Determines number of chunks for available memory of node

    Parameters
    ----------
    msin : str
        Input MS file name
    nsectors : int
        Number of imaging sectors
    fraction : float
        Fraction of MS file to be read
    reweight: bool
        True if reweighting is to be done
    compressed: bool
        True if data are compressed (by Dysco)

    Returns
    -------
    nchunks : int
        Number of chunks
    """
    if reweight:
        scale_rapthor = 10.0
    else:
        scale_rapthor = 4.0
    if compressed:
        scale_rapthor *= 5.0
    tot_m, used_m, free_m = list(
        map(int, os.popen("free -tm").readlines()[-1].split()[1:])
    )
    msin_m = (
        float(subprocess.check_output(["du", "-smL", msin]).split()[0])
        * fraction
    )
    tot_required_m = msin_m * nsectors * scale_rapthor * 2.0
    nchunks = max(1, int(np.ceil(tot_required_m / tot_m)))
    return nchunks


def convert_mjd2mvt(mjd_sec):
    """
    Converts MJD to casacore MVTime

    Parameters
    ----------
    mjd_sec : float
        MJD time in seconds

    Returns
    -------
    mvtime : str
        Casacore MVTime string
    """
    t = Time(mjd_sec / 3600 / 24, format="mjd", scale="utc")
    date, hour = t.iso.split(" ")
    year, month, day = date.split("-")
    d = t.datetime
    month = d.ctime().split(" ")[1]

    return "{0}{1}{2}/{3}".format(day, month, year, hour)


def convert_mvt2mjd(mvt_str):
    """
    Converts casacore MVTime to MJD

    Parameters
    ----------
    mvt_str : str
        MVTime time

    Returns
    -------
    mjdtime : float
        MJD time in seconds
    """
    day_str = mvt_str.split("/")[0].lower()
    months = {
        "jan": 1,
        "feb": 2,
        "mar": 3,
        "apr": 4,
        "may": 5,
        "jun": 6,
        "jul": 7,
        "aug": 8,
        "sep": 9,
        "oct": 10,
        "nov": 11,
        "dec": 12,
    }
    for m in months:
        if m in day_str:
            p = day_str.split(m)
            day_str = "{0}.{1}.{2}".format(p[1], months[m], p[0])
    time_str = mvt_str.split("/")[1]
    t = dateutil.parser.parse("{0}/{1}".format(day_str, time_str))

    return Time(t).mjd * 3600 * 24


def subtract_sector_models(
    msin,
    msout,
    model_list,
    msin_column="DATA",
    model_column="DATA",
    out_column="DATA",
    nr_outliers=0,
    nr_bright=0,
    use_compression=False,
    reweight=True,
    starttime=None,
    phaseonly=True,
):
    """
    Subtract sector model data

    Parameters
    ----------
    msin : str
        Name of MS file from which subtraction will be done
    msout: str
        Name of MS file that will contain the output.
        This function overwrites any existing directory with this name.
    model_list: list
        List of model data MS filenames
    msin_column : str, optional
        Name of input column
    model_column : str, optional
        Name of model column
    out_column : str, optional
        Name of output column
    nr_outliers : int, optional
        Number of outlier sectors. Outlier sectors must be given after normal
        sectors and bright-source sectors in msmod_list
    nr_bright : int, optional
        Number of bright-source sectors. Bright-source sectors must be given
        after normal sectors but before outlier sectors in msmod_list
    use_compression : bool, optional
        If True, use Dysco compression
    reweight : bool, optional
        If True, reweight using the residuals
    starttime : str, optional
        Start time in JD seconds
    phaseonly : bool
        Reweight with phases only
    """
    use_compression = string2bool(use_compression)
    phaseonly = string2bool(phaseonly)
    reweight = string2bool(reweight)

    # Get the model data filenames, filtering any that do not have the right
    # start time
    if starttime is not None:
        # Filter the list of models to include only ones for the given times
        nrows_list = []
        filtered_model_list = []
        for model_ms in model_list:
            tin = pt.table(model_ms, readonly=True, ack=False)
            starttime_chunk = np.min(tin.getcol("TIME"))
            interval = tin.getcol("INTERVAL")[0]
            # Only include files with start times that are within 1 interval of
            # the specified starttime. Multiplying by 1.01 adds 1% tolerance.
            if approx_equal(
                starttime_chunk,
                convert_mvt2mjd(starttime),
                tol=interval * 1.01,
            ):
                nrows_list.append(tin.nrows())
                filtered_model_list.append(model_ms)
                starttime_exact = convert_mjd2mvt(
                    starttime_chunk
                )  # store exact value for use later
            tin.close()
        model_list = filtered_model_list
        if len(set(nrows_list)) > 1:
            print(
                "subtract_sector_models: Model data files have differing "
                "number of rows..."
            )
            sys.exit(1)

    # In case the user did not concatenate LINC output and fed multiple
    # frequency bands, find the correct frequency band.
    chan_freqs = pt.table(msin + "/SPECTRAL_WINDOW").getcol("CHAN_FREQ")
    filtered_model_list = []
    for model_ms in model_list:
        chan_freqs_model = pt.table(model_ms + "/SPECTRAL_WINDOW").getcol(
            "CHAN_FREQ"
        )
        if np.allclose(chan_freqs_model, chan_freqs):
            filtered_model_list.append(model_ms)
    model_list = filtered_model_list

    nsectors = len(model_list)
    if nsectors == 0:
        print("subtract_sector_models: No model data found. Exiting...")
        sys.exit(1)
    print("subtract_sector_models: Found {} model data files".format(nsectors))
    for m in model_list:
        print(m)

    # Define the template MS file. This file is copied to one or more files
    # to be filled with new data
    ms_template = model_list[0]

    # If starttime is given, figure out startrow and nrows for input MS file
    tin = pt.table(msin, readonly=True, ack=False)
    if starttime is not None:
        tapprox = convert_mvt2mjd(starttime_exact) - 100.0
        approx_indx = np.where(tin.getcol("TIME") > tapprox)[0][0]
        for tind, t in enumerate(tin.getcol("TIME")[approx_indx:]):
            if convert_mjd2mvt(t) == starttime_exact:
                startrow_in = tind + approx_indx
                break
        nrows_in = nrows_list[0]
    else:
        startrow_in = 0
        nrows_in = tin.nrows()
    tin.close()

    # Peel outliers if desired.
    if nr_outliers > 0:
        if len(model_list) != nr_outliers:
            raise RuntimeError("Internal error: Unexpected model list size")

        # Open input and output table
        tin = pt.table(msin, readonly=True, ack=False)

        # Use subprocess to call 'cp' to ensure that the copied version has
        # the default permissions (e.g., so it's not read only)
        # TODO: Check for existence of `msout` could be removed. It should
        # always be created in a different temporary directory by the CWL
        # runner. If we don't trust the CWL runner, we might bail out if
        # `msout` exists.
        if os.path.exists(msout):
            # File may exist from a previous iteration; delete it if so
            delete_directory(msout)
        subprocess.check_call(
            ["cp", "-r", "-L", "--no-preserve=mode", ms_template, msout]
        )
        tout = pt.table(msout, readonly=False, ack=False)

        # Define chunks based on available memory
        fraction = float(nrows_in) / float(tin.nrows())
        nchunks = get_nchunks(msin, nr_outliers, fraction, compressed=True)
        nrows_per_chunk = int(nrows_in / nchunks)
        startrows_tin = [startrow_in]
        startrows_tmod = [0]
        nrows = [nrows_per_chunk]
        for i in range(1, nchunks):
            if i == nchunks - 1:
                nrow = nrows_in - (nchunks - 1) * nrows_per_chunk
            else:
                nrow = nrows_per_chunk
            nrows.append(nrow)
            startrows_tin.append(startrows_tin[i - 1] + nrows[i - 1])
            startrows_tmod.append(startrows_tmod[i - 1] + nrows[i - 1])
        print(
            "subtract_sector_models: Using {} chunk(s) for peeling of "
            "outliers".format(nchunks)
        )

        for c, (startrow_tin, startrow_tmod, nrow) in enumerate(
            zip(startrows_tin, startrows_tmod, nrows)
        ):
            # For each chunk, load data
            datain = tin.getcol(msin_column, startrow=startrow_tin, nrow=nrow)
            if use_compression:
                # Replace flagged values with NaNs before compression
                flags = tin.getcol("FLAG", startrow=startrow_tin, nrow=nrow)
                flagged = np.where(flags)
                datain[flagged] = np.NaN
            datamod_list = []
            for i, model_ms in enumerate(model_list[nsectors - nr_outliers :]):
                tmod = pt.table(model_ms, readonly=True, ack=False)
                datamod_list.append(
                    tmod.getcol(
                        model_column, startrow=startrow_tmod, nrow=nrow
                    )
                )
                tmod.close()

            # Subtract sum of model data for this chunk
            other_sectors_ind = list(range(nr_outliers))
            datamod_all = None
            for sector_ind in other_sectors_ind:
                if datamod_all is None:
                    datamod_all = datamod_list[sector_ind].copy()
                else:
                    datamod_all += datamod_list[sector_ind]
            tout.putcol(
                out_column,
                datain - datamod_all,
                startrow=startrow_tmod,
                nrow=nrow,
            )
            tout.flush()
        tout.close()
        tin.close()

    # Peel the bright sources if desired.
    elif nr_bright > 0:
        if len(model_list) != nr_bright:
            raise RuntimeError("Internal error: Unexpected model list size")

        # Open input and output table
        tin = pt.table(msin, readonly=True, ack=False)

        # Use subprocess to call 'cp' to ensure that the copied version has the
        # default permissions (e.g., so it's not read only)
        # TODO: Check for existence of `msout` could be removed. It should
        # always be created in a different temporary directory by the CWL
        # runner. If we don't trust the CWL runner, we might bail out if
        # `msout` exists.
        if os.path.exists(msout):
            # File may exist from a previous iteration; delete it if so
            delete_directory(msout)
        subprocess.check_call(
            ["cp", "-r", "-L", "--no-preserve=mode", ms_template, msout]
        )
        tout = pt.table(msout, readonly=False, ack=False)

        # Define chunks based on available memory
        fraction = float(nrows_in) / float(tin.nrows())
        nchunks = get_nchunks(msin, nr_bright, fraction, compressed=True)
        nrows_per_chunk = int(nrows_in / nchunks)
        startrows_tin = [startrow_in]
        startrows_tmod = [0]
        nrows = [nrows_per_chunk]
        for i in range(1, nchunks):
            if i == nchunks - 1:
                nrow = nrows_in - (nchunks - 1) * nrows_per_chunk
            else:
                nrow = nrows_per_chunk
            nrows.append(nrow)
            startrows_tin.append(startrows_tin[i - 1] + nrows[i - 1])
            startrows_tmod.append(startrows_tmod[i - 1] + nrows[i - 1])
        print(
            "subtract_sector_models: Using {} chunk(s) for peeling of bright "
            "sources".format(nchunks)
        )

        for c, (startrow_tin, startrow_tmod, nrow) in enumerate(
            zip(startrows_tin, startrows_tmod, nrows)
        ):
            # For each chunk, load data
            datain = tin.getcol(msin_column, startrow=startrow_tin, nrow=nrow)
            if use_compression:
                # Replace flagged values with NaNs before compression
                flags = tin.getcol("FLAG", startrow=startrow_tin, nrow=nrow)
                flagged = np.where(flags)
                datain[flagged] = np.NaN
            datamod_list = []
            for i, model_ms in enumerate(model_list[nsectors - nr_bright :]):
                tmod = pt.table(model_ms, readonly=True, ack=False)
                datamod_list.append(
                    tmod.getcol(
                        model_column, startrow=startrow_tmod, nrow=nrow
                    )
                )
                tmod.close()

            # Subtract sum of model data for this chunk
            other_sectors_ind = list(range(nr_bright))
            datamod_all = None
            for sector_ind in other_sectors_ind:
                if datamod_all is None:
                    datamod_all = datamod_list[sector_ind].copy()
                else:
                    datamod_all += datamod_list[sector_ind]
            tout.putcol(
                out_column,
                datain - datamod_all,
                startrow=startrow_tmod,
                nrow=nrow,
            )
            tout.flush()
        tout.close()
        tin.close()
