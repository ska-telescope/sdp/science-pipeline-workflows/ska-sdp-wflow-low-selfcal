#  SPDX-License-Identifier: GPL-3.0-or-later
# Rapthor: LOFAR DDE Pipeline
# Copyright (C) 2023, Team Rapthor
# https://git.astron.nl/RD/rapthor/-/blob/master/LICENSE

"""
Module that holds miscellaneous functions and classes
"""

# This file is copied from the Rapthor repository (with minor changes):
# https://git.astron.nl/RD/rapthor/-/tree/master/rapthor/lib/miscellaneous.py

import logging
import os
import pickle

import lsmtool
import numpy as np
import requests
from shapely.geometry import Polygon

from ska_sdp_wflow_selfcal.pipeline.support.facet import radec2xy, xy2radec
from ska_sdp_wflow_selfcal.pipeline.support.miscellaneous import make_wcs
from ska_sdp_wflow_selfcal.pipeline.support.read_data import (
    compute_observation_width,
    get_observation_params,
    get_phase_center,
)


def download_skymodel(  # pylint: disable=too-many-arguments
    ra,
    dec,
    skymodel_path,
    radius=5.0,
    overwrite=False,
    source="TGSS",
    targetname="Patch",
):
    """
    Download the skymodel for the target field

    Parameters
    ----------
    ra : float
        Right ascension of the skymodel centre.
    dec : float
        Declination of the skymodel centre.
    skymodel_path : str
        Full name (with path) to the skymodel.
    radius : float
        Radius for the TGSS/GSM cone search in degrees.
    source : str
        Source where to obtain a skymodel from. Can be TGSS or GSM. Default is
        TGSS.
    overwrite : bool
        Overwrite the existing skymodel pointed to by skymodel_path.
    target_name : str
        Give the patch a certain name. Default is "Patch".

    Returns
    ---------
    skymodel: LSMTool object containing the downloaded skymodel.
    """

    sky_servers = {
        "TGSS": "http://tgssadr.strw.leidenuniv.nl/cgi-bin/gsmv5.cgi?coord="
        "{ra:f},{dec:f}&radius={radius:f}&unit=deg&deconv=y",
        "GSM": "https://lcs165.lofar.eu/cgi-bin/gsmv1.cgi?coord="
        "{ra:f},{dec:f}&radius={radius:f}&unit=deg&deconv=y",
    }
    if source.upper() not in sky_servers:
        raise ValueError(
            "Unsupported skymodel source specified! Please use TGSS or GSM."
        )

    rapthor_logger = logging.getLogger("rapthor:skymodel")

    file_downloaded = os.path.isfile(skymodel_path)
    if file_downloaded and not overwrite:
        rapthor_logger.warning(
            "Skymodel %s exists and overwrite is set to False! Not "
            "downloading skymodel. If this is a restart this may be "
            "intentional.",
            skymodel_path,
        )
        return lsmtool.load(skymodel_path)

    if (not file_downloaded) and os.path.exists(skymodel_path):
        msg = f"Path {skymodel_path} exists but is not a file!"
        rapthor_logger.error(msg)
        raise ValueError(msg)

    # Empty strings are False. Only attempt directory creation if there is a
    # directory path involved.
    if (
        (not file_downloaded)
        and os.path.dirname(skymodel_path)
        and (not os.path.exists(os.path.dirname(skymodel_path)))
    ):
        os.makedirs(os.path.dirname(skymodel_path))

    if file_downloaded and overwrite:
        rapthor_logger.warning(
            "Found existing skymodel %s and overwrite is True. Deleting "
            "existing skymodel!",
            skymodel_path,
        )
        os.remove(skymodel_path)

    url = sky_servers[source].format(ra=ra, dec=dec, radius=radius)
    rapthor_logger.info(
        'Downloading skymodel for the target into "%s"', skymodel_path
    )
    rapthor_logger.info("Skymodel URL: %s", url)
    response = requests.get(url, timeout=20)
    with open(skymodel_path, "wb") as skymodel_file:
        skymodel_file.write(response.content)

    if not os.path.isfile(skymodel_path):
        rapthor_logger.critical(
            "Skymodel %s does not exist after trying to download the "
            "skymodel.",
            skymodel_path,
        )
        raise IOError(
            f"Skymodel {skymodel_path} does not exist after trying to \
            download the skymodel."
        )

    # Treat all sources as one group (direction)
    skymodel = lsmtool.load(skymodel_path)
    skymodel.group("single", root=targetname)
    skymodel.write(clobber=True)

    return skymodel


def group_sources(  # pylint: disable=too-many-locals
    skymodel,
    target_flux=1.0,
    max_directions=10,
    apply_beam=False,
):
    """
    Groups sources into patches (i.e. into directions) based on target_flux and
    max_directions.
    Based on Rapthor field.py, function make_skymodels().

    Parameters
    ----------
    skymodel: lsmtool.skymodel.SkyModel, modified in-place.
        True-flux or apparent-flux sky model, containing all sources.
        The function groups the sources in the model and applies the beam if
        needed, so it becomes a true-flux sky model.
    apply_beam: bool
        True: 'skymodel' has apparent fluxes, apply the beam.
        False: 'skymodel' has true fluxes, do not apply the beam.

    Returns
    -------
    bright_source_apparent_skymodel: lsmtool.skymodel.SkyModel
        Apparent-flux sky model, containing bright sources only.
    """

    if (
        "Patch" not in skymodel.table.keys()
        or len(set(skymodel.table["Patch"])) == 1
    ):
        # When the sources in the skymodel have not been divided into patches
        # yet, divide them into preliminary patches first with a cheaper
        # algorithm. This situation occurs for the initial skymodel or when
        # filter_skymodel of the previous imaging stage was disabled.
        skymodel.group("threshold", FWHM="40.0 arcsec", threshold=0.05)
    skymodel.group(
        "meanshift",
        byPatch=True,
        applyBeam=apply_beam,
        lookDistance=0.075,
        groupingDistance=0.01,
    )
    skymodel.setPatchPositions(method="wmean")

    bright_source_apparent_skymodel = skymodel.copy()

    fluxes = skymodel.getColValues("I", aggregate="sum", applyBeam=apply_beam)
    if max_directions >= len(fluxes):
        max_directions = len(fluxes)
        target_flux_for_number = np.min(fluxes)
    else:
        # Weight the fluxes by size to estimate the required target flux.
        # The same weighting scheme is used by LSMTool when performing
        # the 'voronoi' grouping later.
        sizes = skymodel.getPatchSizes(
            units="arcsec",
            weight=True,
            applyBeam=apply_beam,
        )

        sizes[sizes < 1.0] = 1.0
        if target_flux is not None:
            trial_target_flux = target_flux
        else:
            trial_target_flux = 0.3

        trial_number = 0
        for _ in range(100):
            if trial_number == max_directions:
                break
            trial_fluxes = fluxes.copy()
            bright_ind = np.where(trial_fluxes >= trial_target_flux)
            median_size = np.median(sizes[bright_ind])
            weights = median_size / sizes
            weights[weights > 1.0] = 1.0
            weights[weights < 0.5] = 0.5
            trial_fluxes *= weights
            trial_fluxes.sort()
            trial_number = len(trial_fluxes[trial_fluxes >= trial_target_flux])
            if trial_number < max_directions:
                # Reduce the trial target flux to next fainter source
                trial_target_flux = trial_fluxes[-(trial_number + 1)]
            elif trial_number > max_directions:
                # Increase the trial flux to next brighter source
                trial_target_flux = trial_fluxes[-(trial_number - 1)]
        target_flux_for_number = trial_target_flux

    if target_flux_for_number > target_flux and max_directions < len(fluxes):
        # Only use the new target flux if the old value might result
        # in more than target_number of calibrators.
        target_flux = target_flux_for_number

    # Tesselate the model.
    skymodel.group(
        "voronoi",
        targetFlux=target_flux,
        applyBeam=apply_beam,
        weightBySize=True,
    )

    return bright_source_apparent_skymodel


def discard_sources_outside_polygon(skymodel, wcs, poly):
    """
    Filters the sky model to select only sources that lie inside the sector

    Parameters
    ----------
    skymodel : lsmtool.skymodel.SkyModel, modified in-place
        The sky model that will be filtered.
    wcs: astropy.wcs.WCS
        Current world coordinate system.
    poly: shapely.geometry.Polygon
        Defines the area in which the sources should be kept.
        Outside of these boundaries the sources are filtered.
    """
    # Make list of sources
    ra = skymodel.getColValues("Ra")
    dec = skymodel.getColValues("Dec")

    x, y = radec2xy(wcs, ra, dec)
    x = np.array(x)
    y = np.array(y)

    # Keep only those sources inside the sector bounding box
    inside = np.zeros(len(skymodel), dtype=bool)
    xmin, ymin, xmax, ymax = poly.bounds
    inside_ind = np.where(
        (x >= xmin) & (x <= xmax) & (y >= ymin) & (y <= ymax)
    )
    inside[inside_ind] = True
    skymodel.select(inside)


def define_sector(  # pylint: disable=too-many-locals
    path_to_grouped_skymodel,
    sector_skymodel_filename,
    input_ms,
    vertices_filename,
):
    """Create a sector skymodel only containing all the sources lying in
    the desired imaging field

    Parameters
    ----------
    path_to_grouped_skymodel : path to LSMTool skymodel object
        Input sky model. This is the skymodel used for calibration.
    sector_skymodel_filename: path to output sector skymodel
    input_ms: path to input measurement set
    vertices_filename: path to file that will contain the vertices

    Returns
    -------
    vertices : np.array
        sector vertices
    """

    [phase_center_ra, phase_center_dec] = get_phase_center(input_ms)

    wcs = make_wcs(phase_center_ra, phase_center_dec)

    observation = get_observation_params(input_ms)
    width = compute_observation_width(observation)

    # Determine the vertices of the sector polygon

    # Define initial polygon as a rectangle
    center_x, center_y = radec2xy(wcs, [phase_center_ra], [phase_center_dec])

    ra_width_pix = width["ra"] / abs(wcs.wcs.cdelt[0])
    dec_width_pix = width["dec"] / abs(wcs.wcs.cdelt[1])
    x_0 = center_x[0] - ra_width_pix / 2.0
    y_0 = center_y[0] - dec_width_pix / 2.0
    poly_verts = [
        (x_0, y_0),
        (x_0, y_0 + dec_width_pix),
        (x_0 + ra_width_pix, y_0 + dec_width_pix),
        (x_0 + ra_width_pix, y_0),
        (x_0, y_0),
    ]
    poly = Polygon(poly_verts)

    vertices_x = poly.exterior.coords.xy[0].tolist()
    vertices_y = poly.exterior.coords.xy[1].tolist()
    ra, dec = xy2radec(wcs, vertices_x, vertices_y)
    vertices = [np.array(ra), np.array(dec)]
    with open(vertices_filename, "wb") as vertices_file:
        pickle.dump(vertices, vertices_file)

    skymodel = lsmtool.load(path_to_grouped_skymodel, beamMS=input_ms)
    discard_sources_outside_polygon(skymodel, wcs, poly)
    skymodel.write(sector_skymodel_filename, clobber=True)

    return zip(vertices_x, vertices_y)


def get_outliers(full_sky, sector_skymodel, outliers_sky):
    """
    Make a sky model of all the outlier sources in the full_sky not included
    in the sector_skymodel
    """
    full_skymodel = lsmtool.load(str(full_sky))
    all_source_names = full_skymodel.getColValues("Name").tolist()
    sector_source_names = []

    skymodel = lsmtool.load(str(sector_skymodel))
    sector_source_names.extend(skymodel.getColValues("Name").tolist())

    outlier_ind = np.array(
        [
            all_source_names.index(sn)
            for sn in all_source_names
            if sn not in sector_source_names
        ]
    )
    outlier_skymodel = full_skymodel.copy()
    outlier_skymodel.select(outlier_ind, force=True)

    outlier_skymodel.write(outliers_sky, clobber=True)


def transfer_patches(from_skymodel, to_skymodel):
    """
    Transfers the patches defined in from_skymodel to to_skymodel.

    Parameters
    ----------
    from_skymodel : lsmtool.skymodel.SkyModel
        Sky model from which to transfer patches.
    to_skymodel : lsmtool.skymodel.SkyModel, modified in-place
        Sky model to which to transfer patches.
    """
    names_from = from_skymodel.getColValues("Name").tolist()
    names_to = to_skymodel.getColValues("Name").tolist()

    if set(names_from) == set(names_to):
        # Both sky models have the same sources, so use indexing
        ind_ss = np.argsort(names_from)
        ind_ts = np.argsort(names_to)
        to_skymodel.table["Patch"][ind_ts] = from_skymodel.table["Patch"][
            ind_ss
        ]
        to_skymodel._updateGroups()  # pylint: disable=protected-access
    elif set(names_to).issubset(set(names_from)):
        # The to_skymodel is a subset of from_skymodel, so use slower matching
        # algorithm
        for ind_ts, name in enumerate(names_to):
            ind_ss = names_from.index(name)
            to_skymodel.table["Patch"][ind_ts] = from_skymodel.table["Patch"][
                ind_ss
            ]
    else:
        # Skymodels don't match, raise error
        raise ValueError(
            "Cannot transfer patches since from_skymodel does not contain "
            "all the sources in to_skymodel"
        )


def get_bright_sources(
    true_skymodel,
    bright_source_apparent_skymodel,
    input_ms,
    sector_vertices,
):
    """
    Generate a sky model with true fluxes containing only bright sources.

    Parameters
    ----------
    true_skymodel: lsmtool.skymodel.SkyModel
        True-flux sky model, containing all sources.
    bright_source_apparent_skymodel: lsmtool.skymodel.SkyModel,
                                     modified in-place
        Apparent-flux sky model, containing bright sources only.
        Applies the selection and patch positions of this sky model.
    input_ms: str
        Input measurement set, for extracting the phase center.
    sector_vertices:
        Discard sources outside the polygon defined by these vertices.

    Returns
    -------
    bright_source_true_skymodel: lsmtool.skymodel.SkyModel
        The generated true-flux sky model containing bright sources only.
    """

    # Match the bright-source apparent-sky model to the tessellated one by
    # removing patches that are not present in the tessellated model.
    bright_patch_names = bright_source_apparent_skymodel.getPatchNames()
    true_patch_names = true_skymodel.getPatchNames()
    for patch_name in bright_patch_names:
        if patch_name not in true_patch_names:
            bright_source_apparent_skymodel.remove(f"Patch == {patch_name}")

    # Initialize the bright-source true-sky model using the true-sky model.
    bright_source_true_skymodel = true_skymodel.copy()

    # For the bright-source true-sky model, duplicate any selections made
    # to the bright-source apparent-sky model.
    source_names = bright_source_true_skymodel.getColValues("Name").tolist()
    bright_source_names = bright_source_apparent_skymodel.getColValues(
        "Name"
    ).tolist()
    matching_ind = []
    for source_name in bright_source_names:
        matching_ind.append(source_names.index(source_name))
    bright_source_true_skymodel.select(np.array(matching_ind))

    # Transfer patches and patch positions to the bright-source true-sky model.
    # (Source names are identical in both, but the order may be different.)
    transfer_patches(
        bright_source_apparent_skymodel, bright_source_true_skymodel
    )
    bright_source_true_skymodel.setPatchPositions(
        patchDict=bright_source_apparent_skymodel.getPatchPositions()
    )

    # Now remove any bright sources that lie outside the imaged area, as they
    # should not be peeled.
    [phase_center_ra, phase_center_dec] = get_phase_center(input_ms)
    wcs = make_wcs(phase_center_ra, phase_center_dec)
    discard_sources_outside_polygon(
        bright_source_true_skymodel, wcs, Polygon(sector_vertices)
    )

    # Recreate patch positions / Remove patch positions for removed patches.
    if len(bright_source_true_skymodel) > 0:
        bright_source_true_skymodel.setPatchPositions()

    return bright_source_true_skymodel


def split_skymodel_file(input_skymodel, filename_prefix):
    """
    Splits a skymodel into different files with a minimum of 100 sources each.
    Generates a single file if input_skymodel contains less than 100 sources.
    """
    source_sectors = []
    nsources = len(input_skymodel)
    if nsources > 0:
        # Choose number of sectors to be the no more than ten, but don't
        # allow fewer than 100 sources per sector if possible
        nnodes = max(
            min(10, round(nsources / 100)), 1
        )  # TODO: tune to number of available nodes and/or memory?
        for i in range(nnodes):
            split_skymodel = input_skymodel.copy()
            startind = i * int(nsources / nnodes)
            if i == nnodes - 1:
                endind = nsources
            else:
                endind = startind + int(nsources / nnodes)
            split_skymodel.select(np.array(list(range(startind, endind))))
            split_skymodel.write(
                f"{filename_prefix}_{i}.skymodel", clobber=True
            )
            source_sectors.append(f"{filename_prefix}_{i}.skymodel")

    # Also save the entire skymodel, as it will be used in wsclean restore
    # operation
    input_skymodel.write(f"{filename_prefix}.skymodel", clobber=True)

    return source_sectors


def process_skymodels(
    calibrate_config,
    input_ms,
    vertices_filename,
    sector_skymodel_filename,
    skymodel_true_filename,
    calibrate_skymodel_filename,
    skymodel_apparent_filename=None,
    bright_sources_skymodel_prefix=None,
):  # pylint: disable=too-many-arguments
    """
    This function generates skymodels necessary for each self-calibration
    loop. This function groups sources, defines vertices, and handles bright
    source skymodels based on the loop index and specified parameters.

    Parameters
    ----------
    calibrate_config: dict
        Settings for source grouping, including target_flux and max_directions.
    input_ms : str
        Measurement set (MS) file containing visibilities.
    vertices_filename : str
        Filename for storing vertices information.
    sector_skymodel_filename : str
        Filename for the sector sky model.
    skymodel_true_filename : str
        Filename for the true-flux sky model.
    calibrate_skymodel_filename : str
        Filename for the sky model to use as input for calibration.
    skymodel_apparent_filename : str, optional
        Filename for the apparent-flux sky model. Defaults to None.
    bright_sources_skymodel_prefix : str, optional
        Prefix for the bright sources sky model filenames. Defaults to None.
        If None, do not create a sky model with the bright sources.
    """

    logger = logging.getLogger("ska-sdp-wflow-selfcal")

    logger.info("run_pipeline:: Group sources")

    skymodel = lsmtool.load(
        skymodel_apparent_filename or skymodel_true_filename, input_ms
    )
    bright_source_apparent_skymodel = group_sources(
        skymodel,
        target_flux=calibrate_config["target_flux"],
        max_directions=calibrate_config["max_directions"],
        apply_beam=skymodel_apparent_filename is None,
    )

    # group_sources ensures that the sky model contains true fluxes.
    true_skymodel = skymodel

    true_skymodel.write(calibrate_skymodel_filename, clobber=True)

    logger.info("run_pipeline:: Define vertices")
    sector_vertices = define_sector(
        calibrate_skymodel_filename,
        sector_skymodel_filename,
        input_ms,
        vertices_filename,
    )

    if bright_sources_skymodel_prefix:
        logger.info("run_pipeline:: Create bright sources sky model")
        bright_sources_skymodel = get_bright_sources(
            true_skymodel,
            bright_source_apparent_skymodel,
            input_ms,
            sector_vertices,
        )
        logger.info("run_pipeline:: Split bright sources sky model file")
        split_skymodel_file(
            bright_sources_skymodel, bright_sources_skymodel_prefix
        )


def create_full_sky_model(
    initial_skymodel_filename: str,
    image_skymodel_filename: str,
    result_skymodel_filename: str,
):
    """
    Creates a sky model for the full sky, including outliers, by adding
    the outliers from an initial full-sky model to a skymodel that covers
    the image area only.

    This function is based on the `update_skymodels()` function from the
    Rapthor repository in field.py.

    Parameters
    ----------
    initial_skymodel_filename: str
        Filename of the initial skymodel, which covers the full sky.
    image_skymodel_filename: str
        Filename for a skymodel which covers the image area only.
    result_skymodel_filename:
        Filename for the result full-sky model.
        The function stores the created skymodel in this file.
    """

    # Load initial sky model and regroup to one patch per entry to ensure any
    # existing patches are removed. Otherwise they may propagate to the DDE
    # direction determination, leading to unexpected results.
    initial_skymodel = lsmtool.load(initial_skymodel_filename)
    initial_skymodel.group("every")

    # Concatenate by position. Any entries in the initial sky model that match
    # one or more entries in the new one will be removed. Use a fairly large
    # matching radius, which favors entries in the new model over those in the
    # initial one (i.e., ones from the initial model are only included if they
    # are far from any in the new model and thus not likely to be duplicates).
    matching_radius_deg = 30.0 / 3600.0  # => 30 arcsec
    skymodel = lsmtool.load(image_skymodel_filename)
    skymodel.concatenate(
        initial_skymodel,
        matchBy="position",
        radius=matching_radius_deg,
        keep="from1",
    )
    skymodel.setPatchPositions()
    skymodel.write(result_skymodel_filename, clobber=True)
