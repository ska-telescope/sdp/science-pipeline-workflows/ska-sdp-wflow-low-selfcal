#  SPDX-License-Identifier: GPL-3.0-or-later
# Rapthor: LOFAR DDE Pipeline
# Copyright (C) 2023, Team Rapthor
# https://git.astron.nl/RD/rapthor/-/blob/master/LICENSE

"""
Script to combine two h5parms
"""

# This file is copied from the Rapthor repository (with minor changes):
# https://git.astron.nl/RD/rapthor/-/tree/master/rapthor/scripts/combine_h5parms.py
# pylint: skip-file
import argparse
import os
import shutil
import tempfile
from argparse import RawTextHelpFormatter

import numpy as np
import scipy.interpolate as si
from astropy.stats import circmean
from losoto.h5parm import h5parm


def expand_array(array, new_shape, new_axis_ind):
    """
    Expands an array along an axis

    Parameters
    ----------
    array : array
        Array to expand
    new_shape : list
        New shape of expanded array
    new_axis_ind : int
        Index of the axis to expand

    Returns
    -------
    new_array : array
        The expanded array
    """
    new_array = np.zeros(new_shape)
    slc = [slice(None)] * len(new_shape)
    for i in range(new_shape[new_axis_ind]):
        slc[new_axis_ind] = slice(i, i + 1)
        new_array[tuple(slc)] = array

    return new_array


def average_polarizations(soltab):
    """
    Average solutions over XX and YY polarizations

    Note: phases are averaged using the circular mean and
    amplitudes are averaged in log space

    Parameters
    ----------
    soltab : soltab
        Solution table with the solutions to be averaged

    Returns
    -------
    vals : array
        The averaged values
    weights : array
        The averaged weights
    """
    # Read in various values
    pol_ind = soltab.getAxesNames().index("pol")
    vals = soltab.val[:]
    weights = soltab.weight[:]

    # Make sure flagged solutions are NaN and have zero weight
    flagged_ind = np.logical_or(~np.isfinite(vals), weights == 0.0)
    vals[flagged_ind] = np.nan
    weights[flagged_ind] = 0.0

    # Average the values over the polarization axis if desired. For the
    # weights, take the min value over the polarization axis
    if soltab.getType() == "phase":
        # Use the circmean to get the average (circmean does not ignore NaNs,
        # so set flagged values to zero first)
        vals[flagged_ind] = 0.0
        vals = circmean(vals, axis=pol_ind, weights=weights)
    elif soltab.getType() == "amplitude":
        # Take the average in log space. Use nanmean to get the average
        # (nanmean ignores NaNs)
        vals = np.log10(vals)
        vals = np.nanmean(vals, axis=pol_ind)
        vals = 10**vals
    else:
        # Use nanmean to get the average (nanmean ignores NaNs)
        vals = np.nanmean(vals, axis=pol_ind)
    weights = np.min(weights, axis=pol_ind)

    return vals, weights


def interpolate_solutions(
    fast_soltab,
    slow_soltab,
    final_axes_shapes,
    slow_vals=None,
    slow_weights=None,
):
    """
    Interpolates slow phases or amplitudes to the fast time and frequency grid

    Parameters
    ----------
    fast_soltab : soltab
        Solution table with fast solutions
    slow_soltab : soltab
        Solution table with slow solutions
    final_axes_shapes : list of int
        Final shape of the output arrays
    slow_vals : array, optional
        Array of values to use for the slow solutions (useful if averaging
        has been done)
    slow_weights : array, optional
        Array of weights to use for the slow solutions (useful if averaging
        has been done)

    Returns
    -------
    vals_interp : array
        The interpolated values
    weights_interp : array
        The interpolated weights
    """
    # Read in various values
    time_ind = fast_soltab.getAxesNames().index("time")
    freq_ind = fast_soltab.getAxesNames().index("freq")
    if slow_vals is None:
        slow_vals = slow_soltab.val[:]
    if slow_weights is None:
        slow_weights = slow_soltab.weight[:]

    # Make sure flagged solutions have zero weight and fill them with 0 for
    # phases and 1 for amplitudes to avoid NaNs
    flagged_ind2 = np.logical_or(~np.isfinite(slow_vals), slow_weights == 0.0)
    if slow_soltab.getType() == "phase":
        slow_vals[flagged_ind2] = 0.0
    elif slow_soltab.getType() == "amplitude":
        slow_vals[flagged_ind2] = 1.0
    else:
        slow_vals[flagged_ind2] = 0.0
    slow_weights[flagged_ind2] = 0.0

    # Interpolate the values and weights
    if len(slow_soltab.time) > 1:
        f = si.interp1d(
            slow_soltab.time,
            slow_vals,
            axis=time_ind,
            kind="nearest",
            fill_value="extrapolate",
        )
        vals_time_intep = f(fast_soltab.time)
        f = si.interp1d(
            slow_soltab.time,
            slow_weights,
            axis=time_ind,
            kind="nearest",
            fill_value="extrapolate",
        )
        weights_time_intep = f(fast_soltab.time)
    else:
        # Just duplicate the single time to all times, without altering the
        # freq axis
        axes_shapes_time_interp = final_axes_shapes[:]
        axes_shapes_time_interp[freq_ind] = slow_vals.shape[freq_ind]
        vals_time_intep = expand_array(
            slow_vals, axes_shapes_time_interp, time_ind
        )
        weights_time_intep = expand_array(
            slow_weights, axes_shapes_time_interp, time_ind
        )
    if len(slow_soltab.freq) > 1:
        f = si.interp1d(
            slow_soltab.freq,
            vals_time_intep,
            axis=freq_ind,
            kind="nearest",
            fill_value="extrapolate",
        )
        vals_interp = f(fast_soltab.freq)
        f = si.interp1d(
            slow_soltab.freq,
            weights_time_intep,
            axis=freq_ind,
            kind="nearest",
            fill_value="extrapolate",
        )
        weights_interp = f(fast_soltab.freq)
    else:
        # Just duplicate the single frequency to all frequencies
        vals_interp = expand_array(
            vals_time_intep, final_axes_shapes, freq_ind
        )
        weights_interp = expand_array(
            weights_time_intep, final_axes_shapes, freq_ind
        )

    # Set all flagged values to NaNs
    vals_interp[weights_interp == 0.0] = np.nan

    return vals_interp, weights_interp


def combine_phase1_phase2_amp2_scalar(ss1, ss2, sso):
    """
    Take phases from 1 and phases and amplitudes from 2, scalar for both

    Parameters
    ----------
    ss1 : solset
        Solution set #1
    ss2 : solset
        Solution set #2
    sso : solset
        Output solution set

    Returns
    -------
    sso : solset
        Updated output solution set
    """
    # First, copy phases from 1
    ss1.obj._f_copy_children(sso.obj, recursive=True, overwrite=True)

    # Next, make the axes and their values for the output soltab
    st1 = ss1.getSoltab("phase000")
    st2 = ss2.getSoltab("phase000")
    axes_names = st1.getAxesNames()
    axes_names2 = st2.getAxesNames()
    axes_vals = []
    for axis in axes_names:
        axis_vals = st1.getAxisValues(axis)
        axes_vals.append(axis_vals)
    axes_shapes = [len(axis) for axis in axes_vals]

    # Average and interpolate the slow phases, then add them to the fast ones
    vals, weights = average_polarizations(st2)
    vals, weights = interpolate_solutions(
        st1, st2, axes_shapes, slow_vals=vals, slow_weights=weights
    )
    vals += st1.val
    weights *= st1.weight
    if "phase000" in sso.getSoltabNames():
        st = sso.getSoltab("phase000")
        st.delete()
    sso.makeSoltab(
        soltype="phase",
        soltabName="phase000",
        axesNames=axes_names,
        axesVals=axes_vals,
        vals=vals,
        weights=weights,
    )

    # Average the amplitudes (no interpolation needed)
    st2 = ss2.getSoltab("amplitude000")
    vals, weights = average_polarizations(st2)
    if "amplitude000" in sso.getSoltabNames():
        st = sso.getSoltab("amplitude000")
        st.delete()
    axes_vals = []
    axes_names2.pop(
        axes_names2.index("pol")
    )  # remove pol axis in output axis names
    for axis in axes_names2:
        axis_vals = st2.getAxisValues(axis)
        axes_vals.append(axis_vals)
    sso.makeSoltab(
        soltype="amplitude",
        soltabName="amplitude000",
        axesNames=axes_names2,
        axesVals=axes_vals,
        vals=vals,
        weights=weights,
    )

    return sso


def combine_h5parms(
    h5parm1,
    h5parm2,
    outh5parm,
    solset1="sol000",
    solset2="sol000",
):
    """
    Combines two h5parms by taking phases from h5parm1 and phases and
    amplitudes from h5parm2, scalar for both.

    Parameters
    ----------
    h5parm1 : str
        Filename of h5parm 1. Solution axes are assumed to be in the
        standard DDECal order of ['time', 'freq', 'ant', 'dir']
    h5parm2 : str
        Filename of h5parm 2. Solution axes are assumed to be in the
        standard DDECal order of ['time', 'freq', 'ant', 'dir', 'pol']
    outh5parm : str
        Filename of the output h5parm
    solset1 : str, optional
        Name of solset for h5parm1
    solset2 : str, optional
        Name of solset for h5parm2
    """
    # Make copies of the input h5parms (since they may be altered by steps
    # below) and open them
    with tempfile.TemporaryDirectory() as tmpdir:
        h5parm1_copy = shutil.copy(h5parm1, tmpdir)
        h5parm2_copy = shutil.copy(h5parm2, tmpdir)
        h1 = h5parm(h5parm1_copy, readonly=False)
        h2 = h5parm(h5parm2_copy, readonly=False)

        ss1 = h1.getSolset(solset=solset1)
        ss2 = h2.getSolset(solset=solset2)

        # Initialize the output h5parm
        if os.path.exists(outh5parm):
            os.remove(outh5parm)
        ho = h5parm(outh5parm, readonly=False)
        sso = ho.makeSolset(solsetName="sol000", addTables=False)

        # Take phases from 1 and phases and amplitudes from 2, scalar
        sso = combine_phase1_phase2_amp2_scalar(ss1, ss2, sso)

        # Close the files, copies are removed automatically
        h1.close()
        h2.close()
        ho.close()


if __name__ == "__main__":
    descriptiontext = "Combine two h5parms.\n"

    parser = argparse.ArgumentParser(
        description=descriptiontext, formatter_class=RawTextHelpFormatter
    )
    parser.add_argument("h51", help="Filename of input h5 1")
    parser.add_argument("h52", help="Filename of input h5 2")
    parser.add_argument("outh5", help="Filename of the output h5")
    args = parser.parse_args()

    combine_h5parms(
        args.h51,
        args.h52,
        args.outh5,
    )
