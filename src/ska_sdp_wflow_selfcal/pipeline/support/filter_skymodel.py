#  SPDX-License-Identifier: GPL-3.0-or-later
# Rapthor: LOFAR DDE Pipeline
# Copyright (C) 2023, Team Rapthor
# https://git.astron.nl/RD/rapthor/-/blob/master/LICENSE

"""
Script to filter and group a sky model with an image
"""

# This file is originally copied from the Rapthor repository:
# https://git.astron.nl/RD/rapthor/-/blob/
# 544ddf8cff8e65c5686b23479e227a12e6d1ed4f/rapthor/scripts/filter_skymodel.py
import os
import shutil
from ast import literal_eval

import astropy.io.ascii
import bdsf
import lsmtool
import numpy as np
from astropy import wcs
from astropy.io import fits as pyfits

from ska_sdp_wflow_selfcal.pipeline.support.miscellaneous import (
    dec2ddmmss,
    ra2hhmmss,
    rasterize,
    read_vertices_ra_dec,
)


# pylint: disable=too-many-arguments,too-many-branches
# pylint: disable=too-many-locals,too-many-statements
def filter_skymodel(
    flat_noise_image,
    true_sky_image,
    input_skymodel_pb,
    output_root,
    vertices_file,
    beam_ms,
    input_bright_skymodel_pb=None,
    threshisl=5.0,
    threshpix=7.5,
    rmsbox=(150, 50),
    rmsbox_bright=(35, 7),
    adaptive_rmsbox=True,
    adaptive_thresh=75.0,
    filter_by_mask=True,
    remove_negative=False,
    ncores=8,
):
    """
    Filter the input sky model

    Note: If no islands of emission are detected in the input image, a
    blank sky model is made. If any islands are detected in the input image,
    filtered true-sky and apparent-sky models are made, as well as a FITS clean
    mask (with the filename flat_noise_image+'.mask'). Various diagnostics are
    also derived and saved in JSON format.

    Parameters
    ----------
    flat_noise_image : str
        Filename of input image to use to detect sources for filtering.
        Ideally, this should be a flat-noise image (i.e., without primary-beam
        correction)
    true_sky_image : str or None
        Filename of input image to use to determine the true flux of sources.
        The primary-beam correction should have been applied to this image.
        When no true_sky_image is given, the fluxes in the sky model will be
        apparent fluxes.
    input_skymodel_pb : str
        Filename of input makesourcedb sky model, with primary-beam correction
    output_root : str
        Root of filenames of output makesourcedb sky models and image
        diagnostics files. Output filenames will be
        output_root+'.apparent_sky.txt', output_root+'.true_sky.txt',
        and output_root+'.image_diagnostics.json'
    vertices_file : str
        Filename of file with vertices
    beam_ms : str
        The filename of the MS for deriving the beam attenuation and
        theoretical image noise.
    input_bright_skymodel_pb : str, optional
        Filename of input makesourcedb sky model of bright sources only,
        with primary-beam correction
    threshisl : float, optional
        Value of thresh_isl PyBDSF parameter
    threshpix : float, optional
        Value of thresh_pix PyBDSF parameter
    rmsbox : tuple of floats, optional
        Value of rms_box PyBDSF parameter
    rmsbox_bright : tuple of floats, optional
        Value of rms_box_bright PyBDSF parameter
    adaptive_rmsbox : bool, optional
        Value of adaptive_rms_box PyBDSF parameter
    use_adaptive_threshold : bool, optional
        If True, use an adaptive threshold estimated from the negative values
        in the image
    adaptive_thresh : float, optional
        If adaptive_rmsbox is True, this value sets the threshold above
        which a source will use the small rms box
    comparison_skymodel : str, optional
        The filename of the sky model to use for flux scale and astrometry
        comparisons
    filter_by_mask : bool, optional
        If True, filter the input sky model by the PyBDSF-derived mask,
        removing sources that lie in unmasked regions
    remove_negative : bool, optional
        If True, remove negative sky model components
    """
    if rmsbox is not None and isinstance(rmsbox, str):
        rmsbox = literal_eval(rmsbox)
    if isinstance(rmsbox_bright, str):
        rmsbox_bright = literal_eval(rmsbox_bright)

    # Try to set the TMPDIR evn var to a short path, to ensure we do not hit
    # the length limits for socket paths (used by the mulitprocessing module).
    # We try a number of standard paths (the same ones used in the tempfile
    # Python library)
    try:
        old_tmpdir = os.environ["TMPDIR"]
    except KeyError:
        old_tmpdir = None
    for tmpdir in ["/tmp", "/var/tmp", "/usr/tmp"]:
        if os.path.exists(tmpdir):
            os.environ["TMPDIR"] = tmpdir
            break

    # Run PyBDSF first on the true-sky image to determine its properties and
    # measure source fluxes. The background RMS map is saved for later use in
    # the image diagnostics step
    img_true_sky = bdsf.process_image(
        true_sky_image if true_sky_image else flat_noise_image,
        mean_map="zero",
        rms_box=rmsbox,
        thresh_pix=threshpix,
        thresh_isl=threshisl,
        thresh="hard",
        adaptive_rms_box=adaptive_rmsbox,
        adaptive_thresh=adaptive_thresh,
        rms_box_bright=rmsbox_bright,
        atrous_do=True,
        atrous_jmax=3,
        rms_map=True,
        quiet=True,
        ncores=ncores,
    )
    catalog_filename = output_root + ".source_catalog.fits"
    img_true_sky.write_catalog(
        outfile=catalog_filename,
        format="fits",
        catalog_type="srl",
        clobber=True,
    )
    true_sky_rms_filename = output_root + ".true_sky_rms.fits"
    img_true_sky.export_image(
        outfile=true_sky_rms_filename, img_type="rms", clobber=True
    )

    flat_noise_rms_filename = output_root + ".flat_noise_rms.fits"
    if true_sky_image:
        # Run PyBDSF again on the flat-noise image and save the RMS map for
        # later use in the image diagnostics step
        img_flat_noise = bdsf.process_image(
            flat_noise_image,
            mean_map="zero",
            rms_box=rmsbox,
            thresh_pix=threshpix,
            thresh_isl=threshisl,
            thresh="hard",
            adaptive_rms_box=adaptive_rmsbox,
            adaptive_thresh=adaptive_thresh,
            rms_box_bright=rmsbox_bright,
            rms_map=True,
            stop_at="isl",
            quiet=True,
            ncores=ncores,
        )
        img_flat_noise.export_image(
            outfile=flat_noise_rms_filename, img_type="rms", clobber=True
        )
        del img_flat_noise  # helps reduce memory usage
    else:
        shutil.copy(true_sky_rms_filename, flat_noise_rms_filename)

    # Set the TMPDIR env var back to its original value
    if old_tmpdir is not None:
        os.environ["TMPDIR"] = old_tmpdir

    emptysky = False

    if img_true_sky.nisl > 0 and os.path.exists(input_skymodel_pb):
        maskfile = true_sky_image + ".mask"
        img_true_sky.export_image(
            outfile=maskfile, clobber=True, img_type="island_mask"
        )

        # Construct polygon needed to trim the mask to the sector
        header = pyfits.getheader(maskfile, 0)
        w = wcs.WCS(header)  # pylint: disable=C0103
        ra_ind = w.axis_type_names.index("RA")
        dec_ind = w.axis_type_names.index("DEC")
        vertices = read_vertices_ra_dec(vertices_file)

        verts = []
        for ra_vert, dec_vert in vertices:
            ra_dec = np.array([[0.0, 0.0, 0.0, 0.0]])
            ra_dec[0][ra_ind] = ra_vert
            ra_dec[0][dec_ind] = dec_vert
            verts.append(
                (
                    w.wcs_world2pix(ra_dec, 0)[0][ra_ind],
                    w.wcs_world2pix(ra_dec, 0)[0][dec_ind],
                )
            )

        hdu = pyfits.open(maskfile, memmap=False)
        data = hdu[0].data

        # Rasterize the poly
        rasterized_data = data[0, 0, :, :]
        rasterized_data = rasterize(verts, rasterized_data)
        data[0, 0, :, :] = rasterized_data

        hdu[0].data = data
        hdu.writeto(maskfile, overwrite=True)

        use_beam = bool(true_sky_image)

        # Load the sky model with the associated beam MS
        try:
            s_in = lsmtool.load(
                input_skymodel_pb,
                beamMS=beam_ms if use_beam else None,
            )
        except astropy.io.ascii.InconsistentTableError:
            emptysky = True

        # If bright sources were peeled before imaging, add them back
        if input_bright_skymodel_pb is not None:
            try:
                s_bright = lsmtool.load(input_bright_skymodel_pb)

                # Rename the bright sources, removing the '_sector_*' added
                # previously (otherwise the '_sector_*' text will be added
                # every iteration, eventually making for very long source
                # names)
                new_names = [
                    name.split("_sector")[0]
                    for name in s_bright.getColValues("Name")
                ]
                s_bright.setColValues("Name", new_names)
                if not emptysky:
                    s_in.concatenate(s_bright)
                else:
                    s_in = s_bright
                    emptysky = False
            except astropy.io.ascii.InconsistentTableError:
                pass

        # Do final filtering and write out the sky models
        if not emptysky:
            if remove_negative:
                # Keep only those sources with positive flux densities
                s_in.select("I > 0.0")
            if s_in and filter_by_mask:
                # Keep only those sources in PyBDSF masked regions
                s_in.select(f"{maskfile} == True")
            if s_in:
                # Write out apparent- and true-sky models
                s_in.group(maskfile)  # group the sky model by mask islands
                s_in.write(output_root + ".true_sky.txt", clobber=True)
                s_in.write(
                    output_root + ".apparent_sky.txt",
                    clobber=True,
                    applyBeam=use_beam,
                )
            else:
                emptysky = True
    else:
        emptysky = True

    if emptysky:
        # No sources cleaned/found in image, so just make a dummy sky model
        # with single, very faint source at center
        dummylines = [
            "Format = Name, Type, Patch, Ra, Dec, I, SpectralIndex, "
            "LogarithmicSI, ReferenceFrequency='100000000.0', MajorAxis, "
            "MinorAxis, Orientation\n"
        ]
        ra, dec = img_true_sky.pix2sky(
            (img_true_sky.shape[-2] / 2.0, img_true_sky.shape[-1] / 2.0)
        )
        del img_true_sky  # helps reduce memory usage
        if ra < 0.0:
            ra += 360.0
        ra = ra2hhmmss(ra)
        sra = f"{ra[0]:02}:{ra[1]:02}:{ra[2]:.6f}"
        dec = dec2ddmmss(dec)
        decsign = "-" if dec[3] < 0 else "+"
        sdec = f"{decsign}{dec[0]:02}.{dec[1]:02}.{dec[2]:.6f}"

        dummylines.append(f",,p1,{sra},{sdec}\n")
        dummylines.append(
            f"s0c0,POINT,p1,{sra},{sdec},0.00000001,[0.0,0.0],false,"
            f"100000000.0,,,\n"
        )
        with open(
            output_root + ".apparent_sky.txt", "w", encoding="utf-8"
        ) as current_file:
            current_file.writelines(dummylines)
        with open(
            output_root + ".true_sky.txt", "w", encoding="utf-8"
        ) as current_file:
            current_file.writelines(dummylines)
