#  SPDX-License-Identifier: BSD-3-Clause

""" This module contains commands to use WSClean """
from subprocess import STDOUT, check_call

import h5py

from ska_sdp_wflow_selfcal.pipeline.config.config import render_wsclean_option
from ska_sdp_wflow_selfcal.pipeline.support.read_data import (
    get_imaging_channels_out,
)

CHANNELS_OUT_KEY = "channels-out"
DECONVOLUTION_CHANNELS_KEY = "deconvolution-channels"
SPECTRAL_POLY_ORDER_KEY = "fit-spectral-pol"


class WSCleanRunner:  # pylint: disable=R0902
    """This class contains commands to use WSClean"""

    def __init__(self, settings):
        """
        Constructor.
        The 'settings' argument holds the parsed workflow arguments.
        """
        self.settings = settings
        self.log_filename = f"wsclean.{settings.logging_tag}.log"
        self.ms_channels_out = get_imaging_channels_out(settings.input_ms)

    def run_wsclean(self, arguments):
        """
        Runs WSClean with the given arguments.
        Writes the WSClean command and its output to the log file.
        """
        command = self.settings.wsclean_cmd + arguments
        with open(self.log_filename, "a", encoding="utf-8") as outfile:
            outfile.write(f"Running: {' '.join(command)}\n")
        # Closing 'outfile' before starting WSClean ensures that the command is
        # always above the WSClean output.

        with open(self.log_filename, "a", encoding="utf-8") as outfile:
            check_call(command, stderr=STDOUT, stdout=outfile)

    def add_ms_based_options(
        self, configuration_file_arguments, wsclean_config
    ):
        """
        Add default options, based on the input MS, if they are not in the
        configuration.
        """
        if CHANNELS_OUT_KEY not in wsclean_config:
            channels_out = self.ms_channels_out
            configuration_file_arguments.extend(
                render_wsclean_option(CHANNELS_OUT_KEY, channels_out)
            )
        else:
            channels_out = wsclean_config[CHANNELS_OUT_KEY]

        if DECONVOLUTION_CHANNELS_KEY not in wsclean_config:
            # Use the number of output channels, up to a maximum of 4, for
            # spectral fitting.
            deconvolution_channels = min(4, channels_out)
            configuration_file_arguments.extend(
                render_wsclean_option(
                    DECONVOLUTION_CHANNELS_KEY, deconvolution_channels
                )
            )
        else:
            deconvolution_channels = wsclean_config[DECONVOLUTION_CHANNELS_KEY]

        if SPECTRAL_POLY_ORDER_KEY not in wsclean_config:
            # Set the fit spectral order to one less than the number of
            # deconvolution channels.
            spectral_poly_order = max(1, deconvolution_channels - 1)
            configuration_file_arguments.extend(
                render_wsclean_option(
                    SPECTRAL_POLY_ORDER_KEY, spectral_poly_order
                )
            )

    def run(  # pylint: disable=R0913
        self,
        msin,
        facets_file,
        solutions_file,
        fits_mask,
        temp_dir,
        n_major_iterations,
        wsclean_config,
    ):
        """Run WSClean"""

        # The calibration solutions always contain the 'phase000' field.
        # Check if 'amplitude000' is also present
        solution_fields = "phase000"
        with h5py.File(solutions_file, "r") as h5_solutions:
            if "amplitude000" in list(
                h5_solutions[list(h5_solutions.keys())[0]].keys()
            ):
                solution_fields = "amplitude000,phase000"

        # process config dictionary into a list
        configuration_file_arguments = []
        for key in sorted(wsclean_config):
            val = wsclean_config[key]
            configuration_file_arguments.extend(
                render_wsclean_option(key, val)
            )

        # Add default settings, in case the configuration does not define them.
        self.add_ms_based_options(configuration_file_arguments, wsclean_config)

        self.run_wsclean(
            [
                "-facet-regions",
                facets_file,
                "-apply-facet-solutions",
                solutions_file,
                f"{solution_fields}",
                "-fits-mask",
                fits_mask,
                "-temp-dir",
                temp_dir,
                "-size",
                str(self.settings.configuration["image_size"]),
                str(self.settings.configuration["image_size"]),
                "-scale",
                str(self.settings.configuration["image_scale"]),
                "-nmiter",
                f"{n_major_iterations}",
            ]
            + configuration_file_arguments
            + [msin],
        )

    def restore(
        self,
        image_name,
        bright_source_skymodel="bright_source_skymodel.txt",
    ):
        """Run WSClean restore operation"""
        self.run_wsclean(
            self.settings.restore_args
            + [
                "-restore-list",
                f"{image_name}.fits",
                bright_source_skymodel,
                f"{image_name}.fits",
            ],
        )

        self.run_wsclean(
            self.settings.restore_args
            + [
                "-restore-list",
                f"{image_name}-pb.fits",
                bright_source_skymodel,
                f"{image_name}-pb.fits",
            ],
        )
