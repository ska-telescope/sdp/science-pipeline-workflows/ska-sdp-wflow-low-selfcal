#  SPDX-License-Identifier: BSD-3-Clause

# -*- coding: utf-8 -*-
"""
This files checks that the DP3 and WSClean binaries used are the expected
ones for the current version. It also verifies that everybeam and casacore are
using the same binaries throughout the pipeline execution.
The checks do not raise an exception (the pipeline can work also with
different versions), but add an ERROR message in the logs.
The reference to a specific commit is added to enable results reproducibility.
"""

import logging
import os
import re
import shutil
import subprocess
from glob import glob

import casacore
import everybeam

logger = logging.getLogger("ska-sdp-wflow-selfcal")

EXPECTED_DP3_VERSION = "6.3.0"
EXPECTED_WSCLEAN_VERSION = "3.5"


def check_version(dp3_path, wsclean_path):
    """
    This function checks if there are multiple versions of the same library
    loaded, and if DP3 and WSClean have the expected version.
    """
    dp3_exe = shutil.which(dp3_path)
    wsclean_exe = shutil.which(wsclean_path)

    success = True

    try:
        success &= verify_single_casacore(dp3_exe, wsclean_exe)
    except subprocess.CalledProcessError as exception:
        logger.error("Error while checking single CasaCore: %s", exception)
        success = False

    try:
        success &= verify_single_everybeam(dp3_exe, wsclean_exe)
    except subprocess.CalledProcessError as exception:
        logger.error("Error while checking single EveryBeam: %s", exception)
        success = False

    logger.info(
        "Single CasaCore and EveryBeam checks: %s",
        "PASSED" if success else "FAILED",
    )

    success &= check_dp3_version(dp3_exe)
    success &= check_wsclean_version(wsclean_exe)

    if success:
        logger.info("Version checks successful")

    return success


def check_dp3_version(dp3_exe):
    """Checks if DP3 has the expected version."""

    # The command 'DP3 -v' returns a string containing the version and a
    # commit hash, if available. Since Spack builds of DP3 may not contain the
    # commit hash, check the version string only. (Spack may not create a
    # git clone when building DP3.)
    dp3_version_lines = (
        subprocess.check_output([dp3_exe, "-v"]).decode().splitlines()
    )
    if len(dp3_version_lines) == 0:
        installed_dp3_version = "(Error reading DP3 version.)"
    else:
        dp3_version_split = dp3_version_lines[0].split(" ")
        if len(dp3_version_split) < 2 or dp3_version_split[0] != "DP3":
            installed_dp3_version = (
                f"(Incorrect DP3 version line: {dp3_version_lines[0]})"
            )
        else:
            installed_dp3_version = dp3_version_split[1]

    if EXPECTED_DP3_VERSION != installed_dp3_version:
        logger.error(
            "DP3 version mismatch. Expected: %s Found: %s",
            EXPECTED_DP3_VERSION,
            installed_dp3_version,
        )
        return False

    return True


def check_wsclean_version(wsclean_exe):
    """Checks if WSClean has the expected version."""

    # The command 'wsclean -version' returns a string containing the version,
    # author, date, the commit hash and other info. Again, Spack builds may
    # not contain the commit hash, so check the version only.
    # The version is the 3rd word of the 2nd line of the output.
    wsclean_version_lines = (
        subprocess.check_output([wsclean_exe, "-version"])
        .decode()
        .splitlines()
    )
    if len(wsclean_version_lines) < 5:
        installed_wsclean_version = "(Error reading WSClean version.)"
    else:
        wsclean_version_split = wsclean_version_lines[1].split(" ")
        if (
            len(wsclean_version_split) < 3
            or wsclean_version_split[0] != "WSClean"
            or wsclean_version_split[1] != "version"
        ):
            installed_wsclean_version = (
                f"(Incorrect WSClean version line: {wsclean_version_lines[1]})"
            )
        else:
            installed_wsclean_version = wsclean_version_split[2]

    if EXPECTED_WSCLEAN_VERSION != installed_wsclean_version:
        logger.error(
            "WSClean version mismatch. Expected: %s Found: %s",
            EXPECTED_WSCLEAN_VERSION,
            installed_wsclean_version,
        )
        return False

    return True


def verify_single_everybeam(dp3_exe, wsclean_exe):
    """Verify that Python, DP3, and WSClean use the same EveryBeam library."""

    dependency = "libeverybeam"
    path_via_python = get_path_to_dependency(everybeam.__file__, dependency)
    path_via_dp3 = get_path_to_dependency(dp3_exe, dependency)
    path_via_wsclean = get_path_to_dependency(wsclean_exe, dependency)

    # set everybeam data if empty
    if (
        "EVERYBEAM_DATADIR" not in os.environ
        or not os.environ["EVERYBEAM_DATADIR"]
    ):
        os.environ["EVERYBEAM_DATADIR"] = os.path.join(
            os.path.dirname(path_via_dp3), "share", "everybeam"
        )

    if path_via_python == path_via_dp3 == path_via_wsclean:
        return True

    logger.error(
        "Single EveryBeam check failed. When multiple versions of "
        "EveryBeam are loaded, unexpected behaviour can occur.\n"
        "Python points to  %s\n"
        "DP3 points to     %s\n"
        "WSClean points to %s\n",
        path_via_python,
        path_via_dp3,
        path_via_wsclean,
    )
    return False


def verify_single_casacore(dp3_exe, wsclean_exe):
    """
    Verify that Python, Everybeam, DP3 and WSClean
    use the same CasaCore library.
    """

    filelist = glob(
        os.path.join(
            os.path.dirname(casacore.__file__), "tables", "_tables.cpython-*so"
        )
    )
    if not filelist:
        raise FileNotFoundError(
            "File search to Python CasaCore library location unsuccessful."
        )
    casacore_python_lib = filelist[0]

    dependency = "libcasa_tables"
    path_via_python = get_path_to_dependency(casacore_python_lib, dependency)
    path_via_everybeam = get_path_to_dependency(everybeam.__file__, dependency)
    path_via_dp3 = get_path_to_dependency(dp3_exe, dependency)
    path_via_wsclean = get_path_to_dependency(wsclean_exe, dependency)

    if (
        path_via_python
        == path_via_everybeam
        == path_via_dp3
        == path_via_wsclean
    ):
        return True

    logger.error(
        "Single CasaCore check failed. When multiple versions of "
        "CasaCore are loaded, unexpected behaviour can occur.\n"
        "Python points to    %s\n"
        "EveryBeam points to %s\n"
        "DP3 points to       %s\n"
        "WSClean points to   %s\n",
        path_via_python,
        path_via_everybeam,
        path_via_dp3,
        path_via_wsclean,
    )
    return False


def get_path_to_dependency(executable, dependency):
    """
    Uses the command 'ldd' to determine if :param executable (or library)
    depends on :param dependency. Returns None if not found,
    otherwise it returns the path to the dependency.

    Since libraries always have the extension ".so", :param dependency must be
    provided without an extension.
    """

    # Libraries may have a hash and a version suffix, so put them in optional
    # regexp groups so the library is also recognized when it has them.
    target_dependency = rf"{dependency}(-.*)?\.so(\.[0-9]+)?"

    ldd_output = subprocess.check_output(["ldd", executable]).decode()

    match = re.search(
        f"^\t{target_dependency} => (.*{target_dependency}) ",
        ldd_output,
        re.MULTILINE,
    )

    # os.path.abspath() removes remains of relative paths (../../ etc).
    return os.path.abspath(match.group(3)) if match else None
