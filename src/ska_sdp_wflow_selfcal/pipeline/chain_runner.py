#  SPDX-License-Identifier: BSD-3-Clause

"""This module runs the user-defined chain of functions in YAML.
It is not yet usable as it is still under development"""

import copy
from argparse import Namespace
from pathlib import Path
from typing import Any

from ska_sdp_wflow_selfcal.pipeline.dp3_helper import Dp3Runner
from ska_sdp_wflow_selfcal.pipeline.operations import calibrate, image
from ska_sdp_wflow_selfcal.pipeline.support.skymodel_utils import (
    get_outliers,
    process_skymodels,
)
from ska_sdp_wflow_selfcal.pipeline.wsclean_helper import WSCleanRunner


class ChainRunner:
    """
    Class to run the user-defined chain of functions called in the YAML
    configuration file.

    Attributes
    ----------
    _private_properties: dict[str, typing.Any] (PRIVATE CLASS ATTRIBUTE)
        stores essential input/default values for the class to run;
        this is a dictionary combining the private properties to avoid having
        more class instances than allowed by the current linting options.

    input_ms_record: list[dict[str, list[typing.Optional[str]]]]
        records input measurement sets (if any) to all functions
        called in this class through YAML; auto-updated as the functions
        are called.

    input_skymodels_record: list[typing.Optional[dict[str, str]]]
        records input skymodel filenames submitted for a cycle (if any)
        through YAML.

    input_solutions_record: list[dict[str, list[typing.Optional[str]]]]
        records input solution filenames (if any) to `calibrate()`
        and `predict()` of the various cycles.

    output_ms_record: list[dict[str, list[typing.Optional[str]]]]
        records output measurement sets (if any) from all functions
        called in this class through YAML; auto-updated as the functions
        are called.

    output_solutions_record: list[typing.Optional[str]]
        records output h5 solution filenames (if any) from all functions called
        in this class through YAML; auto-updated as the functions are
        called.

    Methods
    -------
    see every method below.
    """

    def __init__(
        self,
        num_cycles: int,
        *,
        dp3_runner: Dp3Runner,
        wsclean_runner: WSCleanRunner,
        work_dir: Path,
        settings: Namespace,
    ):
        """
        Initiates the class.

        Parameters
        ----------
        """
        self._private_properties = {
            "dp3_runner": dp3_runner,
            "input_ms": Path(settings.input_ms).absolute(),
            "num_cycles": num_cycles,
            "settings": settings,
            "work_dir": work_dir,
            "wsclean_runner": wsclean_runner,
        }

        self.input_ms_record = [
            {"calibrate": [None, None], "image": [None, None]}
        ] * num_cycles

        self.input_skymodels_record = [None] * num_cycles

        self.input_solutions_record = [
            {"image": [None, None], "predict": [None, None]}
        ] * num_cycles

        self.output_ms_record = copy.deepcopy(self.input_ms_record)

        self.output_solutions_record = [None] * num_cycles

    def configure_input_ms(self, ms_input: str, func: str, cycle: int) -> None:
        """
        Updates the attribute `input_ms_record` accordingly.

        Parameters
        ----------
        ms_input: str
            keyword to call previously generated MS,
            or a custom MS entry.

        func: str
            the current self calibration function.

        cycle: int
            the current cycle.

        Raises
        ------
        UnboundLocalError:
            if 'ms_input' refers to an output MS of an earlier step,
            and that output MS is unknown.
        """
        # input MS
        if ms_input == "pipeline_input":
            self.input_ms_record[cycle][func] = [
                f"{self._private_properties['input_ms']}",
                ms_input,
            ]

        # MS from `calibrate` or `image` in previous cycle
        elif ms_input in [
            f"cycle_{idx_cycle+1}_{func_name}_output"
            for idx_cycle in range(cycle + 1)
            for func_name in ["calibrate", "image"]
        ]:
            cycle_request = int(ms_input.split("_")[1]) - 1
            func_request = ms_input.split("_")[2]

            if self.output_ms_record[cycle_request][func_request] == [
                None,
                None,
            ]:
                raise UnboundLocalError(
                    f"'{ms_input}' refers to a nonexistent MS"
                )

            self.input_ms_record[cycle][func] = self.output_ms_record[
                cycle_request
            ][func_request]

        # custom MS
        else:
            self.input_ms_record[cycle][func] = [ms_input, "custom"]

    def configure_input_solutions(
        self, solutions_input: str, func: str, cycle: int
    ) -> None:
        """
        Updates the attribute `input_solutions_record` accordingly.

        Parameters
        ----------
        solutions_input: str
            keyword to call previously generated solutions filename,
            or a custom solutions entry.

        func: str
            the current self calibration function.

        cycle: int
            the current cycle.

        Raises
        ------
        UnboundLocalError:
            if requested solutions do not exist.
        """
        # solutions from `image` or `predict` in previous cycle
        if solutions_input in [
            f"cycle_{idx_cycle+1}_output" for idx_cycle in range(cycle + 1)
        ]:
            cycle_request = int(solutions_input.split("_")[1]) - 1

            if self.output_solutions_record[cycle_request] is None:
                raise UnboundLocalError(
                    f"'{solutions_input}' refers to non-existent solutions"
                )

            self.input_solutions_record[cycle][func] = [
                self.output_solutions_record[cycle_request],
                f"cycle_{cycle_request+1}_output",
            ]

        # custom solutions
        else:
            self.input_solutions_record[cycle][func] = [
                solutions_input,
                "custom",
            ]

    def include_skymodels(self, skymodels: dict[str, Any], cycle: int) -> None:
        """
        Processes input skymodels to a cycle and stores them in the attribute
        `input_skymodels_record`.

        Parameters
        ----------
        skymodels: dict[str, typing.Any]
            dictionary of the submitted skymodels in YAML.

        cycle: int
            the current cycle.

        Raises
        ------
        UnboundLocalError:
            if default `skymodel_apparent_filename` is requested in cycle 0.
        """
        work_dir = self._private_properties["work_dir"]

        # fetch options
        if (
            skymodels["vertices_filename"] is None
            or skymodels["vertices_filename"] == ""
        ):
            skymodels["vertices_filename"] = str(
                work_dir.joinpath("vertices.pkl")
            )

        if (
            skymodels["sector_skymodel_filename"] is None
            or skymodels["sector_skymodel_filename"] == ""
        ):
            skymodels["sector_skymodel_filename"] = str(
                work_dir.joinpath("sector.skymodel")
            )

        if (
            skymodels["skymodel_true_filename"] is None
            or skymodels["skymodel_true_filename"] == ""
        ):
            if cycle == 0:
                skymodels["skymodels_true_filename"] = str(
                    work_dir.joinpath("initial.skymodel")
                )

            else:
                imaging_dir = work_dir.joinpath(f"image_{cycle}")

                imaging_prefix = self._private_properties[
                    "settings"
                ].configuration["selfcal_cycles"][cycle - 1]["image"][
                    "wsclean"
                ][
                    "name"
                ]

                skymodels["skymodels_true_filename"] = str(
                    imaging_dir.joinpath(f"{imaging_prefix}.true_sky.txt")
                )

        if (
            skymodels["calibrate_skymodel_filename"] is None
            or skymodels["calibrate_skymodel_filename"] == ""
        ):
            skymodels["calibrate_skymodel_filename"] = str(
                work_dir.joinpath(f"calibrate_{cycle+1}.skymodel")
            )

        if "skymodel_apparent_filename" in skymodels and (
            skymodels["skymodel_apparent_filename"] is None
            or skymodels["skymodel_apparent_filename"] == ""
        ):
            if cycle == 0:
                raise UnboundLocalError(
                    "default behaviour does not support "
                    f"`skymodel_apparent_filename` in cycle {cycle+1}"
                )

            imaging_dir = work_dir.joinpath(f"image_{cycle}")

            settings = self._private_properties["settings"]
            cycle_config = settings.configuration["selfcal_cycles"][cycle - 1]
            imaging_prefix = cycle_config["image"]["wsclean"]["name"]

            skymodels["skymodels_true_filename"] = str(
                imaging_dir.joinpath(f"{imaging_prefix}.true_sky.txt")
            )

        if "bright_sources_skymodel_prefix" in skymodels and (
            skymodels["bright_sources_skymodel_prefix"] is None
            or skymodels["bright_sources_skymodel_prefix"] == ""
        ):
            skymodels["bright_sources_skymodel_prefix"] = str(
                work_dir.joinpath(f"bright_sources_{cycle+1}")
            )

        # fetch outliers' option & run `get_outliers()`
        if "outliers_skymodel_filename" in skymodels and (
            skymodels["outliers_skymodel_filename"] is None
            or skymodels["outliers_skymodel_filename"] == ""
        ):
            filename = f"outlier_{cycle+1}_predict.skymodel"
            skymodels["outliers_skymodel_filename"] = str(
                work_dir.joinpath(filename)
            )

            get_outliers(
                skymodels["calibrate_skymodel_filename"],
                skymodels["sector_skymodel_filename"],
                skymodels["outliers_skymodel_filename"],
            )

        # run `process_skymodels()`
        config_dict = skymodels["settings"]
        args = skymodels.pop("settings")
        args = (
            args.pop("outliers_skymodel_filename")
            if "outliers_skymodel_filename" in args
            else args
        )

        process_skymodels(
            # This still only takes the MS input to the pipeline;
            # can be changed in the future.
            config_dict,
            self._private_properties["input_ms"],
            **args,
        )

        # record skymodels
        self.input_skymodels_record[cycle] = skymodels

    def run_calibrate(
        self, *, cycle: int, pipeline_config: dict[str, Any]
    ) -> None:
        """
        Runs the `calibrate()` step including all relevant intrinsic
        operations; updates the attribute `output_skymodels_record`
        accordingly.

        Parameters
        ----------
        cycle: int
            the current cycle.

        pipeline_config: dict[str, typing.Any]
            YAML configuration for this step.

        Raises
        ------
        KeyError:
            if the input skymodel is not defined.
        """
        # input MS
        self.configure_input_ms(
            pipeline_config["input_ms"], "calibrate", cycle
        )

        # input skymodel
        if (
            "calibrate_skymodel_filename" not in pipeline_config
            or pipeline_config["calibrate_skymodel_filename"] is None
            or pipeline_config["calibrate_skymodel_filename"] == ""
        ):
            if (
                self.input_skymodels_record[cycle] is None
                or "calibrate_skymodel_filename"
                not in self.input_skymodels_record[cycle]
            ):
                raise KeyError(
                    f"no skymodels submitted in cycle {cycle+1} "
                    "or to `calibrate()` thereof"
                )

            pipeline_config[
                "calibrate_skymodel_filename"
            ] = self.input_skymodels_record[cycle][
                "calibrate_skymodel_filename"
            ]

        # output solutions
        if "solutions_filename" not in pipeline_config:
            pipeline_config["solutions_filename"] = self._private_properties[
                "work_dir"
            ].joinpath(f"calibrate_{cycle+1}_solutions.h5")

        calibrate(
            self._private_properties["dp3_runner"],
            self.input_ms_record[cycle]["calibrate"][0],
            pipeline_config["calibrate_skymodel_filename"],
            self._private_properties["work_dir"],
            cycle,
            self._private_properties["settings"],
            str(pipeline_config["solutions_filename"]),
        )

        self.output_solutions_record[cycle] = str(
            pipeline_config["solutions_filename"]
        )

    def run_image(
        self, *, cycle: int, pipeline_config: dict[str, Any]
    ) -> None:
        """
        Runs the `image()` step including all relevant intrinsic
        operations.

        Parameters
        ----------
        cycle: int
            the current cycle.

        pipeline_config: dict[str, typing.Any]
            YAML configuration for this step.

        Raises
        ------
        KeyError:
            if the input skymodel/vertices are not defined.
        """
        # input MS
        self.configure_input_ms(pipeline_config["input_ms"], "image", cycle)

        # input skymodel
        if (
            "calibrate_skymodel_filename" not in pipeline_config
            or pipeline_config["calibrate_skymodel_filename"] is None
            or pipeline_config["calibrate_skymodel_filename"] == ""
        ):
            if (
                self.input_skymodels_record[cycle] is None
                or "calibrate_skymodel_filename"
                not in self.input_skymodels_record[cycle]
            ):
                raise KeyError(
                    f"no skymodels submitted in cycle {cycle+1} "
                    "or to `image()` thereof"
                )

            pipeline_config[
                "calibrate_skymodel_filename"
            ] = self.input_skymodels_record[cycle][
                "calibrate_skymodel_filename"
            ]

        # input solutions
        self.configure_input_solutions(
            pipeline_config["solutions_filename"], "image", cycle
        )

        # input vertices
        if (
            "vertices_filename" not in pipeline_config
            or pipeline_config["vertices_filename"] is None
            or pipeline_config["vertices_filename"] == ""
        ):
            if (
                self.input_skymodels_record[cycle] is None
                or "vertices_filename"
                not in self.input_skymodels_record[cycle]
            ):
                raise KeyError(
                    f"no vertices submitted in cycle {cycle+1} "
                    "via `include_skymodels()` or to `image()` thereof"
                )

            pipeline_config["vertices_filename"] = self.input_skymodels_record[
                cycle
            ]["vertices_filename"]

        # bright sources skymodel
        if (
            "bright_sources_skymodel_filename" not in pipeline_config
            or pipeline_config["bright_sources_skymodel_filename"] is None
            or pipeline_config["bright_sources_skymodel_filename"] == ""
        ):
            if (
                self.input_skymodels_record[cycle] is None
                or "bright_sources_skymodel_prefix"
                not in self.input_skymodels_record[cycle]
            ):
                pipeline_config["bright_sources_skymodel_filename"] = None

            else:
                bright_sources_skymodel_prefix = self.input_skymodels_record[
                    cycle
                ]["bright_sources_skymodel_prefix"]

                pipeline_config[
                    "bright_sources_skymodel_filename"
                ] = f"{bright_sources_skymodel_prefix}.skymodel"

        image(
            self._private_properties["dp3_runner"],
            self._private_properties["wsclean_runner"],
            self.input_ms_record[cycle]["image"][0],
            self.input_solutions_record[cycle]["image"][0],
            pipeline_config["calibrate_skymodel_filename"],
            pipeline_config["vertices_filename"],
            self._private_properties["work_dir"],
            cycle,
            pipeline_config["bright_sources_skymodel_filename"],
        )
