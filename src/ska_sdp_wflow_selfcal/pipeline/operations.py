#  SPDX-License-Identifier: BSD-3-Clause

""" This module contains all operations to run in the pipeline """

import logging
import os
import shutil

from ska_sdp_wflow_selfcal.pipeline.support.blank_image import blank_image
from ska_sdp_wflow_selfcal.pipeline.support.combine_h5parms import (
    combine_h5parms,
)
from ska_sdp_wflow_selfcal.pipeline.support.filter_skymodel import (
    filter_skymodel,
)
from ska_sdp_wflow_selfcal.pipeline.support.H5parm_collector import (
    collect_h5parms,
)
from ska_sdp_wflow_selfcal.pipeline.support.make_region_file import (
    make_region_file,
)
from ska_sdp_wflow_selfcal.pipeline.support.miscellaneous import concatenate_ms
from ska_sdp_wflow_selfcal.pipeline.support.read_data import (
    calculate_n_chunks,
    compute_observation_width,
    get_imaging_n_iterations,
    get_observation_params,
    get_patches,
    get_phase_center,
    get_start_times,
)
from ska_sdp_wflow_selfcal.pipeline.support.subtract_sector_models import (
    subtract_sector_models,
)
from ska_sdp_wflow_selfcal.pipeline.support.time import human_readable_time

logger = logging.getLogger("ska-sdp-wflow-selfcal")


def log_ms_operation(operation, input_ms, hr_time=None):
    """Log a debug message for an operation on (a part of) an MS."""
    if hr_time:
        logger.debug(
            "Running %s for %s, start_time %s", operation, input_ms, hr_time
        )
    else:
        logger.debug("Running %s for %s", operation, input_ms)


def create_input_lists(input_ms_or_list, data_fraction, minimum_time):
    """
    Creates lists with input measurement set files and start times, for use by
    calibrate() and predict().

    Parameters
    ----------
    input_ms_or_list : str or list of str
        Either the initial input measurement set, which should be split, or a
        list of already split measurement sets.
    data_fraction: float
        Data fraction to process, e.g., 0.2 means use 20 % of the data.
    minimum_time: float
        Minimum time duration for each MS chunk, in seconds.

    Returns
    -------
    A tuple containing:
    - A list of input measurement sets.
    - If ms_input must be split: The start times for reading ms_input
      so it can be split up.
      If ms_input was already split: A list containing None values.
    """
    if isinstance(input_ms_or_list, str):
        n_chunks = calculate_n_chunks(
            input_ms_or_list, data_fraction, minimum_time
        )
        logger.info(
            "Splitting the measurement set in %s time chunks", n_chunks
        )

        start_time_list = get_start_times(input_ms_or_list, n_chunks)
        input_ms_list = [input_ms_or_list] * len(start_time_list)
    elif isinstance(input_ms_or_list, list):
        input_ms_list = input_ms_or_list
        # Do not provide start time overrides. Use the start times of the MSs.
        start_time_list = [None] * len(input_ms_or_list)
    else:
        raise TypeError("Internal error: Invalid ms_input type")

    return input_ms_list, start_time_list


def temp_ms_name(input_ms, start_time, work_dir, suffix):
    """
    Creates a filename for a temporary MS in the working directory.

    Parameters
    ----------
    input_ms : str
        A filename of an existing measurement set.
    start_time: float or None
        If float, `input_ms' should not contain a start time in its name.
        This function will put the start time in the generated filename.
        If None, 'input_ms' should be generated using an earlier call
        to this function, and already has the start time in its name.
    work_dir: str
        The directory for creating the temporary MS.
    suffix: str
        Filename suffix for the temporary MS.

    Returns
    -------
    temp_ms: str
        The generated filename, which includes a start time and ends
        with the given suffix.
    """

    if start_time:
        # The MS typically exists outside the working directory.
        # Create an MS in the working directory, starting with the same name.
        ms_filename = os.path.basename(input_ms)
        mjd_time = "mjd" + str(int(start_time))
        temp_ms = f"{work_dir}/{ms_filename}.{mjd_time}.{suffix}"
    else:
        # For existing MSs in the working directory, only replace the suffix.
        temp_ms = os.path.splitext(input_ms)[0] + "." + suffix

    return temp_ms


def calibrate(
    dp3_runner,
    input_ms_or_list,
    input_skymodel,
    work_dir,
    selfcal_loop,
    settings,
    solutions_filename,
):  # pylint: disable=too-many-locals, too-many-arguments
    """
    The calibrate function orchestrates the calibration process for the selfcal
    workflow, handling different calibration tasks based on the
    self-calibration (selfcal) loop iteration.
    It performs scalarphase calibration for loop 1 and 2 and both scalarphase
    and complexgain calibration for loop 3. The function can optionally combine
    DP3 calibration calls.

    Parameters
    ----------
    dp3_runner : object
        An object responsible for running DP3 commands.
    input_ms_or_list : str or list of str
        Either the initial input measurement set, which should be split, or a
        list of already split measurement sets, containing observational data.
    input_skymodel : str
        Skymodel containing grouped sources, which serves as input skymodel.
    work_dir : str
        Directory where intermediate and output files are stored.
    selfcal_loop : int
        The current iteration of the self calibration loop (0-based).
    settings : object
        Configuration settings containing parameters like data_fraction,
        minimum_time and settings from the YML configuration file.
    solutions_filename:
        Target filename where the combined solutions have to be written to.

    Returns
    ----------
    combined_solutions : str
        The filename of the generated combined calibration solutions file.
    """

    logger.info("run_pipeline:: Start calibrate_%s", selfcal_loop + 1)

    loop_config = settings.configuration["selfcal_cycles"][selfcal_loop]
    calibrate_config = loop_config["calibrate"]
    dp3_config = calibrate_config["dp3"]

    input_ms_list, start_time_list = create_input_lists(
        input_ms_or_list,
        loop_config["data_fraction"],
        settings.configuration["minimum_time"],
    )

    output_solutions = []
    for ms_index, input_ms in enumerate(input_ms_list):
        hr_time = human_readable_time(start_time_list[ms_index])

        output_solutions.append(
            dp3_runner.calibrate(
                f"calibrate_{selfcal_loop + 1}.ms{ms_index}",
                input_ms,
                hr_time,
                input_skymodel,
                dp3_config,
            )
        )

    # In distributed runs, 'solutions' contains dask delayed objects. join()
    # then returns a list with the actual return values.
    distributed_solutions = dp3_runner.join()
    if distributed_solutions:
        output_solutions = distributed_solutions

    # Transpose solutions into a list per solver type.
    # [ [ time1solver1_, time1solver2], [ time2solver1, time2solver2 ], ... ]
    # -> [ [ time1solver1, time2solver1, ... ],
    #      [ time1solver2, time2solver2, ... ] ]
    output_solutions = list(map(list, zip(*output_solutions)))

    # Stitch all solutions together
    logger.info("Start collect_h5parms scalarphase")
    combined_solutions_fast_phase = (
        f"{work_dir}/out-cal{selfcal_loop + 1}-fastphase-solutions.h5"
    )
    collect_h5parms(
        output_solutions[0],
        combined_solutions_fast_phase,
    )
    logger.info("Done collect_h5parms scalarphase")

    if len(output_solutions) == 1:
        os.rename(combined_solutions_fast_phase, solutions_filename)
    elif len(output_solutions) == 2:
        # Merge the scalarphase and complexgain solutions in a single h5 file
        logger.info("Start collect_h5parms complexgain")
        combined_solutions_complex_gain = (
            f"{work_dir}/out-cal{selfcal_loop + 1}-complexgain-solutions.h5"
        )
        collect_h5parms(
            output_solutions[1],
            combined_solutions_complex_gain,
        )
        logger.info("Done collect_h5parms complexgain")

        logger.info("Start combine_h5parms")

        combine_h5parms(
            combined_solutions_fast_phase,
            combined_solutions_complex_gain,
            solutions_filename,
            solset1="sol000",
            solset2="sol000",
        )

        os.remove(combined_solutions_fast_phase)
        os.remove(combined_solutions_complex_gain)

        logger.info("Done combine_h5parms")
    else:
        raise RuntimeError(
            "Unsupported solver step count for calibration. "
            "Only one or two solver steps are supported."
        )

    logger.info("run_pipeline:: Done calibrate_%s", selfcal_loop + 1)


def predict(
    dp3_runner,
    input_ms_or_list,
    skymodels_list,
    work_dir,
    combined_solutions,
    selfcal_loop,
    source_type,
    settings,  # None
):  # pylint: disable=too-many-arguments,too-many-locals,too-many-statements,
    # pylint: disable=too-many-branches
    """
    This function performs a DP3 predict operation and subtracts the predicted
    Measurement Set (MS) from the input MS.

    Parameters
    ----------
    dp3_runner: object
        An instance of the DP3 runner used to execute the predict tasks.
    input_ms_or_list : str or list of str
        Either the initial input measurement set, which should be split, or a
        list of already split measurement sets, containing observational data.
    skymodels_list: list of skymodel files
        List of sky models to be used for prediction.
    work_dir: str
        Directory where the output files will be stored.
    combined_solutions: path to h5parm solutions file
        Combined calibration solutions to be applied during prediction.
    selfcal_loop: int
        The current iteration of the self calibration loop (0-based).
    source_type: str
        Type of sources to process. It can be either 'outlier' or
        'bright_source'.
    settings: object
        Configuration settings for the processing, including data_fraction
        and minimum_time for chunking.

    Returns
    ----------
    in_imaging: list of str
        List of paths to the MS files created after the subtraction of
        model data, to be used as input for the next imaging operation.
    """

    directions_str = []
    for skymodel in skymodels_list:
        directions_list = get_patches(skymodel)[0]
        directions = "],[".join(directions_list)
        directions_str.append(f"[[{directions}]]")

    config = settings.configuration

    input_ms_list, start_time_list = create_input_lists(
        input_ms_or_list,
        config["selfcal_cycles"][selfcal_loop]["data_fraction"],
        config["minimum_time"],
    )

    out_predict = []
    for skymodel_index, skymodel in enumerate(skymodels_list):
        for ms_index, input_ms in enumerate(input_ms_list):
            start_time = start_time_list[ms_index]
            hr_time = human_readable_time(start_time)

            log_ms_operation("predict", input_ms, hr_time)

            suffix = f"predict_{selfcal_loop + 1}"
            suffix += f"{source_type}_{skymodel_index}_modeldata"
            output_ms = temp_ms_name(input_ms, start_time, work_dir, suffix)
            out_predict.append(output_ms)

            logging_tag = f"{suffix}.{ms_index}"

            dp3_runner.predict(
                logging_tag,
                input_ms,
                output_ms,
                hr_time,
                directions_str[skymodel_index],
                skymodel,
                combined_solutions,
                config["selfcal_cycles"][selfcal_loop]["predict"]["dp3"],
            )

    dp3_runner.join()

    logger.info("Start subtract_sector_models")
    in_imaging = []
    if source_type == "outlier":
        nr_outliers = len(skymodels_list)
        nr_bright = 0
    elif source_type == "bright_source":
        nr_outliers = 0
        nr_bright = len(skymodels_list)
    else:
        raise ValueError(f"Source type '{source_type}' is not supported.")

    for ms_index, input_ms in enumerate(input_ms_list):
        start_time = start_time_list[ms_index]
        if not start_time:
            # subtract_sector_models needs a valid time. If start_time is None,
            # input_ms is already split, so extract the time from input_ms.
            hr_time = human_readable_time(get_start_times(input_ms)[0])
        else:
            hr_time = human_readable_time(start_time)

        log_ms_operation("subtract_sector_models", input_ms, hr_time)

        suffix = f"predict_{selfcal_loop + 1}_no_{source_type}s"
        output_ms = temp_ms_name(input_ms, start_time, work_dir, suffix)
        in_imaging.append(output_ms)

        subtract_sector_models(
            input_ms,
            output_ms,
            out_predict,
            nr_outliers=nr_outliers,
            nr_bright=nr_bright,
            reweight=False,
            starttime=hr_time,
            phaseonly=True,
        )

    logger.info("Done subtract_sector_models")

    # Remove temporary MSs with predicted modeldata.
    for model_ms in out_predict:
        shutil.rmtree(model_ms)

    return in_imaging


def applybeam_shift_average(
    dp3_runner,
    input_ms_or_list,
    selfcal_loop,
    settings,
):
    """
    Runs dp3_runner.applybeam for each input ms.

    Parameters
    ----------
    input_ms_or_list : str or list of str
        Either the initial input measurement set, which should be split, or a
        list of already split measurement sets, containing observational data.
    selfcal_loop : int
        The current iteration of the self calibration loop (0-based).
    settings: object
        Configuration settings for the processing, including data_fraction
        and minimum_time for chunking.

    Returns
    -------
    A list with the output ms for each input ms.
    """

    config = settings.configuration
    cycle_config = config["selfcal_cycles"][selfcal_loop]

    input_ms_list, start_time_list = create_input_lists(
        input_ms_or_list,
        cycle_config["data_fraction"],
        config["minimum_time"],
    )
    suffix = f"image_{selfcal_loop + 1}_prep"

    input_imaging_ms_list = []
    for ms_index, input_ms in enumerate(input_ms_list):
        start_time = start_time_list[ms_index]
        hr_time = human_readable_time(start_time)

        log_ms_operation("applybeam_shift_average", input_ms, hr_time)

        output_ms = temp_ms_name(
            input_ms, start_time, settings.work_dir, suffix
        )

        dp3_runner.applybeam(
            f"image_{selfcal_loop + 1}.{ms_index}",
            input_ms,
            hr_time,
            output_ms,
            cycle_config["image"]["applybeam"],
        )
        input_imaging_ms_list.append(output_ms)
    dp3_runner.join()

    return input_imaging_ms_list


def image(  # pylint: disable=too-many-arguments,too-many-locals
    dp3_runner,
    wsclean_runner,
    input_ms_or_list,
    solutions_to_apply,
    skymodel,
    vertices_filename,
    base_work_dir,
    selfcal_loop,
    bright_sources_skymodel=None,
):
    """
    Performs an imaging step in the self calibration workflow.

    Parameters
    ----------
    input_ms_or_list : str or list of str
        Either the initial input measurement set, which should be split, or a
        list of already split measurement sets, containing observational data.
    selfcal_loop : int
        The current iteration of the self calibration loop (0-based).
    """

    logger.info("run_pipeline:: Start image_%d", selfcal_loop + 1)

    config = wsclean_runner.settings.configuration
    image_config = config["selfcal_cycles"][selfcal_loop]["image"]
    image_prefix = image_config["wsclean"]["name"]

    image_work_dir = f"{base_work_dir}/image_{selfcal_loop + 1}"
    temp_dir = f"{image_work_dir}/tmp"
    os.makedirs(temp_dir, exist_ok=True)
    os.chdir(image_work_dir)

    input_imaging_ms_list = applybeam_shift_average(
        dp3_runner,
        input_ms_or_list,
        selfcal_loop,
        wsclean_runner.settings,
    )

    mask_filename = f"im_{selfcal_loop + 1}_{image_prefix}_mask.fits"
    if isinstance(input_ms_or_list, str):
        first_input_ms = input_ms_or_list
    elif isinstance(input_ms_or_list, list):
        first_input_ms = input_ms_or_list[0]
    reference_ra_deg, reference_dec_deg = get_phase_center(first_input_ms)

    # Create imaging mask
    logger.info("Start blank_image")
    blank_image(
        mask_filename,
        input_image=None,
        vertices_file=vertices_filename,
        reference_ra_deg=reference_ra_deg,
        reference_dec_deg=reference_dec_deg,
        cellsize_deg=config["image_scale"],
        imsize=[config["image_size"], config["image_size"]],
    )

    logger.info("Start make_region_file")
    # Observation parameters should be equal for all MSs.
    width = compute_observation_width(get_observation_params(first_input_ms))
    facets_file = "regions.ds9"
    make_region_file(
        skymodel,
        float(reference_ra_deg),
        float(reference_dec_deg),
        # Note: WSClean requires that all sources in the solution file
        # must have corresponding regions in the facets region file.
        # We ensure this by applying a 20% padding to the width.
        # The value of 20 % was determined experimentally. See
        # https://git.astron.nl/RD/rapthor/-/merge_requests/103#note_71941
        width["ra"] * 1.2,
        width["dec"] * 1.2,
        facets_file,
    )

    nmiter = get_imaging_n_iterations(
        input_imaging_ms_list,
        image_config["max_nmiter"],
        peel_bright_sources=False,
    )
    logger.info(
        "Number of major iterations for imaging is %d. Maximum was %d.",
        nmiter,
        image_config["max_nmiter"],
    )

    logger.info("Start WSClean. Logging to %s", wsclean_runner.log_filename)

    concatenated_ms = f"image_{selfcal_loop + 1}.ms"
    concatenate_ms(input_imaging_ms_list, concatenated_ms, delete_input=True)
    wsclean_runner.run(
        concatenated_ms,
        facets_file,
        solutions_to_apply,
        mask_filename,
        temp_dir,
        nmiter,
        image_config["wsclean"],
    )

    logger.info("Done WSClean")

    if bright_sources_skymodel:
        logger.info(
            "Start wsclean restore. Logging to %s", wsclean_runner.log_filename
        )
        wsclean_runner.restore(
            f"{image_prefix}-MFS-image",
            bright_sources_skymodel,
        )
        logger.info("Done wsclean restore")

    if image_config["filter_skymodel"]:
        logger.info("Start filter_skymodel.py")
        filter_skymodel(
            f"{image_prefix}-MFS-image.fits",
            f"{image_prefix}-MFS-image-pb.fits"
            if config["use_beam"]
            else None,
            f"{image_prefix}-sources-pb.txt",
            f"{image_prefix}",
            vertices_filename,
            beam_ms=concatenated_ms,
            threshisl=image_config["thresh_isl"],
            threshpix=image_config["thresh_pix"],
        )
        logger.info("Done filter_skymodel.py")
    else:
        logger.info("Skip filter_skymodel")
        os.rename(
            f"{image_prefix}-sources.txt", f"{image_prefix}.true_sky.txt"
        )
        if config["use_beam"]:
            os.rename(
                f"{image_prefix}-sources-pb.txt",
                f"{image_prefix}.apparent_sky.txt",
            )
        else:  # Remove file from previous run(s).
            try:
                os.remove(f"{image_prefix}.apparent_sky.txt")
            except FileNotFoundError:
                pass

    shutil.rmtree(concatenated_ms, ignore_errors=True)

    os.chdir(base_work_dir)
    logger.info("run_pipeline:: Done image_%d", selfcal_loop + 1)
