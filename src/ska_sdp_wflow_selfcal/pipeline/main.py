#!/usr/bin/env python3

#  SPDX-License-Identifier: BSD-3-Clause

# -*- coding: utf-8 -*-
" This script defines a SKA self calibration workflow"

import argparse
import logging
import os
from glob import glob
from pathlib import Path

import numpy as np
import tables

from ska_sdp_wflow_selfcal.pipeline.config.config import read_config_file
from ska_sdp_wflow_selfcal.pipeline.distribute import Distributor, start_dask
from ska_sdp_wflow_selfcal.pipeline.dp3_helper import Dp3Runner
from ska_sdp_wflow_selfcal.pipeline.operations import calibrate, image, predict
from ska_sdp_wflow_selfcal.pipeline.support.miscellaneous import (
    concatenate_ms,
    file_exists,
)
from ska_sdp_wflow_selfcal.pipeline.support.read_data import (
    calculate_n_chunks,
    get_phase_center,
    get_start_times,
)
from ska_sdp_wflow_selfcal.pipeline.support.skymodel_utils import (
    create_full_sky_model,
    download_skymodel,
    get_outliers,
    process_skymodels,
)
from ska_sdp_wflow_selfcal.pipeline.version import check_version
from ska_sdp_wflow_selfcal.pipeline.wsclean_helper import WSCleanRunner

CALIBRATE_INDEX = 0
PREDICT_INDEX = 1
IMAGE_INDEX = 2
OPERATION_MAP = {
    "calibrate": CALIBRATE_INDEX,
    "predict": PREDICT_INDEX,
    "image": IMAGE_INDEX,
}


def get_run_matrix(settings):
    """
    Determines which operations there are, which should be run and
    which should be skipped.

    Returns
    -------
        An N x 3 matrix with boolean values, where N is the number of self
        calibration cycles and each column corresponds to the 3 possible
        operations that each cycle may have: calibrate, predict and image.
    """

    # Convert the configuration to an initial matrix, where only operations
    # and cycles that the configuration has are enabled.
    cycles_config = settings.configuration["selfcal_cycles"]

    run_matrix = np.ones((len(cycles_config), 3), dtype=bool)
    for loop_index, loop_config in enumerate(cycles_config):
        if "predict" not in loop_config:  # Only "predict" is optional
            run_matrix[loop_index, PREDICT_INDEX] = False

    # Convert settings.resume_from_operation into loop and operation indices.
    resume_op = settings.resume_from_operation

    operation, loop = resume_op.split("_")
    try:
        # Raises KeyError if 'operation' has an unknown string.
        operation_index = OPERATION_MAP[operation]
        # Raises ValueError if 'loop' cannot be converted to int.
        loop_index = int(loop) - 1
        if loop_index < 0 or loop_index >= len(run_matrix):
            raise IndexError()
    except (IndexError, KeyError, ValueError) as exception:
        raise ValueError(f"Invalid operation: '{resume_op}'") from exception

    # If the operation is already skipped, it does not exist at all.
    if not run_matrix[loop_index, operation_index]:
        raise ValueError(f"Operation {resume_op} is not in the configuration")

    logger = logging.getLogger("ska-sdp-wflow-selfcal")
    logger.info("Start from %s.", resume_op)

    # Process settings.run_single_operation and update the matrix.
    if settings.run_single_operation == "True":
        run_matrix[:, :] = False
        run_matrix[loop_index, operation_index] = True
    else:
        run_matrix[0:loop_index, :] = False
        run_matrix[loop_index, 0:operation_index] = False

    return run_matrix


def get_filenames(
    working_dir,
    input_ms_path,
    data_fraction,
    minimum_time,
    suffix,
):
    """
    Get a list of filenames based on the input parameters.

    Parameters
    ----------
    working_dir: str
        Path to the working directory.
    input_ms_path: str
        Path to the input measurement set.
    data_fraction: float
        Fraction of data to be processed.
    minimum_time: float
        Minimum time interval for splitting the data.
    suffix: str
        Suffix to be appended to the generated filenames.

    Returns
    -------
    list of str
         A list of filenames generated based on the input parameters.
    """

    logger = logging.getLogger("ska-sdp-wflow-selfcal")

    input_ms_filename = os.path.basename(input_ms_path)
    n_chunks = calculate_n_chunks(
        input_ms_path,
        data_fraction,
        minimum_time,
    )
    # Determine times in the modified julian (calendar) days format
    mjd_times = [
        "mjd" + str(int(one_time))
        for one_time in get_start_times(input_ms_path, n_chunks)
    ]

    ms_list = []
    for mjd_time in mjd_times:
        file_name = f"{working_dir}/{input_ms_filename}.{mjd_time}.{suffix}"

        if not os.path.exists(file_name):
            logger.error(
                "run_pipeline:: File does not exist: %s. Run calibrate "
                "operation first.",
                file_name,
            )
        ms_list.append(file_name)
        logger.info("run_pipeline:: File collected:%s", file_name)

    return ms_list


# pylint: disable=too-many-locals, too-many-branches, too-many-statements
def run_pipeline(settings):
    """Run pipeline"""

    # Read and validate configuration file
    settings.configuration = read_config_file(settings.config_file)

    if not settings.logging_tag:
        settings.logging_tag = str(os.getpid())

    work_dir = settings.work_dir
    log_dir = f"{work_dir}/logs"
    os.makedirs(log_dir, exist_ok=True)
    os.chdir(work_dir)

    log_filename = f"{log_dir}/wflow-selfcal.{settings.logging_tag}.log"
    handler = logging.FileHandler(log_filename)
    handler.setLevel(level=logging.INFO)
    handler.setFormatter(
        logging.Formatter(
            "%(asctime)s.%(msecs)d %(name)s %(levelname)s %(message)s",
            datefmt="%Y-%m-%d %H:%M:%S",
        )
    )

    logger = logging.getLogger("ska-sdp-wflow-selfcal")
    logger.addHandler(handler)

    # Start logging things once the logger is fully configured
    logger.info("Called with settings:")
    for key, val in vars(settings).items():
        logger.info(f"    {key}: {val}")  # pylint: disable=W1203

    if len(settings.input_ms) > 1:
        logger.info("Concatenating input MSs.")
        concat_ms = str(Path(settings.work_dir) / "concat_msin.ms")
        concatenate_ms(settings.input_ms, concat_ms)
        settings.input_ms = concat_ms
    else:
        settings.input_ms = settings.input_ms[0]

    if settings.dask_scheduler is not None:
        start_dask(settings.dask_scheduler, logger)

    run_matrix = get_run_matrix(settings)

    # Check version
    check_successful = check_version(
        settings.dp3_path, settings.wsclean_cmd[-1]
    )
    if not check_successful and settings.ignore_version_errors != "True":
        raise RuntimeError(
            f"Version check failed. See log file {log_filename} for more "
            "info.\nUse --ignore_version_errors for ignoring these errors."
        )

    dp3_runner = Distributor(  # pylint: disable=E1102
        Dp3Runner, hasattr(settings, "dask_scheduler")
    )(settings)
    wsclean_runner = WSCleanRunner(settings)

    logger.info("run_pipeline:: Read phase center")
    [ra, dec] = get_phase_center(settings.input_ms)
    logger.info("phase center coordinates are: RA %s, DEC %s ", ra, dec)

    # Download initial skymodel if not present already
    initial_skymodel_filename = f"{work_dir}/initial.skymodel"
    if not file_exists(initial_skymodel_filename, "Download skymodel"):
        download_skymodel(ra, dec, initial_skymodel_filename)

    vertices_filename = f"{work_dir}/vertices.pkl"
    sector_skymodel_filename = f"{work_dir}/sector.skymodel"

    input_ms_or_list = settings.input_ms

    # selfcal_loop is an internal, 0-based self calibration loop index.
    # Towards users, e.g., in log messages and file names, indices are 1-based.
    for selfcal_loop in range(len(run_matrix)):
        selfcal_loop_config = settings.configuration["selfcal_cycles"][
            selfcal_loop
        ]
        if "predict" in selfcal_loop_config:
            predict_config = selfcal_loop_config["predict"]
            subtract_outliers = predict_config["subtract_outliers"]
            subtract_bright_sources = predict_config["subtract_bright_sources"]
        else:
            subtract_outliers = False
            subtract_bright_sources = False

        calibrate_skymodel_filename = (
            f"{work_dir}/calibrate_{selfcal_loop + 1}.skymodel"
        )
        solutions_filename = (
            f"{work_dir}/calibrate{selfcal_loop + 1}_solutions.h5"
        )

        if subtract_outliers:
            # Start from the initial input MS, instead of reusing existing MSs
            # where outliers are already subtracted.
            input_ms_or_list = settings.input_ms

        # All operations use this filename prefix for generating and/or
        # locating skymodel file(s) containing bright sources.
        if subtract_bright_sources:
            bright_sources_skymodel_prefix = (
                f"{work_dir}/bright_sources_{selfcal_loop + 1}"
            )
        else:
            bright_sources_skymodel_prefix = None

        # Calibrate
        if not run_matrix[selfcal_loop, CALIBRATE_INDEX]:
            logger.info("run_pipeline:: SKIP calibrate_%d", selfcal_loop + 1)
        else:
            if selfcal_loop == 0:
                skymodel_true_sky_filename = initial_skymodel_filename
                skymodel_apparent_sky_filename = None
            else:
                # Use the imaging directory and name of the previous loop.
                imaging_dir = f"{work_dir}/image_{selfcal_loop + 1 - 1}"
                imaging_prefix = settings.configuration["selfcal_cycles"][
                    selfcal_loop - 1
                ]["image"]["wsclean"]["name"]

                skymodel_true_sky_filename = (
                    f"{imaging_dir}/{imaging_prefix}.true_sky.txt"
                )
                skymodel_apparent_sky_filename = (
                    (f"{imaging_dir}/{imaging_prefix}.apparent_sky.txt")
                    if settings.configuration["use_beam"]
                    else None
                )

                if subtract_outliers:
                    # Create a true skymodel that contains outliers by
                    # concatenating the initial sky model with the one from the
                    # previous cycle. This step needs to be done if the
                    # data_fraction is different than the previous cycle. The
                    # pipeline then starts from the original input MS and
                    # (re)subtracts sources outside of the imaged area.
                    full_skymodel_filename = (
                        f"{work_dir}/full_{selfcal_loop + 1}.skymodel"
                    )
                    create_full_sky_model(
                        initial_skymodel_filename,
                        skymodel_true_sky_filename,
                        full_skymodel_filename,
                    )
                    skymodel_true_sky_filename = full_skymodel_filename
                    skymodel_apparent_sky_filename = None

            if subtract_bright_sources:
                # Remove skymodel files from previous runs. Otherwise the
                # similar glob() call for predict may use old files which may
                # contain patch names that don't exist in the current run.
                for old_skymodel in glob(
                    bright_sources_skymodel_prefix + "_*"
                ):
                    os.remove(old_skymodel)

            process_skymodels(
                selfcal_loop_config["calibrate"],
                settings.input_ms,
                vertices_filename,
                sector_skymodel_filename,
                skymodel_true_sky_filename,
                calibrate_skymodel_filename,
                skymodel_apparent_sky_filename,
                bright_sources_skymodel_prefix,
            )

            calibrate(
                dp3_runner,
                input_ms_or_list,
                calibrate_skymodel_filename,
                work_dir,
                selfcal_loop,
                settings,
                solutions_filename,
            )

        # Predict
        if not run_matrix[selfcal_loop, PREDICT_INDEX]:
            # Only log a skipped predict operation if it exists.
            if "predict" in selfcal_loop_config:
                logger.info("run_pipeline:: SKIP predict_%d", selfcal_loop + 1)

            # Collect MS files in the working directory from a previous run.
            # The files in the list must be sorted in time, otherwise the
            # imaging operation on the concatenated MS will fail.

            # Any future operation may need MSs without outliers.
            if subtract_outliers and (
                np.any(run_matrix[selfcal_loop, PREDICT_INDEX + 1 :])
                or np.any(run_matrix[selfcal_loop + 1 :, :])
            ):
                input_ms_or_list = get_filenames(
                    work_dir,
                    settings.input_ms,
                    selfcal_loop_config["data_fraction"],
                    settings.configuration["minimum_time"],
                    # Use the same output filename suffix as in predict().
                    f"predict_{selfcal_loop + 1}_no_outliers",
                )

            # Only the next image operation needs MSs without bright sources.
            if (
                subtract_bright_sources
                and run_matrix[selfcal_loop, IMAGE_INDEX]
            ):
                ms_list_no_outliers_no_bright_sources = get_filenames(
                    work_dir,
                    settings.input_ms,
                    selfcal_loop_config["data_fraction"],
                    settings.configuration["minimum_time"],
                    # Use the same output filename suffix as in predict().
                    f"predict_{selfcal_loop + 1}_no_bright_sources",
                )
        else:
            # The predict operation is present in the configuration and
            # enabled. Subtract outliers and/or bright sources.
            if subtract_outliers:
                logger.info(
                    "run_pipeline:: Start predict_%d - subtract outliers",
                    selfcal_loop + 1,
                )

                outliers_skymodel_filename = (
                    f"{work_dir}/predict{selfcal_loop + 1}_outliers.skymodel"
                )

                get_outliers(
                    calibrate_skymodel_filename,
                    sector_skymodel_filename,
                    outliers_skymodel_filename,
                )

                input_ms_or_list = predict(
                    dp3_runner,
                    settings.input_ms,
                    [outliers_skymodel_filename],
                    work_dir,
                    solutions_filename,
                    selfcal_loop,
                    "outlier",
                    settings,
                )

                logger.info(
                    "run_pipeline:: Done predict_%d - subtract outliers",
                    selfcal_loop + 1,
                )

            if subtract_bright_sources:
                logger.info(
                    "run_pipeline:: "
                    "Start predict_%d - subtract bright sources",
                    selfcal_loop + 1,
                )

                bright_sources_list = glob(
                    bright_sources_skymodel_prefix + "_*"
                )
                if not bright_sources_list:
                    logger.error(
                        "run_pipeline:: bright sources skymodels are not "
                        "available. Run calibrate_%s operation to generate "
                        "them.",
                        selfcal_loop + 1,
                    )
                ms_list_no_outliers_no_bright_sources = predict(
                    dp3_runner,
                    input_ms_or_list,
                    bright_sources_list,
                    work_dir,
                    solutions_filename,
                    selfcal_loop,
                    "bright_source",
                    settings,
                )

                logger.info(
                    "run_pipeline:: Done predict_%d - subtract bright sources",
                    selfcal_loop + 1,
                )

        # Image
        if not run_matrix[selfcal_loop, IMAGE_INDEX]:
            logger.info("run_pipeline:: SKIP image_%d", selfcal_loop + 1)
        else:
            if subtract_bright_sources:
                # Use the MSs created by the 'bright_source' predict operation.
                ms_list_input_imaging = ms_list_no_outliers_no_bright_sources
                bright_sources_skymodel_filename = (
                    f"{bright_sources_skymodel_prefix}.skymodel"
                )
            else:
                # Use the MSs where bright sources were not subtracted.
                ms_list_input_imaging = input_ms_or_list
                bright_sources_skymodel_filename = None

            image(
                dp3_runner,
                wsclean_runner,
                ms_list_input_imaging,
                solutions_filename,
                calibrate_skymodel_filename,
                vertices_filename,
                work_dir,
                selfcal_loop,
                bright_sources_skymodel_filename,
            )


class SplitAction(argparse.Action):
    """
    Stores an argument string, containing spaces, as a list of strings.
    """

    def __init__(self, option_strings, dest, nargs=None, **kwargs):
        if nargs is not None:
            raise ValueError("nargs not allowed")
        super().__init__(option_strings, dest, **kwargs)

    def __call__(self, parser, namespace, values, option_string=None):
        setattr(namespace, self.dest, values.split())


def main():
    """
    Performs global initialisation.
    Parses command line arguments and passes them to run_pipeline().
    """
    # The 'tables' module hides HDF5 error messages by default when loading it,
    # When for example EveryBeam cannot load its coefficients, HDF5 also no
    # longer prints any error message. -> Disable silencing those errors.
    tables.silence_hdf5_messages(False)

    parser = argparse.ArgumentParser(
        description="Run direction dependent calibration pipeline\n",
        formatter_class=argparse.RawTextHelpFormatter,
    )

    parser.add_argument(
        "--dp3_path",
        help="Path to DP3 executable",
        type=str,
        default="DP3",
    )
    parser.add_argument(
        "--wsclean_cmd",
        "--wsclean_path",
        help="Full WSClean command, including 'mpirun' prefix if needed",
        type=str,
        action=SplitAction,
        default=["wsclean"],
    )
    parser.add_argument(
        "--restore_args",
        help="Extra WSClean arguments for restore runs",
        type=str,
        action=SplitAction,
        default=[],
    )
    parser.add_argument(
        "--input_ms",
        help="Path to input Measurement Set(s)",
        nargs="+",
        type=str,
        required=True,
    )
    parser.add_argument(
        "--work_dir",
        help="Path to working directory, where all output files will be saved",
        type=str,
        default="working_dir",
    )
    parser.add_argument(
        "--logging_tag",
        help="Tag for log files. Defaults to the process id",
        type=str,
        default="",
    )
    parser.add_argument(
        "--resume_from_operation",
        help="Resume from any given operation",
        type=str,
        default="calibrate_1",
    )
    parser.add_argument(
        "--run_single_operation",
        help="Only run the operation given via resume_from_operation",
        type=str,
        default="False",
    )
    parser.add_argument(
        # TODO Use dashes to separate words.
        # Other options contain both dashes and underscores
        # Generally dashes only in command line options is preferred,
        # but it should be applied consistently then.
        "--dask_scheduler",
        help="The address of a Dask scheduler server as a string "
        "'<IP address>:<port number>' or '<hostname>:<port number>'. "
        "When provided, the pipeline uses Dask for executing multiple DP3 "
        "runs concurrently. E.g. on multiple compute nodes with multiple "
        "processes per node. The Dask scheduler and workers should then "
        "already be running. See the example sbatch scripts for an example on "
        "how to start Dask. If this argument is not provided, the pipeline "
        "runs all DP3 processes sequentially, without using Dask. Note that "
        "DP3 processes are still multi-threaded.",
        type=str,
        default=None,
    )
    parser.add_argument(
        "--ignore_version_errors",
        help="If True, the pipeline will run also if there is a mismatch in "
        "the expected DP3 and WSClean commits, or if multiple binaries are "
        "found for casacore and everybeam",
        type=str,
        default="False",
    )
    parser.add_argument(
        "--config",
        help="Configuration file in yml format. "
        "See the config file in config/ for an example.",
        dest="config_file",
        type=str,
        required=True,
    )

    settings = parser.parse_args()

    # DP3 only needs custom environmental variables on the CI.
    settings.dp3_environment = None

    # Convert input paths to absolute paths
    settings.dp3_path = str(Path(settings.dp3_path).absolute())
    settings.work_dir = str(Path(settings.work_dir).absolute())
    for i, input_ms in enumerate(settings.input_ms):
        settings.input_ms[i] = str(Path(input_ms).absolute())

    run_pipeline(settings)


if __name__ == "__main__":
    main()
