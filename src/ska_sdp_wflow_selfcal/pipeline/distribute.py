#  SPDX-License-Identifier: BSD-3-Clause

"""
This module provides the following;
- start_dask and stop_dask functions for connecting to and stopping the Dask
  scheduler and workers.
- A Distributor class, which wraps existing classes to create a version
  whose methods can be executed asynchronously through Dask. This allows for
  both execution using multiple nodes and multiple processes per node.
  See the documentation of the Distributor class for further details.
"""


import atexit
import os
import sys

import dask
import dask.distributed

this = sys.modules[__name__]
this.dask_client = None


def _stop_dask():
    """
    Stop external DASK programs. This function is very fault tolerant, since it
    is intended to be run at python program exit.
    """
    # Try shutting down the connected scheduler and workers gracefully.
    if this.dask_client:
        this.dask_client.shutdown()

    this.dask_client = None


def stop_dask():
    """Stop the dask scheduler and workers started via start_dask"""
    atexit.unregister(_stop_dask)
    _stop_dask()


def start_dask(dask_scheduler_address, logger):
    """
    Connect to the Dask scheduler that has been started in the background
    together with the Dask workers. Do nothing if this function was called
    before, and the scheduler is connected already.
    Automatically stop the background processes at program exit.
    Use 'logger' for (short) messages related to starting and stopping the
    dask processes. Refer to the documentation in the example script that
    starts the Dask scheduler and workers for details on e.g. the log file
    location.
    """

    if (
        this.dask_client
        and this.dask_client.scheduler
        and this.dask_client.scheduler.address
    ):
        return

    atexit.register(_stop_dask)

    # Ensure that other mpirun calls (e.g., for wsclean) can run together
    # with the dask workers (that are idle then).
    os.environ["SLURM_OVERLAP"] = "yes"

    logger.info("Connecting to Dask Scheduler.")
    this.dask_client = dask.distributed.Client(dask_scheduler_address)
    logger.info("Dask client connected to scheduler: " + str(this.dask_client))


class Distributor:  # pylint: disable=R0903
    """
    Wrapper class factory to create distributed versions of runner classes.
    It executes its methods asynchronously through dask.delayed.
    The return value is then a dask Delayed object.
    To wait for the asynchronous calls to finish call `runner.join()`.
    This returns a tuple of the actual results.
    To indicate a dependence between calls, pass (a tuple of) the delayed
    result(s) as the `depends_on` keyword arguments.

    Usage example:
        runner = Distributor(Runner)(runner_arg1, runner_arg2)

        result1 = runner.method1()
        result2 = runner.method2(method2_arg1, method2_arg2)
        runner.method3(method3_arg1,
            depends_on=(result1, result2))
        runner.join()

        runner.method1()
        runner.join()
    """

    @staticmethod
    def __new__(distributor_cls, runner_cls, run_distributed=True):
        """
        Returns a wrapper class around the class `runner_cls`

        If `run_distributed == False` then this is only a wrapper that
        presents the interface of a distributed runner, but executes
        synchronously.

        If `run_distributed == True` the calls to the methods are actually
        executed asynchronously through dask.delayed.
        """

        def wrap_method(method_name):
            """
            Function to create a wrapper for the method from the runner_cls
            with the name `method_name`.
            """

            # Extract the method from the runner_cls
            method = getattr(runner_cls, method_name)

            def method_stripped(
                *args, depends_on=(), **kwargs
            ):  # pylint: disable=W0613
                """
                Thin wrapper that strips the `depends_on` argument from the
                keyword arguments and then passes the argument and keyword
                arguments to `method`.
                """
                return method(*args, **kwargs)

            def wrapper(self, *args, **kwargs):
                """
                Wrapper function that either executes the method synchronously
                or asynchronously through dask.delayed.
                """
                if run_distributed:
                    lazy_result = dask.delayed(method_stripped)(
                        self.runner, *args, **kwargs
                    )
                    self.lazy_results.append(lazy_result)
                    return lazy_result
                return method_stripped(self.runner, *args, **kwargs)

            return wrapper

        class WrappedClass:
            """
            Wrapper class for a runner

            """

            def __init__(self, *args, **kwargs):
                """
                Constructor passes all arguments to the constructor of the
                wrapped class.
                """
                self.runner = runner_cls(*args, **kwargs)
                self.lazy_results = []

            def join(self):
                """
                Evaluate all previous method calls, wait for the results and
                return them. If run_distributed is False, this method doesn't
                do anything.
                """
                if run_distributed:
                    results = dask.compute(*self.lazy_results)
                    self.lazy_results = []
                    return results
                return None

        # Iterate over all (public) methods in runner_cls and replace them by a
        # wrapped version
        for method_name in dir(runner_cls):
            if method_name.startswith("_"):
                continue
            setattr(WrappedClass, method_name, wrap_method(method_name))

        return WrappedClass
