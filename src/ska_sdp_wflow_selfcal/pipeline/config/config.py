"""
Module to read and validate a yml configuration file
"""

import json
import os
from pathlib import Path
from typing import Any, Optional, Sequence, Union

import yaml
from jsonschema import Draft202012Validator
from referencing import Registry, Resource
from referencing.jsonschema import DRAFT202012

ScalarArg = Union[str, bool, int, float, Path]
Arg = Union[ScalarArg, Sequence[ScalarArg]]
SCHEMAS_DIR = Path(__file__).parent / "schemas"


class Action(yaml.YAMLObject):
    """
    Base class for all Actions
    Actions represent a custom tag in a yaml file that performs some action,
    i.e. modifies the content.
    """

    yaml_loader = yaml.SafeLoader

    # pylint: disable=unused-argument
    def eval(self, value, config_file, contexts):
        """
        Evaluate the action
        Child classes should override this function

        Arguments
        ---------
        value: Any
            If the Action is used as key in a key,value pair in a dict,
            the value part is passed as value argument.
        config_file: str
            Name of the current config file.
        contexts: list of dictionaries
            List of scopes that can be used to resolve names

        Returns
        -------
        The override should return the value to replace the tag, or,
        if the custom tag is intended to be used as key in a dictionary,
        the return value should be a dictionary that will replace the custom
        tag
        """
        raise RuntimeError("eval function for this Action was not implemented")


class ConditionalAction(Action):
    """
    Class to represent an `!if` custom tag in a yaml file

    Example usage of the !if tag in a yaml file:

    use_beam: true
    options:
      !if use_beam:
        beammode: full
        beaminterval: 60
      other: foo

    Will be evaluated to

    options:
      beammode: full
      beaminterval: 60
      other: foo
    """

    yaml_tag = "!if"

    def __init__(self, condition):
        self.condition = condition

    # pylint: disable=unused-argument
    def eval(self, value, config_file, contexts):
        """
        Evaluates the !if action

        Arguments
        ---------
        value: any
            key,value pairs to append if the condition evaluates to true
        config_file: str
            unused
        contexts: list of dictionaries
            List of scopes used to resolve the condition

        Returns
        -------
        The value argument if the condition evaluates to true
        """
        return value if find_in_contexts(self.condition, contexts) else {}

    @classmethod
    def from_yaml(cls, loader, node):
        return ConditionalAction(node.value)


class IncludeAction(Action):
    """
    Class to represent an `!include` custom tag in a yaml file

    Example usage of the !include tag in a yaml file:

    !include common.yml:

    This inserts the contents of common.yml in the current yaml file
    """

    yaml_tag = "!include"

    def __init__(self, filename):
        self.filename = filename

    # pylint: disable=unused-argument
    def eval(self, value, config_file, contexts):
        """
        Evaluates the include tag.

        Arguments
        ---------
        config_file: str or path
            The name of the config_file. Can be a relative path with respect
            to the current configuration file

        Returns
        -------
        A dictionary containing the configuration in config_file.

        The current contexts are passed on when the config_file is read and
        processed. The included file can reference content in the main file.
        """
        filename = os.path.join(os.path.dirname(config_file), self.filename)
        return read_config_file(filename, validate=False, contexts=contexts)

    @classmethod
    def from_yaml(cls, loader, node):
        return IncludeAction(node.value)


class ReferenceAction(Action):
    """
    Class to represent an `!ref` custom tag in a yaml file

    Example usage of the !ref tag in a yaml file:

    foo: bar
    foobar: [foo, !ref[foo]]

    The resulting value for foobar is
    foobar: foo, bar

    """

    yaml_tag = "!ref"

    def __init__(self, varname):
        self.varname = varname

    # pylint: disable=unused-argument
    def eval(self, value, config_file, contexts):
        """
        Returns the value of the variable found in the contexts
        """
        value = find_in_contexts(self.varname, contexts)
        return value

    @classmethod
    def from_yaml(cls, loader, node):
        return ReferenceAction(node.value)


def retrieve_from_filesystem(uri: str) -> Resource:
    """
    Helper function to retrieve schemas referenced in others.

    See:
    https://python-jsonschema.readthedocs.io/en/latest/referencing/#resolving-references-from-the-file-system
    """
    path = SCHEMAS_DIR / uri.removeprefix("http://localhost/")
    contents = json.loads(path.read_text())
    return DRAFT202012.create_resource(contents)


def load_config_schema_dict() -> dict:
    """
    Load the schema dictionary for a pipeline config file.
    """
    path = SCHEMAS_DIR / "config.json"
    return json.loads(path.read_text())


REGISTRY = Registry(retrieve=retrieve_from_filesystem)
VALIDATOR = Draft202012Validator(load_config_schema_dict(), registry=REGISTRY)


def validate_calibrate(calibrate_config: dict, cycle_index: int) -> None:
    """Validates the calibrate configuration section of a self-cal cycle."""

    # calibrate supports only one or two solver steps, where the first step
    # has "scalarphase" mode and the second step has "complexgain" mode.
    solver_modes = []
    for step_config in calibrate_config["dp3"]["steps"].values():
        if step_config and step_config["type"] == "ddecal":
            solver_modes.append(step_config["mode"])

    if len(solver_modes) == 0 or len(solver_modes) > 2:
        raise ValueError(
            f"{len(solver_modes)} solver steps found in "
            f"calibrate_{cycle_index + 1}. "
            "calibrate supports only 1 or 2 solver steps."
        )
    if solver_modes[0] != "scalarphase":
        raise ValueError(
            f"Unsupported mode '{solver_modes[0]}' found for the first "
            f"solver step of calibrate_{cycle_index + 1}. "
            "calibrate only supports 'scalarphase' mode here."
        )
    if len(solver_modes) > 1 and solver_modes[1] != "complexgain":
        raise ValueError(
            f"Unsupported mode '{solver_modes[1]}' found for the second "
            f"solver step of calibrate_{cycle_index + 1}. "
            "calibrate only supports 'complexgain' mode here."
        )


def validate_image(image_config: dict) -> None:
    """Validates the image configuration section of a self-cal cycle."""
    if image_config["filter_skymodel"]:
        if not image_config["thresh_isl"] or not image_config["thresh_pix"]:
            raise ValueError(
                "Both 'thresh_isl' and 'thresh_pix' are required when "
                "'filter_skymodel' is true."
            )


def validate_config(config: dict) -> None:
    """
    Validate a pipeline configuration dictionary.

    Args:
        config: The pipeline configuration dictionary to validate.

    Raises:
        jsonschema.exceptions.ValidationError: If the configuration
            does not conform to the schema.

    Notes:
        Setting some DP3 and WSClean options may be refused; for example, if
        they are strictly under the control of the pipeline.
    """
    VALIDATOR.validate(instance=config)

    cycles_config = config["selfcal_cycles"]

    if not cycles_config:
        raise ValueError("No selfcal cycles found in configuration")

    previous_data_fraction = None
    for cycle_index, cycle_config in enumerate(cycles_config):
        # predict must have subtract_outliers and/or subtract_bright_sources.
        if (
            "predict" in cycle_config
            and not cycle_config["predict"]["subtract_outliers"]
            and not cycle_config["predict"]["subtract_bright_sources"]
        ):
            raise ValueError(
                "Subtracting outliers and bright sources are both disabled "
                f"in cycle {cycle_index+1}"
            )

        # When the data fraction changes, predict must subtract outliers.
        current_data_fraction = cycle_config["data_fraction"]
        if (
            previous_data_fraction
            and current_data_fraction != previous_data_fraction
        ):
            if (
                "predict" not in cycle_config
                or not cycle_config["predict"]["subtract_outliers"]
            ):
                raise ValueError(
                    "Subtracting outliers is disabled in cycle "
                    f"{cycle_index+1}, while the data_fraction of this cycle "
                    "differs from the previous cycle"
                )
        previous_data_fraction = current_data_fraction

        validate_calibrate(cycle_config["calibrate"], cycle_index)

        validate_image(cycle_config["image"])


# pylint: disable=dangerous-default-value
def read_config_file(
    config_file: Path, validate: bool = True, contexts: list[dict] = []
) -> None:
    """
    Validate and returns a pipeline configuration file in YAML format.
    Same as `validate_config()` otherwise.
    """

    with open(config_file, "r", encoding="utf-8") as file_object:
        config = yaml.safe_load(file_object)
    config = post_process_config(config, config_file, contexts)
    if validate:
        validate_config(config)
    return config


def find_in_contexts(key, contexts):
    """
    Look up a key in contexts

    Arguments
    ---------
    key: str
    contexts: list of dicts

    contexts is list of scopes (dicts) to search for variable names.
    The contexts are scanned from the last (deepest nested) to
    the first (top level)
    """
    for context in contexts[::-1]:
        if key in context:
            return context[key]
    raise RuntimeError(f"Could not find key {key} in configuration.")


def post_process_config(config, config_file, contexts):
    """
    Evaluates the custom tag actions in the configuration

    Arguments
    ---------
    config: Action, dict or list

    Returns
    -------
    Updated config

    The config argument is recursively scanned for Actions.
    For each Action that is found its eval() method is called.
    If the Action is a value, by itself, in a list or a dict,
    then it is replaced by the returned value of the eval() method.
    If the Action is a key in a dictionary, that key is removed and
    the dict returned by the eval() method is appended.
    """
    if isinstance(config, Action):
        new_dict = config.eval(config, config_file, contexts)
        return post_process_config(new_dict, config_file, contexts)
    if isinstance(config, list):
        return [
            post_process_config(item, config_file, contexts) for item in config
        ]
    if isinstance(config, dict):
        contexts = contexts + [config]
        for key, value in list(config.items()):
            if isinstance(key, Action):
                new_dict = key.eval(value, config_file, contexts)
                new_dict = post_process_config(new_dict, config_file, contexts)
                config.pop(key)
                config.update(new_dict)
            elif isinstance(value, Action):
                new_value = value.eval(value, config_file, contexts)
                new_value = post_process_config(
                    new_value, config_file, contexts
                )
                config[key] = new_value
            elif isinstance(value, list):
                config[key] = post_process_config(value, config_file, contexts)
            value = post_process_config(value, config_file, contexts)
    return config


def render_dp3_option(key: str, value: Optional[Any]) -> list[str]:
    """
    Converts a single yml config item into a DP3 argument.
    The config item may recursively contain more config items itself.

    Arguments
    ---------
    key: str
        the DP3 step name (e.g., ddecal, predict, etc.).

    value: Any | None
        the configuration associated with the step
        (e.g., str, bool, int, float,
        list (or list of lists), dict (of all datatypes))

    Returns
    -------
    An iterable with the new DP3 command(s) appended as strings.

    Generally, the function appends:
        "key=value" if value is an str, int, bool, float or list
    e.g.,
        "key=[item_0,item_1,...,item_n]"
        where value is a list.

    If value is None, then it appends the following:
        "key="

    If value is a dict, then it appends the following for every item:
        "key.sub_key=sub_value"
    where sub_value is an str, int, bool, float or list
    e.g.,
        "key.sub_key=[[stn_000,stn_001],[10.0,False]]"
        where sub_value is a list of lists.

    If sub_value is a dict, the function runs recursively until all nested
    dict items are resolved
    e.g.,
        "key.sub_key.sub_key_0=sub_value_0"
        where {key: {sub_key: {sub_key_0, sub_value_0}}} is the input
        and sub_value_0 is not a dict.

    Note
    ----
    All list items are allowed except dict, which would be an illogical
    way of passing arguments in DP3.
    """
    if isinstance(value, dict):
        output = []
        for sub_key, sub_value in value.items():
            output += render_dp3_option(f"{key}.{sub_key}", sub_value)
        return output

    if isinstance(value, list):
        # Removing spaces from list as a precaution
        # following example in ddecal (antennaconstraint)
        # https://dp3.readthedocs.io/en/latest/steps/DDECal.html
        value = [f"{item}".replace(" ", "") for item in value]
        value = "[" + ",".join(value).replace("'", "").replace('"', "") + "]"

    return [f"{key}=" if value is None else f"{key}={value}"]


def render_dp3_config(config: dict[str, Optional[Any]]) -> list[str]:
    """
    Converts yml config values into a list with DP3 arguments.
    """
    args = []

    for key, value in config.items():
        if key == "steps":
            steps = ",".join(value)
            args.append(f"steps=[{steps}]")

            for step_name, step_config in value.items():
                # If a step in DP3 has no configuration, it will
                # not expect any commands of the form 'step=';
                # otherwise, an exception will be raised.
                if step_config is not None:
                    args += render_dp3_option(step_name, step_config)

        else:
            args += render_dp3_option(key, value)

    return args


def render_scalar_arg(arg: ScalarArg) -> str:
    """
    Convert a ScalarArg to a string type.
    """
    if isinstance(arg, Path):
        return arg.as_posix()

    if isinstance(arg, bool):
        return str(arg).lower()

    return str(arg)


def render_wsclean_option(key: str, arg: Arg) -> list[str]:
    """
    Appropriately render a WSClean option as a list of strings.
    """
    # Boolean options are dealt with first. Examples:
    # key="multiscale", arg=True  is rendered as "-multiscale"
    # key="multiscale", arg=False is ignored
    dashed_key = "-" + key
    if isinstance(arg, bool):
        return [dashed_key] if arg else []

    if isinstance(arg, (list, tuple)):
        rendered_arg = [render_scalar_arg(item) for item in arg]
    else:
        rendered_arg = [render_scalar_arg(arg)]
    return [dashed_key] + rendered_arg
