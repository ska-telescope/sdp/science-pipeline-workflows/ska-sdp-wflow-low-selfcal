#  SPDX-License-Identifier: BSD-3-Clause

"""This module contains commands to use DP3"""
import logging
import os
from subprocess import STDOUT, CalledProcessError, check_call

from ska_sdp_wflow_selfcal.pipeline.config.config import render_dp3_config
from ska_sdp_wflow_selfcal.pipeline.support.read_data import (
    calculate_n_times,
    get_antennas,
    get_observation_params,
    get_patches,
)

logger = logging.getLogger(__name__)
logger.setLevel(logging.INFO)

SOLVER_TYPE = "ddecal"


class Dp3Runner:
    """This class contains commands to use DP3"""

    def __init__(self, settings):
        logs_dir = f"{settings.work_dir}/logs"
        os.makedirs(logs_dir, exist_ok=True)
        self.log_filename_base = f"{logs_dir}/DP3.{settings.logging_tag}"

        self.settings = settings

        self.environment = os.environ.copy()
        if settings.dp3_environment:
            # There are extra environmental variables provided. Make sure that
            # check_call() is provided with all other env. vars it needs too.
            self.environment.update(settings.dp3_environment)

    def run_dp3(self, pipeline_config, file_config, logging_name):
        """
        Runs DP3 and writes the DP3 command and its output to a log file.

        Parameters:
        - pipeline config: Internal pipeline configuration for the DP3 command.
        - file_config: User configuration from the YML file.
        - logging_name: Description of the part of the pipeline which is run.
          It will be included in the log file name.
        """
        command = (
            [self.settings.dp3_path]
            + render_dp3_config(pipeline_config)
            + render_dp3_config(file_config)
        )
        log_filename = self.log_filename_base + f".{logging_name}.log"
        with open(log_filename, "a", encoding="utf-8") as outfile:
            outfile.write(f"Running: {' '.join(command)}\n")
        # Closing 'outfile' before starting DP3 ensures that the command is
        # always printed above the DP3 output.

        with open(log_filename, "a", encoding="utf-8") as outfile:
            try:
                check_call(
                    command,
                    stderr=STDOUT,
                    stdout=outfile,
                    env=self.environment,
                )
            except CalledProcessError as exception:
                with open(log_filename, "r", encoding="utf-8") as file:
                    last_lines = "".join(file.readlines()[-100:])
                    raise RuntimeError(
                        "DP3 failed with the following last lines of output: "
                        f"\n{last_lines}"
                    ) from exception

    def predict(  # pylint: disable=too-many-arguments
        self,
        logging_name,
        msin,
        msout,
        starttime,
        directions,
        input_skymodel,
        solutions_to_apply,
        predict_config,
    ):
        """This workflow performs direction-dependent prediction of sector sky
        models and subtracts the resulting model data from the input data,
        reweighting if desired. The resulting data are suitable for imaging."""

        step_names = list(predict_config["steps"].keys())
        if len(step_names) != 1:
            raise RuntimeError("predict does not support multiple DP3 steps")
        step_name = step_names[0]

        config = {
            "msin": msin,
            "msin.ntimes": calculate_n_times(
                msin, self.settings.configuration["minimum_time"]
            ),
            "msout": msout,
            f"{step_name}.applycal.parmdb": solutions_to_apply,
            f"{step_name}.directions": directions,
            f"{step_name}.sourcedb": input_skymodel,
        }

        if starttime:
            config["msin.starttime"] = starttime

        self.run_dp3(config, predict_config, logging_name)

    def calibrate(  # pylint: disable=too-many-arguments
        self,
        logging_name,
        msin,
        starttime,
        input_skymodel,
        calibrate_config,
    ):
        """
        Performs calibration using a single DP3 call.

        Parameters
        ----------
        logging_name: str
            Prefix for generating log messages. Also used as prefix for
            output solution filenames, which are returned.
            When running multiple calibrate() calls in parallel, each call
            should therefore use a different logging_name.
        calibrate_config: dict
            DP3 step configuration, which should contain one or more solver
            steps. If two solver steps are consecutive, configures the steps
            so the second step reuses predicted visibilities from the
            first step. If there is a step between solver steps, each solver
            step does its own predictions.

            Typically, the first solver step performs fast phase-only
            calibration (with core stations constrained to have
            the same solutions), which corrects for ionospheric effects.

            If there is a second solver step, it typically performs an
            unconstrained slow gain calibration, which corrects for
            station-to-station differences.

        Returns
        -------
        A list with the solution filename for each solver step.
        """

        steps = calibrate_config["steps"]

        config = {
            "msin": msin,
            "msin.ntimes": calculate_n_times(
                msin, self.settings.configuration["minimum_time"]
            ),
        }

        if starttime:
            config["msin.starttime"] = starttime

        solver_count = 0
        for step_config in list(steps.values()):
            if step_config and step_config["type"] == SOLVER_TYPE:
                solver_count += 1

        solver_index = 0
        solutions = []
        for step_index, (step_name, step_config) in enumerate(steps.items()):
            if not step_config or step_config["type"] != SOLVER_TYPE:
                continue

            config.update(
                self.get_solver_model_config(input_skymodel, steps, step_index)
            )

            if step_config["mode"] == "scalarphase":
                config[f"{step_name}.solint"] = 1
                config[f"{step_name}.smoothnessrefdistance"] = 0.0
                config[
                    f"{step_name}.smoothnessreffrequency"
                ] = get_observation_params(msin)["ref_freq"]
                if solver_count == 2:
                    config[
                        f"{step_name}.antennaconstraint"
                    ] = self.get_default_antenna_constraint(msin)
            else:
                config[f"{step_name}.solint"] = config["msin.ntimes"]

            solution_filename = os.path.join(
                self.settings.work_dir,
                f"{logging_name}.{solver_index}.{step_config['mode']}.h5parm",
            )
            solutions.append(solution_filename)
            config[f"{step_name}.h5parm"] = solution_filename
            solver_index += 1

        self.run_dp3(config, calibrate_config, logging_name)

        return solutions

    def applybeam(  # pylint: disable=too-many-arguments
        self, logging_name, msin, starttime, msout, applybeam_config
    ):
        """This step uses DP3 to prepare the input data for imaging."""

        config = {
            "msin": msin,
            "msin.ntimes": calculate_n_times(
                msin, self.settings.configuration["minimum_time"]
            ),
            "msout": msout,
        }
        if starttime:
            config["msin.starttime"] = starttime

        self.run_dp3(
            config,
            applybeam_config,
            logging_name,
        )

    @staticmethod
    def get_default_antenna_constraint(measurement_set):
        """Get the default antenna_constraint for the measurement_set.
        This function is only implemented for the LOFAR telescope,
        in which case it selects the core stations."""
        observation_parms = get_observation_params(measurement_set)
        if observation_parms["telescope_name"] == "LOFAR":
            core_stations = [
                ant
                for ant in get_antennas(measurement_set)
                if ant.startswith("CS")
            ]
            return f"[{core_stations}]"
        return "[]"

    @staticmethod
    def get_solver_model_config(input_skymodel, steps, step_index):
        """
        Generate DDECal parset configuration for the predicted model data.

        The generated configuration reuses predicted model data if possible.
        """
        step_name = list(steps.keys())[step_index]

        # If the previous step is also a ddecal step, keep model
        # data in the previous step and reuse it in the current step.
        previous_step_name = None
        previous_step_config = None
        if step_index > 0:
            previous_step_name = list(steps.keys())[step_index - 1]
            previous_step_config = list(steps.values())[step_index - 1]

        if previous_step_name and previous_step_config["type"] == SOLVER_TYPE:
            directions_list = get_patches(input_skymodel)[0]
            reusemodels = ",".join(
                [
                    previous_step_name + "." + direction
                    for direction in directions_list
                ]
            )
            config = {
                f"{previous_step_name}.keepmodel": True,
                f"{step_name}.reusemodel": f"[{reusemodels}]",
            }
        else:  # Predict model data using the input sky model.
            config = {f"{step_name}.sourcedb": input_skymodel}

        return config
