PYTHON_SWITCHES_FOR_FLAKE8=--extend-ignore=E203
PYTHON_SWITCHES_FOR_PYLINT=--disable=fixme --good-names=ra,i,x,y --load-plugins pylint_pytest
PYTHON_LINT_TARGET += examples/
