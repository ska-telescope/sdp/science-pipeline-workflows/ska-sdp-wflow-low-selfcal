*********************************
SKA SDP Self-calibration Workflow
*********************************

This documentation describes the self-calibration pipeline which
implements a self-calibration loop using the LOFAR software components
DP3 and WSClean.

- See the :doc:`README <README>` page for general information to get started.

- See the :doc:`guides/installation` page that explains how to install the
  pipeline dependencies on the CSD3 cluster. This could serve as
  an example to get your environment set up.

- See the :doc:`guides/slurm` page for an example SLURM batch (sbatch) script
  to run the self-calibration pipeline on HPC clusters.

.. Hidden toctree to manage the sidebar navigation.
.. Note that each entry is a path to its documentation sourcefile and will be
.. replaced in the readthedocs page by its document title as set in that file.
.. Take this into account to keep a well-structured overview.
.. You can set custom headers with: custom header <path to sourcefile>

.. toctree::
  :maxdepth: 1
  :caption: SKA SDP Self-calibration Workflow
  :hidden:

  README <README>
  guides/installation
  guides/slurm
  guides/configuration

.. toctree::
  :maxdepth: 1
  :caption: Releases

  CHANGELOG.rst
