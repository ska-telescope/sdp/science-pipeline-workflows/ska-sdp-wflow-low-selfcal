.. doctest-skip-all
.. _guides-configuration:

Still contains many placeholders, to be extended...

*************
Configuration
*************

The pipeline configuration is specified in a YML configuration file. It specifies the settings specific to each self calibration cycle, including the arguments for DP3 and WSClean. 
The 'config' folder contains predefined configurations for various instruments.

The YML syntax is described here https://docs.ansible.com/ansible/latest/reference_appendices/YAMLSyntax.html

Custom YML tags
---------------
- '!if'
- '!include'
- '!ref'

See tests/test_config.py for usage examples.

Validation
----------

Once a configuration is read, it is validated using the json schemas
in the 'src/ska_sdp_wflow_selfcal/pipeline/config/schemas' folder
See https://json-schema.org/ for a description of json schemas.

At top level the required fields are:

- image_scale
- image_size
- minimum_time
- selfcal_cycles
- use_beam: boolean, toggling whether the beam should be applied in the processing. This option is handled
  mostly in the configuration file itself using the conditional custom tag '!if' to pass the correct options to
  DP3 and WSClean. When use_beam is false the pipeline is run as if the instrument has a unity response in all directions,
  and therefore the apparent flux is equal to the intrinsic flux. There is some logic in the python function 'filter_skymodel',
  to handle this case.


selfcal_cycles
--------------

dp3
---

wsclean
-------
