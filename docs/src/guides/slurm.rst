.. doctest-skip-all
.. _guides-slurm:

****************
Running on SLURM
****************

We maintain a tested and approved SLURM batch (sbatch) script for running the
pipeline on both the `CSD3 cluster <https://docs.hpc.cam.ac.uk/hpc/>`_ and the
`DAS6 cluster <https://www.cs.vu.nl/das6/usage.shtml>`_. These scripts can be
found in the `examples <https://gitlab.com/ska-telescope/sdp/science-pipeline-workflows/ska-sdp-wflow-selfcal/-/tree/main/examples>`_
directory. The scripts use Dask distribution for the calibration stage (DP3),
and MPI distribution for the imaging stage (WSClean).
A copy of the sbatch script for CSD3 is included below.

Feel free to adapt it to your needs.

Information on general structure
================================
In order to isolate the code that is common to both clusters, both sbatch scripts call several other scripts. This reduces code duplication between the two scripts and has as additional advantage that the two individual sbatch scripts focus more on settings that differ between clusters. In general, the scripts have been made as self-documenting as possible and consist of the following stages.

First, the scripts provide the recommended set of SLURM parameters, followed by directory paths, and required arguments and other settings for the pipeline.
Next, they start the poetry environment which contains all pipeline dependencies. Doing this in the sbatch script avoids having to add the necessary commands for starting the poetry environment into your `~/.bashrc` file. In the latter case, it would not be possible to consecutively run repos that require different dependencies.

Once all settings are defined and the poetry environment is loaded, the Dask scheduler and workers are started. The scheduler will run on the head node, whereas each node will run a number of Dask workers. That is, on both the head node and the worker nodes. The total number of nodes here depends on the SLURM environment requested. Starting this up is done in a separate script, `start_dask_scheduler_workers <https://gitlab.com/ska-telescope/sdp/science-pipeline-workflows/ska-sdp-wflow-selfcal/-/blob/main/examples/start_dask_scheduler_workers.sh>`_. Be aware that this is a recursive script. It runs to the full extend only on the head node, whereafter it calls itself recursively on all worker nodes. That way, we can use a single ssh command to load the poetry environment prior to starting the Dask workers on each node. We start the scheduler and workers in this manner so that the workload is evenly distributed over all the requested nodes and to have more control over this process. Once all Dask workers are requested, a separate script is called to connect to the scheduler and wait for all workers to become available to the scheduler. We noticed that in some situations where commencing the pipeline is sufficiently fast and DP3 is called when not all workers are available yet, the work does not distribute properly over nodes.

The `start_dask_scheduler_workers` script also supports starting "dool", which tracks resource usage while the pipeline is running. The resource usage tracking is disabled by default. The script only starts "dool" if is available in your $PATH or in the poetry environment for the pipeline. If enabled, dool generates one csv file for each node, which will contain about one megabyte per hour. The `--dool-delay` option of `start_dask_scheduler_workers` allows using a custom delay value, and thereby reduce the csv file size. An internal SKAO `confluence page  <https://confluence.skatelescope.org/display/SE/System%27s+resource+tracing+with+dool>` contains a tool for converting dool reports in csv format into png images with graphs that display resource usage over time.

Also note that anything below an `exec` is not ran as per the behaviour of `exec`.

.. literalinclude:: ../../../examples/csd3_sbatch.sh
    :language: bash
