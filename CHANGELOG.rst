###########
Change Log
###########

All notable changes to this project will be documented in this file.
This project adheres to `Semantic Versioning <http://semver.org/>`_.

[main]
******
(Latest changes not linked to a new release are here. Please include link to MR.)

[0.3.1] February 3, 2025
************************
* Use DP3 6.3 and EveryBeam 0.7.0, which enables supporting simulated SKA-LOW data. (`MR224 <https://gitlab.com/ska-telescope/sdp/science-pipeline-workflows/ska-sdp-wflow-selfcal/-/merge_requests/224>`_)
* Disable silencing HDF5 errors. Missing EveryBeam data files now show a clear error message. (`MR221 <https://gitlab.com/ska-telescope/sdp/science-pipeline-workflows/ska-sdp-wflow-selfcal/-/merge_requests/221>`_)

[0.3.0] January 16, 2025
************************
* Allow other steps besides solver steps during calibration. (`MR191 <https://gitlab.com/ska-telescope/sdp/science-pipeline-workflows/ska-sdp-wflow-selfcal/-/merge_requests/191>`_)
* Validate solver settings in YAML config file. (`MR198 <https://gitlab.com/ska-telescope/sdp/science-pipeline-workflows/ska-sdp-wflow-selfcal/-/merge_requests/198>`_)
* Update Python module dependencies to recent versions. (`MR196 <https://gitlab.com/ska-telescope/sdp/science-pipeline-workflows/ska-sdp-wflow-selfcal/-/merge_requests/196>`_)
* Add (partial) support for the SKA-MID telescope.
* Renamed the repository to ska-sdp-wflow-selfcal (removed 'low' from the name).
* Remove solution files from individual DP3 runs. Only keep the merged solution file. (`MR208 <https://gitlab.com/ska-telescope/sdp/science-pipeline-workflows/ska-sdp-wflow-selfcal/-/merge_requests/208>`_)
* Support a different data_fraction in each calibration cycle. Use a final cycle with 100% of the data in the example configurations. (`MR185 <https://gitlab.com/ska-telescope/sdp/science-pipeline-workflows/ska-sdp-wflow-selfcal/-/merge_requests/185>`_)
* Support !if, !include and !ref custom tags in YAML config file. (`MR199 <https://gitlab.com/ska-telescope/sdp/science-pipeline-workflows/ska-sdp-wflow-selfcal/-/merge_requests/199>`_)
* Use separate YAML configurations for full and averaged test data sets. (`MR206 <https://gitlab.com/ska-telescope/sdp/science-pipeline-workflows/ska-sdp-wflow-selfcal/-/merge_requests/206>`_)
* Make filter_skymodel optional. Disable it in default configurations. (`MR207 <https://gitlab.com/ska-telescope/sdp/science-pipeline-workflows/ska-sdp-wflow-selfcal/-/merge_requests/207>`_)
* Exploit DP3's new beam_interval setting, improving performance. (`MR194 <https://gitlab.com/ska-telescope/sdp/science-pipeline-workflows/ska-sdp-wflow-selfcal/-/merge_requests/194>`_)
* Make ``--input-ms`` and ``--config arguments`` mandatory (remove default values). (`MR212 <https://gitlab.com/ska-telescope/sdp/science-pipeline-workflows/ska-sdp-wflow-selfcal/-/merge_requests/212>`_)
* Update installations instructions. (`MR210 <https://gitlab.com/ska-telescope/sdp/science-pipeline-workflows/ska-sdp-wflow-selfcal/-/merge_requests/210>`_, `MR215 <https://gitlab.com/ska-telescope/sdp/science-pipeline-workflows/ska-sdp-wflow-selfcal/-/merge_requests/215>`_, `MR217 <https://gitlab.com/ska-telescope/sdp/science-pipeline-workflows/ska-sdp-wflow-selfcal/-/merge_requests/217>`_)
* Check main version of DP3 and WSClean, instead of git commit hash. Use DP3 6.2.1 and WSClean 3.5. (`MR219 <https://gitlab.com/ska-telescope/sdp/science-pipeline-workflows/ska-sdp-wflow-selfcal/-/merge_requests/219>`_)

[0.2.0] October 23, 2024
************************
* Move many hardcoded configuration settings to a YAML configuration file.
* Read the number of self calibration cycles from the YAML file.
* Make the predict operation optional. Allow outlier and/or bright source prediction in any cycle.
* Remove ``--dp3_args`` option, since DP3 configuration is in YAML now. (`MR128 <https://gitlab.com/ska-telescope/sdp/science-pipeline-workflows/ska-sdp-wflow-low-selfcal/-/merge_requests/128>`_)
* Remove ``--subtract-bright-sources`` argument. Predict configuration is in YAML now. (`MR160 <https://gitlab.com/ska-telescope/sdp/science-pipeline-workflows/ska-sdp-wflow-low-selfcal/-/merge_requests/160>`_)
* Document example sbatch scripts. (`MR100 <https://gitlab.com/ska-telescope/sdp/science-pipeline-workflows/ska-sdp-wflow-low-selfcal/-/merge_requests/100>`_)
* Add fourth self-calibration cycle. (`MR108 <https://gitlab.com/ska-telescope/sdp/science-pipeline-workflows/ska-sdp-wflow-low-selfcal/-/merge_requests/108>`_)
* Remove ``--combine_dp3_calls`` option. Always combine DP3 calls now.(`MR123 <https://gitlab.com/ska-telescope/sdp/science-pipeline-workflows/ska-sdp-wflow-low-selfcal/-/merge_requests/123>`_)
* Support starting dool along with dask. (`MR136 <https://gitlab.com/ska-telescope/sdp/science-pipeline-workflows/ska-sdp-wflow-low-selfcal/-/merge_requests/136>`_)
* Support running without SLURM. (`MR154 <https://gitlab.com/ska-telescope/sdp/science-pipeline-workflows/ska-sdp-wflow-low-selfcal/-/merge_requests/154>`_)
* Onboard changes from Rapthor. (`MR137 <https://gitlab.com/ska-telescope/sdp/science-pipeline-workflows/ska-sdp-wflow-low-selfcal/-/merge_requests/137>`_)
* Use LSMTool 1.6.0 and EveryBeam 0.6.1. (`MR142 <https://gitlab.com/ska-telescope/sdp/science-pipeline-workflows/ska-sdp-wflow-low-selfcal/-/merge_requests/142>`_)
* Clean up and/or merge several internal functions, including the main self calibration loop.
* Improve names of intermediate output files.

[0.1.2] April 25, 2024
**********************
* Extract all MPI commands from the pipeline and add as command line argument (`MR82 <https://gitlab.com/ska-telescope/sdp/science-pipeline-workflows/ska-sdp-wflow-low-selfcal/-/merge_requests/82>`_)
* Start dask scheduler and workers outside the pipeline and pass as an argument (`MR84 <https://gitlab.com/ska-telescope/sdp/science-pipeline-workflows/ska-sdp-wflow-low-selfcal/-/merge_requests/84>`_)
* Commit check:
  * DP3 should have version 6.0.1-46-g1b792f2
  * WSClean should have version v3.3-86-gdfb984b

[0.1.1] February 7, 2024
************************
* First repository release following the `procedure <https://confluence.skatelescope.org/display/SE/Release+procedure+for+workflows>`_ (`MR71 <https://gitlab.com/ska-telescope/sdp/science-pipeline-workflows/ska-sdp-wflow-low-selfcal/-/merge_requests/71>`_)
* Add version check against specific DP3 and WSClean git commits:
  * DP3 should have version 6.0.1-76-g0386079d
  * WSClean should have version v3.3-116-g02993d7
* Checks that only one everybeam library is found
* Checks that only one casacore library is found

Added
-----

* Empty Python project directory structure
