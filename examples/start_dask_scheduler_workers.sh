#!/bin/bash
#
# Example file for starting a Dask scheduler and workers
# in a multi-node SLURM environment. The Dask scheduler
# is started on the head node and the workers on all nodes.

LOGS_DIR=`pwd`/logs
LOGGING_TAG=${SLURM_JOB_ID:-$$}
WORKERS_PER_NODE=8
WORKER_ARGS=
SCHEDULER_PORT=8786
RUN_DOOL=
DOOL_DELAY=1

# Process the provided arguments. Relies on `shift` so that every next
# argument is always on position 1 ($1) and its value on position 2 ($2).
while [ "$#" -ne 0 ]; do
  case $1 in
  --venv)
    # Load Python virtual environment. The next argument holds the venv directory.
    source "$2/bin/activate"
    shift 2
    ;;
  --spack)
    # Load Spack modules. The next arguments hold:
    # - The Spack installation directory (single argument).
    # - The modules that should be loaded (one or more arguments).
    source "$2/share/spack/setup-env.sh"
    shift 2
    # Load spack modules until encountering the next argument, starting with "--".
    while [ -n "$1" -a "${1:0:2}" != "--" ]; do
      spack load $1
      shift
    done
    ;;
  --start)
    # Recursive call, issued via ssh, for starting a dask worker.
    # A recursive call allows activating a virtual environment at the
    # remote node before starting the dask worker.
    # --start is therefore always combined with --venv.
    # --start should always be the last argument, since it executes
    # all remaining arguments as a command.
    shift 1
    echo "Executing \"$@\""
    exec "$@"
    ;;
  --logs-dir)
    LOGS_DIR="$2"
    shift 2
    ;;
  --logging-tag)
    LOGGING_TAG="$2"
    shift 2
    ;;
  --port)
    SCHEDULER_PORT=$2
    shift 2
    ;;
  --worker-args)
    WORKER_ARGS="$2"
    shift 2
    ;;
  --workers-per-node)
    WORKERS_PER_NODE=$2
    shift 2
    ;;
  --dool)
    if which dool &>/dev/null; then
        RUN_DOOL=yes
    else
        echo dool was not found. Not using dool.
    fi
    shift
    ;;
  --dool-delay)
    DOOL_DELAY=$2
    shift 2
    ;;
  *)
    echo "$0: Unknown argument: $1"
    exit 1
    ;;
  esac
done

if [ -n "$SPACK_ROOT" ] && which spack &> /dev/null; then
  # Forward spack environment to nodes in recursive calls.
  LOADED="$(spack find --loaded --format '{name}/{hash}')"
  if [ -n "$LOADED" ]; then
    if [ -n "$VIRTUAL_ENV" ]; then
      echo "$0: Error: Found active Spack and Python environments, which most likely have conflicts."
      exit 1
    fi
    ENV_ARGS="--spack $SPACK_ROOT $LOADED"
  fi
fi
if [ -n "$VIRTUAL_ENV" ]; then
  # Forward Python virtual environment to nodes in recursive calls.
  ENV_ARGS="--venv $VIRTUAL_ENV"
fi
if [ -z "$ENV_ARGS" ]; then
  echo "$0: Error, script must run in a Spack or Python virtual environment."
  exit 1
fi

##### Create output directories
mkdir -p $LOGS_DIR

##### Fetch list of nodes. Only use the current node when not using Slurm.
HEAD_NODE="$(hostname)"
NODES="$(scontrol show hostnames)"
if [ -z "$NODES" ]; then NODES=$HEAD_NODE; fi
NODE_LIST=( $NODES )
N_NODES=${#NODE_LIST[@]}

echo "Allocated nodes: $NODES"
echo "Head node:       $HEAD_NODE"
echo "Logging tag:     $LOGGING_TAG"

##### Start dask scheduler on head node
SCHEDULER_ADDR=$HEAD_NODE:$SCHEDULER_PORT
SCHEDULER_LOGFILE=$LOGS_DIR/dask_scheduler_${HEAD_NODE}_${LOGGING_TAG}.log

dask scheduler --port ${SCHEDULER_PORT} &> $SCHEDULER_LOGFILE &
echo "Starting dask scheduler on $SCHEDULER_ADDR"

##### Start dask workers on all nodes via ssh (even on head node, it's fine)
SCRIPT_PATH=`realpath $0`
for node in $NODES; do
    logfile=$LOGS_DIR/dask_worker_${node}_${LOGGING_TAG}.log
    # Recursively call this script. Passing $ENV_ARGS makes it load the current
    # Spack or Python environment prior to running the 'dask worker' command.
    ssh $node $SCRIPT_PATH $ENV_ARGS \
        --start dask worker $SCHEDULER_ADDR --name $node \
        --nworkers $WORKERS_PER_NODE $WORKER_ARGS --nthreads 1 &> $logfile &
    echo "Starting $WORKERS_PER_NODE dask workers on $node" \
        "(see $logfile for details)"

    if [ -n "$RUN_DOOL" ]; then
        dool_logfile=$LOGS_DIR/dool-report-${node}-${LOGGING_TAG}.csv
        # Recursively call this script for starting dool on all nodes.
        ssh $node $SCRIPT_PATH --venv "$VIRTUAL_ENV" \
            --start dool --time --mem --swap --io --aio --disk --fs --net \
                         --cpu --cpu-use --output $dool_logfile $DOOL_DELAY &
        echo "Starting dool on $node"
    fi
done

N_WORKERS_EXPECTED=$(($WORKERS_PER_NODE * ${N_NODES}))
`dirname $SCRIPT_PATH`/wait_for_dask_workers.py \
  $SCHEDULER_ADDR $N_WORKERS_EXPECTED
