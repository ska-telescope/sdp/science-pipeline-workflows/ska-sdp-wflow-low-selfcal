#!/usr/bin/env python3
#  SPDX-License-Identifier: BSD-3-Clause

"""
This script connects to the provided Dask scheduler and then waits for the
expected number of Dask workers to become available.
"""

import sys
import time

import dask.distributed


def wait_for_dask_workers(dask_scheduler_address, n_workers_expected):
    """The main function, see description at the top of the file."""
    print(
        "Connecting to scheduler to wait for the expected "
        f"{n_workers_expected} workers.",
        flush=True,
    )
    dask_client = dask.distributed.Client(dask_scheduler_address, timeout=100)

    timeout = time.time() + 100
    n_workers = 0
    while n_workers < n_workers_expected:
        if time.time() > timeout:
            message = (
                f"Timeout while waiting for Dask workers, {n_workers} workers "
                + f"found while expecting {n_workers_expected}."
            )
            if n_workers == 0:
                message += (
                    " Only the dask scheduler is accessible. This situation "
                    "can happen when there were issues starting the workers "
                    "on the worker nodes or when the SLURM environment is not "
                    "as expected."
                )
            raise TimeoutError(message)
        time.sleep(1)
        n_workers = len(dask_client.scheduler_info()["workers"])

    print(
        f"Dask scheduler now has all {n_workers} workers available "
        f"(time till timeout: {timeout - time.time():.1f} seconds)."
    )


if __name__ == "__main__":
    if len(sys.argv) == 3:
        wait_for_dask_workers(sys.argv[1], int(sys.argv[2]))
    else:
        raise SyntaxError(
            "Script expects two arguments: "
            "<scheduler address> <expected worker count>"
        )
