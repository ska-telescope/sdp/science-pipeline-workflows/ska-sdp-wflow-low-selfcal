#!/bin/bash
# Example SLURM batch script for running the pipeline at the Astron part of the
# DAS-6 cluster. Adjust requested resources and job variables as necessary.
#
# SLURM settings:
#SBATCH --job-name=LowSelfCal       # Default is name of script.
#SBATCH -w node509,node511,node512  # Request specific nodes.
#SBATCH --ntasks-per-node=1
#SBATCH --exclusive                 # Exclusive access to nodes (all RAM/CPUs) / avoid sharing.
#SBATCH -t 24:00:00                 # Set job timeout.

##### Pipeline directory settings.
# Location of the pipeline. It should have a poetry environment.
# https://developer.skao.int/projects/ska-sdp-wflow-selfcal/en/latest/README.html#installation
# describes how to create a poetry virtual environment for the pipeline.
PIPELINE_HOME=$HOME/schaap/ska-sdp-wflow-selfcal

# Working directory. Pipeline output goes here.
# For DAS-6, use a directory on /var/scratch and ensure there is enough space:
#   - midbands_averaged.ms requires at least 8 GB space
#   - midbands.ms requires at least 325 GB space.
SCRATCH_DIR=/var/scratch/$USER
WORK_DIR=$SCRATCH_DIR/workdirs/ast-1514/output

# Command to execute DP3 and WSClean. (WSClean is started using mpirun.)
DP3_PATH=$SCRATCH_DIR/schaap/dp3/build/DP3
MPIRUN="mpirun --bind-to none -x OPENBLAS_NUM_THREADS -npernode 1"
WSCLEAN_CMD="$MPIRUN $SCRATCH_DIR/schaap/wsclean/build/wsclean-mp"

##### Pipeline arguments.
# Input measurement set.
INPUT_MS=/var/scratch/csalvoni/data/midbands_averaged.ms

# Extra arguments for the pipeline.
PIPELINE_ARGS+=" --ignore_version_errors True"
PIPELINE_ARGS+=" --resume_from_operation image_1 --run_single_operation True"
# Copy one of the example yml files in the config/ directory, adjust
# settings as needed (like "numthreads" for DP3), and update the path below.
PIPELINE_ARGS+=" --config /path/to/config_file.yml"

# Distribution settings.
DASK_WORKERS_PER_NODE=3
DASK_PORT=8786

# Logging tag is incorporated in the Dask logs too.
LOGGING_TAG="${SLURM_JOB_ID:-$$}"

# -----------------------------------------------------------------------------
# Anything below should not be edited
# -----------------------------------------------------------------------------

##### Set up DAS-6 specific environment.
module purge
module load spack/13.1.0
module load openmpi

export EVERYBEAM_DATADIR=$SCRATCH_DIR/schaap/everybeam/share/everybeam

# Load poetry environment
ENV_DIR=$(cd $PIPELINE_HOME; poetry env info --path)
source $ENV_DIR/bin/activate

##### Start Dask scheduler on head node and workers on all nodes.
# Adding a "--dool" option enables tracking resource usage using dool.
$PIPELINE_HOME/examples/start_dask_scheduler_workers.sh \
  --workers-per-node $DASK_WORKERS_PER_NODE \
  --logging-tag $LOGGING_TAG \
  --logs-dir $WORK_DIR/logs \
  --port $DASK_PORT

##### Start pipeline.
# "exec" so that SIGTERM propagates to the pipeline executable
exec ska-sdp-wflow-selfcal \
  --dp3_path $DP3_PATH \
  --wsclean_cmd "$WSCLEAN_CMD" \
  --input_ms "$INPUT_MS" --work_dir "$WORK_DIR" \
  --logging_tag $LOGGING_TAG \
  --dask_scheduler `hostname`:$DASK_PORT \
  $PIPELINE_ARGS
