#!/usr/bin/env python3
# pylint: disable=too-many-instance-attributes,too-many-locals
# pylint: disable=too-many-arguments,no-member

"""
Visualizes resource usage statistics as recorded in a csv file by dool. The
plots include tags that indicate when the main processing stages commences.
These tags are derived from the pipeline's main log file and then matched in
time to the usage statistics report from dool.

Usage:
    You can run this script as follows:
        python dool_visualizer --report="path/to/dool-resource-usage.csv" \
                               --log="path/to/main-log-file.log"
    The two options 'report' and 'log' are needed at minimum.
    Refer to the help messages of the other options to control e.g. which
    resource usage statics to visualize. By default all are included.

Based largely on the script by Team SCOOP (available at
https://confluence.skatelescope.org/download/attachments/261859809/dool-visu_scoop.py).
Added automatic extraction of tags from the main pipeline's log file
that indicate the main processing stages. These are then added as tags
in the visualization (i.e. vertical lines + message).

@authors: Team SCOOP, Herman Groot (Team Schaap)
"""

import argparse
import bisect
import collections
import csv
import os.path
from datetime import datetime, timedelta

import matplotlib.pyplot as plt
import numpy as np

parser = argparse.ArgumentParser()
parser.add_argument(
    "--report",
    "-r",
    type=str,
    help="Filepath to the Dool Report csv that contains the logged resource "
    "usage",
    required=True,
)
parser.add_argument(
    "--log",
    "-l",
    type=str,
    help="Filepath to the log file of the pipeline from which the "
    "processing stage tags are derived",
    required=True,
)
parser.add_argument(
    "--dpi",
    type=str,
    choices=["low", "medium", "high"],
    default="medium",
    help="Resolution (dpi) of the image export of the figure",
)
parser.add_argument("--cpu", action="store_true", help="Visualize CPU")
parser.add_argument(
    "--cpu-all", action="store_true", help="Visualize all CPU cores"
)
parser.add_argument("--mem", action="store_true", help="Visualize memory")
parser.add_argument("--net", action="store_true", help="Visualize network")
parser.add_argument("--io", action="store_true", help="Visualize io")
args = parser.parse_args()
csv_filename = args.report
log_filename = args.log


class DoolVisualizer:
    """
    Dool visualizer
    """

    def __init__(self):
        """
        Init
        """
        self.csv_report = []
        self.with_io = True
        self.prof = {}
        self.prof_keys = []
        self.ncpu = 0

        self._stamps = np.array([])
        self._xticks = ()
        self._yrange = 0
        self._zeros = np.array([])

        self.tag_triggers = [":: Download ", ":: Start "]
        self.tag_triggers2strip = [True, True]
        # tag_triggers2strip: whether to strip this trigger from the message

        self.parse_reports()
        self.create_profile()
        self.create_plt_params()
        self.tag_extract()

    def parse_reports(self):
        """
        Parse both the log and the csv reports
        """
        # Log file
        with open(log_filename, "r", encoding="utf-8") as logfile:
            self.taglines = [
                line.split(maxsplit=4)
                for line in logfile
                if any(trigger in line for trigger in self.tag_triggers)
            ]

        # Csv file
        self.csv_report = []
        with open(csv_filename, "r", newline="", encoding="utf-8") as csvfile:
            csvreader = csv.reader(csvfile)
            for row in csvreader:
                self.csv_report.append(row)

        version = self.csv_report[0]
        host = self.csv_report[2]
        command = self.csv_report[3]

        if not ("read" in self.csv_report[5] and "writ" in self.csv_report[5]):
            self.with_io = False

        print(f"version: {''.join(version)}")
        print(f"Hostname: {host[1]}")
        print(f"Username: {host[-1]}")
        print(f"Commandline: {command[1]}")

    def tag_extract(self):
        """
        Create a list of tag messages that are linked in time to the usage
        statistics reports extracted from the dool csv report.
        """
        # Evaluate each line and extract the parts we need
        tmp = collections.defaultdict(list)
        for line in self.taglines:
            cur_datetime = datetime.strptime(
                " ".join(line[0:2]), "%Y-%m-%d %H:%M:%S.%f"
            )
            cur_datetime = datetime.fromtimestamp(
                round(datetime.timestamp(cur_datetime))
            )  # round to nearest second

            # Tags need to be short
            message = line[4].strip()
            for trigger, do_strip in zip(
                self.tag_triggers, self.tag_triggers2strip
            ):
                if do_strip and (trigger in message):
                    message = message.split(trigger)[1]

            # Save only the parts we need
            tmp[cur_datetime].append(message)

        # Tags that are too close to each other visualize badly, so group tags
        # together that are close in time
        max_timediff = timedelta(seconds=1)

        next_key = list(tmp.keys())[-1] + 3 * max_timediff
        for key in reversed(list(tmp.keys())):
            if next_key - key <= max_timediff:
                tmp[key].extend(tmp[next_key])
                del tmp[next_key]
            next_key = key

        # Only the first tag in each group is plotted. Exceptions:
        first_key = list(tmp.keys())[0]
        tmp[first_key] = [tmp[first_key][-1]]

        # Create an ordered list of csv timestamps, for finding row indices
        # using 'bisect_right'. The csv file may not have a timestamp every
        # second.
        times = self.prof["time"][:, 1]
        self.taglines = [
            [
                bisect.bisect_right(times, dtime),
                messages[0],
                str(datetime.time(dtime)),
                dtime,
            ]
            for dtime, messages in tmp.items()
        ]

    def create_profile(self):
        """
        Create profiling dictionary
        """
        # Get total number of cpus
        self.ncpu = int(self.csv_report[5][-1]) + 1

        # Init dictionaries containing indices
        sys_idx = {}
        io_idx = {}
        net_idx = {}
        cpu_idx = {}

        # Corresponding indices to system columns: "--time"
        sys_idx = {"time": 0}

        # Corresponding indices to memory columns: "--mem --swap"
        mi0 = len(sys_idx)
        mem_labels = [
            "mem-used",
            "mem-free",
            "mem-cach",
            "mem-avai",
            "swp-used",
            "swp-free",
        ]
        mem_idx = {label: mi0 + idx for idx, label in enumerate(mem_labels)}

        # Corresponding indices to io/disk columns "--io --aio --disk --fs"
        ii0 = len(sys_idx) + len(mem_idx)
        io_labels = ["io-async", "fs-file", "fs-inod"]
        if self.with_io:
            io_labels = [
                "io-read",
                "io-writ",
                "io-async",
                "dsk-read",
                "dsk-writ",
                "fs-file",
                "fs-inod",
            ]
        io_idx = {label: ii0 + idx for idx, label in enumerate(io_labels)}

        # Corresponding indices to network: "--net"
        ni0 = len(sys_idx) + len(mem_idx) + len(io_idx)
        net_labels = ["net-recv", "net-send"]
        net_idx = {label: ni0 + idx for idx, label in enumerate(net_labels)}

        # Corresponding indices to CPU: "--cpu --cpu-use"
        ci0 = len(sys_idx) + len(mem_idx) + len(io_idx) + len(net_idx)
        cpu_labels = [
            "cpu-usr",
            "cpu-sys",
            "cpu-idl",
            "cpu-wai",
            "cpu-stl",
        ] + [f"cpu-{i}" for i in range(self.ncpu)]
        cpu_idx = {label: ci0 + idx for idx, label in enumerate(cpu_labels)}

        # Full indices correspondence
        prof_idx = {**sys_idx, **mem_idx, **io_idx, **net_idx, **cpu_idx}
        self.prof_keys = list(prof_idx.keys())

        # Construct full profiling dictionary of list
        for key in self.prof_keys:
            self.prof[key] = []

        # Loop on timestamps (timing starts for index 6, i.e. the 7th line in
        # the resource usage report)
        year = int(
            self.taglines[0][0].split("-")[0]
        )  # pipeline logger includes the year, dool does not
        for stamp in range(6, len(self.csv_report)):
            # Skip csv non-informative lines
            if self.csv_report[stamp] == []:
                # Line is empty
                continue
            if self.csv_report[stamp][0] in (
                "Host:",
                "Cmdline:",
                "system",
                "time",
            ):
                # We have one of the lines dool print at init (e.g. column
                # names, etc.)
                continue

            # Loop on keys
            for key in self.prof_keys:
                # Get value (string)
                val = self.csv_report[stamp][prof_idx[key]]

                # Convert val to float (except time)
                if key == "time":
                    # Keep the original str but also convert to datetime object
                    # for further processing
                    val = [val, datetime.strptime(val, "%b-%d %H:%M:%S")]
                    # reset year because its default (1900) is too low for
                    # conversions to timestamps
                    val[1] = val[1].replace(year=year)
                else:
                    val = float(val)

                # Append value to dictionary
                self.prof[key].append(val)

        # Convert list to numpy array
        for key in self.prof_keys:
            self.prof[key] = np.array(self.prof[key])

    def create_plt_params(self):
        """
        Create plot parameters
        """
        nstamps = len(self.prof["time"])
        self._stamps = np.arange(nstamps)
        self._yrange = 10
        xrange = 20
        xticks_val = (nstamps - 1) / xrange * np.arange(xrange + 1)
        xticks_label = [round(tick_val) for tick_val in xticks_val]

        # For conciseness, only the first xtick label includes the date
        # (and then every other tick contains a time)
        i = 0
        xticks_label[i] = f"{xticks_label[i]}\n{self.prof['time'][i, 0]}"
        for i in range(2, xrange + 1, 2):
            cur_val = xticks_label[i]  # use label cause it is rounded already
            cur_timestr = datetime.strftime(
                self.prof["time"][cur_val, 1], "%H:%M:%S"
            )  # excludes date
            xticks_label[i] = f"{cur_val}\n{cur_timestr}"
        self._xticks = (xticks_val, xticks_label)
        self._zeros = np.zeros(nstamps)

    def plot_cpu_average(self):
        """
        Plot average cpu usage
        """
        alpha = 0.25

        # user-cpu
        plt.fill_between(
            self._stamps,
            self._zeros,
            self.prof["cpu-usr"],
            color="b",
            alpha=alpha,
        )
        plt.plot(self._stamps, self.prof["cpu-usr"], "b", label="usr")

        # system-cpu
        plt.fill_between(
            self._stamps,
            self._zeros,
            self.prof["cpu-sys"],
            color="purple",
            alpha=alpha,
        )
        plt.plot(self._stamps, self.prof["cpu-sys"], "purple", label="sys")

        # idle-cpu
        plt.fill_between(
            self._stamps,
            self._zeros,
            self.prof["cpu-idl"],
            color="g",
            alpha=alpha,
        )
        plt.plot(self._stamps, self.prof["cpu-idl"], "g", label="idle")

        # waiting-cpu
        plt.fill_between(
            self._stamps,
            self._zeros,
            self.prof["cpu-wai"],
            color="r",
            alpha=alpha,
        )
        plt.plot(self._stamps, self.prof["cpu-wai"], "r", label="wait")

        # stalled-cpu
        plt.fill_between(
            self._stamps,
            self._zeros,
            self.prof["cpu-stl"],
            color="orange",
            alpha=alpha,
        )
        plt.plot(self._stamps, self.prof["cpu-stl"], "orange", label="stalled")

        plt.xticks(self._xticks[0], self._xticks[1])
        plt.yticks(100 / self._yrange * np.arange(self._yrange + 1))
        plt.ylabel("CPU usage (%)")
        plt.xlabel("Timestamp [s]")
        plt.grid()
        plt.legend(loc=1)

    def plot_cpu_per_core(
        self,
        with_legend=False,
        with_color_bar=False,
        fig=None,
        nsbp=None,
        sbp=None,
    ):
        """
        Plot cpu per core
        """
        alpha = 0.8
        colormap = plt.cm.jet(np.linspace(0, 1, self.ncpu + 1))
        cpu_n = self._zeros
        cpu_nn = None
        for cpu in range(self.ncpu):
            if cpu > 0:
                cpu_n = cpu_nn
            cpu_nn = self.prof[f"cpu-{cpu}"] / self.ncpu + cpu_n
            plt.fill_between(
                self._stamps,
                cpu_n,
                cpu_nn,
                color=colormap[cpu],
                alpha=alpha,
                label=f"cpu-{cpu}",
            )
        plt.xticks(self._xticks[0], self._xticks[1])
        plt.yticks(100 * 1 / self._yrange * np.arange(self._yrange + 1))
        plt.ylabel(f" CPU Cores (x{self.ncpu}) (%)")
        plt.grid()
        plt.xlabel("Timestamp [s]")

        if with_legend:
            plt.legend(loc=1, ncol=3)

        if with_color_bar:
            cax = fig.add_axes(
                [
                    0.955,
                    0.96 - (sbp - 0.2) / (nsbp - 1),
                    fig.get_figwidth() / 1e4,
                    0.7 / nsbp,
                ]
            )  # [left, bottom, width, height]
            plt.colorbar(
                plt.cm.ScalarMappable(
                    norm=plt.Normalize(vmin=1, vmax=self.ncpu), cmap=plt.cm.jet
                ),
                ticks=np.linspace(1, self.ncpu, min(self.ncpu, 5), dtype="i"),
                cax=cax,
            )

    def plot_memory_usage(self):
        """
        Plot memory usage
        """
        alpha = 0.3
        mem_unit = 1024**3  # (GB)

        # Total memory
        plt.fill_between(
            self._stamps,
            self.prof["mem-used"] / mem_unit,
            (
                self.prof["mem-used"]
                + self.prof["mem-cach"]
                + self.prof["mem-free"]
            )
            / mem_unit,
            alpha=alpha,
            label="Total memory",
            color="b",
        )

        # Used memory
        plt.fill_between(
            self._stamps,
            self._zeros,
            self.prof["mem-used"] / mem_unit,
            alpha=alpha * 3,
            label="Used memory",
            color="b",
        )

        # Total swap
        plt.fill_between(
            self._stamps,
            self._zeros,
            (self.prof["swp-used"] + self.prof["swp-free"]) / mem_unit,
            alpha=alpha,
            label="Total swap",
            color="r",
        )

        # Used swap
        plt.fill_between(
            self._stamps,
            self._zeros,
            self.prof["swp-used"] / mem_unit,
            alpha=alpha * 3,
            label="Used swap",
            color="r",
        )

        plt.xticks(self._xticks[0], self._xticks[1])
        plt.yticks(
            np.linspace(
                0,
                max(
                    (
                        self.prof["mem-used"]
                        + self.prof["mem-cach"]
                        + self.prof["mem-free"]
                    )
                    / mem_unit
                ),
                5,
                dtype="i",
            )
        )
        plt.ylabel("Memory (GB)")
        plt.legend(loc=1)
        plt.grid()
        plt.xlabel("Timestamp [s]")

    def plot_network(self):
        """
        Plot network activity
        """
        # Yplot: #opened_files - #inodes
        plt.plot(self._stamps, self.prof["fs-file"], "g-", label="# Open file")
        plt.plot(self._stamps, self.prof["fs-inod"], "y-", label="# inodes")
        plt.ylabel("Total number")
        plt.xlabel("Timestamp [s]")
        plt.xticks(self._xticks[0], self._xticks[1])
        plt.legend(loc=2)
        plt.grid()

        # YYplot: download - upload
        alpha = 0.5
        mem_unit = 1024**2
        pltt = plt.twinx()
        pltt.fill_between(
            self._stamps,
            self._zeros,
            self.prof["net-recv"] / mem_unit,
            alpha=alpha,
            color="b",
            label="Recv",
        )
        pltt.fill_between(
            self._stamps,
            self._zeros,
            self.prof["net-send"] / mem_unit,
            alpha=alpha,
            color="r",
            label="Send",
        )
        pltt.grid()
        pltt.set_ylabel("Network (MB/s)")
        pltt.legend(loc=1)

    def plot_io(self):
        """
        Plot IO stats
        """
        if self.with_io:
            alpha = 0.5
            mem_unit = 1024**2

            # Yplot: #read-requests; #write-requests; #async-requests
            plt.plot(self._stamps, self.prof["io-read"], "b-", label="#read")
            plt.plot(self._stamps, self.prof["io-writ"], "r-", label="#write")
            plt.plot(self._stamps, self.prof["io-async"], "g-", label="#async")
            plt.ylabel("# Number of requests")
            plt.xticks(self._xticks[0], self._xticks[1])
            plt.legend(loc=2)
            plt.xlabel("Timestamp [s]")
            plt.grid()

            # YYplot: disk-read; disk-write
            pltt = plt.twinx()
            pltt.fill_between(
                self._stamps,
                self._zeros,
                self.prof["dsk-read"] / mem_unit,
                color="b",
                alpha=alpha,
                label="Disk read",
            )
            pltt.fill_between(
                self._stamps,
                self._zeros,
                self.prof["dsk-writ"] / mem_unit,
                color="r",
                alpha=alpha,
                label="Disk write",
            )
            pltt.set_ylabel("IO (MB)")
            pltt.legend(loc=1)
            plt.grid()

    def plot_tags(self, axes, nsbp):
        """
        Plot the tag lines + their messages
        """
        # Have the axes of the tag subplot match the others
        cax = axes[-1]  # bottom subplot
        xlims_btm = cax.get_xlim()
        pos_btm = cax.get_position()

        cax = axes[0]
        cax.set_xticks(self._xticks[0])
        cax.set_xlim(xlims_btm)

        # Add the tag labels
        ylims = [0.5, 1.0]
        cax.set_ylim(ylims)
        for i, tag in enumerate(self.taglines):
            lower_y = 0.1 * (i % 3)
            cax.text(tag[0] + 0.5, (0.8 - lower_y) * ylims[1], tag[1])

        plt.subplots_adjust(hspace=0.5)
        plt.tight_layout()

        # Re-scale the y-limits so that it appears that the tag axes are slided
        # against the axes of the 2nd subplot
        pos1 = axes[0].get_position()
        pos2 = axes[1].get_position()

        ylims[0] -= (
            (pos2.y1 - pos_btm.y0) / (pos1.y1 - pos2.y1) * np.diff(ylims)[0]
        )
        cax.set_ylim(ylims)

        # Slide the tag axes downwards so it spans the whole figure
        pos1.y0 = pos_btm.y0
        cax.set_position(pos1)
        cax.set_axis_off()

        # Add the vertical lines
        for i in range(0, nsbp):
            cax = axes[i]
            ylims = cax.get_ylim()
            for tag in self.taglines:
                cax.plot(
                    tag[0] * np.ones([2], int),
                    ylims,
                    "k",
                    linewidth=2.0,
                    linestyle="--",
                )
            cax.set_ylim(ylims)


def main():  # pylint: disable=C0116
    dvisu = DoolVisualizer()

    nsbp = args.cpu + args.cpu_all + args.mem + args.net + args.io
    all_metrics = False
    if nsbp == 0:
        nsbp = 4 + dvisu.with_io
        all_metrics = True
    nsbp += 1  # for the tag labels

    s_p = 0  # subplot destination index

    figsize = (18, (nsbp - 1) * 2.4)  # height scales with nsubplots
    fig = plt.figure(1, figsize=figsize)
    axes = []

    height_ratios = [1.0] + (nsbp - 1) * [100 / (nsbp - 1)]
    gridspec = fig.add_gridspec(nsbp, 1, height_ratios=height_ratios)

    # Tag labels come here
    axes.append(fig.add_subplot(gridspec[s_p]))
    s_p += 1

    if args.cpu or all_metrics:
        axes.append(fig.add_subplot(gridspec[s_p]))
        s_p += 1
        dvisu.plot_cpu_average()

    if args.cpu_all or all_metrics:
        axes.append(fig.add_subplot(gridspec[s_p]))
        s_p += 1
        dvisu.plot_cpu_per_core(
            with_color_bar=True, fig=fig, nsbp=nsbp, sbp=2
        )  # with_legend=True)

    if args.mem or all_metrics:
        axes.append(fig.add_subplot(gridspec[s_p]))
        s_p += 1
        dvisu.plot_memory_usage()

    if args.net or all_metrics:
        axes.append(fig.add_subplot(gridspec[s_p]))
        s_p += 1
        dvisu.plot_network()

    if (args.io or all_metrics) and dvisu.with_io:
        axes.append(fig.add_subplot(gridspec[s_p]))
        s_p += 1
        dvisu.plot_io()

    # Plot the tags. This includes setting a tight_layout, because the figure
    # needs to be scaled to its final position so the tags can be adjusted to
    # figure positions
    dvisu.plot_tags(axes, nsbp)

    plt.show()
    figname = os.path.splitext(csv_filename)[0] + "-visu.png"

    dpi = {"low": 200, "medium": 400, "high": 600}
    plt.savefig(figname, dpi=dpi[args.dpi], pil_kwargs={"quality": 75})
    # quality level when saving in jpg format only, ignored otherwise.
    # Note, jpg images at quality 75 have only a slightly lower file size than
    # png, so saving in png is recommended.
    print(f"Figure saved in: {os.path.abspath(figname)}")


if __name__ == "__main__":
    main()
