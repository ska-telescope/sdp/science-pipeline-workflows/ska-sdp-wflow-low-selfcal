#!/bin/bash
# Example SLURM batch script for running the pipeline at CSD3.
# Adjust requested resources and job variables as necessary.
#
# SLURM settings:
#SBATCH --job-name=LowSelfCal # Default is name of script.
#SBATCH --partition=icelake   # Use 'icelake' nodes.
#SBATCH --nodes=3             # Use any three nodes.
#SBATCH --ntasks-per-node=1
#SBATCH --exclusive           # Gain exclusive access to the nodes / avoid sharing.
#SBATCH -t 24:00:00           # Set job timeout.

##### Pipeline directory settings.
# Installation directories of helper libraries and tools.
SPACK_ROOT=$HOME/rds/hpc-work/spack/opt/spack/linux-rocky8-icelake/gcc-13.2.0
OPENMPI_ROOT=$SPACK_ROOT/openmpi-5.0.2-hj6jvdb4s6irf6rrnvfzgeahfofhszaz
EVERYBEAM_ROOT=$SPACK_ROOT/everybeam-0.5.5-hlm4zwzjwxa3wnwiqvg265hvef2cxi3v

# Location of the pipeline. It should have a poetry virtual environment.
# https://developer.skao.int/projects/ska-sdp-wflow-selfcal/en/latest/README.html#installation
# describes how to create a poetry virtual environment for the pipeline.
PIPELINE_HOME=$HOME/schaap/ska-sdp-wflow-selfcal

# Working directory. We recommend using your 'hpc-work' directory for CSD3.
WORK_DIR=$HOME/rds/hpc-work/ast-1489/avg

# Command to execute DP3 and WSClean. (WSClean is started using mpirun.)
SCRATCH_DIR=$HOME/scratch
DP3_PATH=$SCRATCH_DIR/schaap/dp3/build/DP3
MPIRUN="$OPENMPI_ROOT/bin/mpirun --bind-to none -x OPENBLAS_NUM_THREADS -npernode 1"
WSCLEAN_CMD="$MPIRUN $SCRATCH_DIR/schaap/wsclean/build/wsclean-mp"

##### Pipeline arguments.
# Input measurement set.
INPUT_MS=$HOME/rds/skao/hpcsalv1/data/midbands_averaged.ms

# Extra arguments for the pipeline.
PIPELINE_ARGS+=" --ignore_version_errors True"
PIPELINE_ARGS+=" --resume_from_operation image_1 --run_single_operation True"
# Copy one of the example yml files in the config/ directory, adjust
# settings as needed (like "numthreads" for DP3), and update the path below.
PIPELINE_ARGS+=" --config /path/to/config_file.yml"

# Distribution settings.
DASK_WORKERS_PER_NODE=3
DASK_PORT=8786

# Logging tag is incorporated in the Dask logs too
LOGGING_TAG="${SLURM_JOB_ID:-$$}"

# -----------------------------------------------------------------------------
# Anything below should not be edited
# -----------------------------------------------------------------------------

# Set up CSD3 specific environment.
module purge
module load rhel8/slurm
module load openmpi

export EVERYBEAM_DATADIR=$EVERYBEAM_ROOT/share/everybeam

# Make sure the local bin directory is in your PATH by adding the following
# line to your .bashrc file ($HOME/.bashrc), otherwise the system cannot find
# the poetry command:
# export PATH="$HOME/.local/bin:$PATH"

# Load poetry environment
ENV_DIR=$(cd $PIPELINE_HOME; poetry env info --path)
source $ENV_DIR/bin/activate

##### Start Dask scheduler on head node and workers on all nodes.
# Adding a "--dool" option enables tracking resource usage using dool.
$PIPELINE_HOME/examples/start_dask_scheduler_workers.sh \
  --workers-per-node $DASK_WORKERS_PER_NODE \
  --logging-tag $LOGGING_TAG \
  --logs-dir $WORK_DIR/logs \
  --port $DASK_PORT

##### Start pipeline.
# "exec" so that SIGTERM propagates to the pipeline executable
exec ska-sdp-wflow-selfcal \
  --dp3_path $DP3_PATH \
  --wsclean_cmd "$WSCLEAN_CMD" \
  --input_ms "$INPUT_MS" --work_dir "$WORK_DIR" \
  --logging_tag $LOGGING_TAG \
  --dask_scheduler `hostname`:$DASK_PORT \
  $PIPELINE_ARGS
