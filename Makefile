# include OCI Images support
include .make/oci.mk

# include k8s support
include .make/k8s.mk

# include Helm Chart support
include .make/helm.mk

# Include Python support
include .make/python.mk

# include raw support
include .make/raw.mk

# include core make support
include .make/base.mk

# include your own private variables for custom deployment configuration
include PrivateRules.mak

# include local variables and overrides, which may for example speed up pylint
# using: PYTHON_SWITCHES_FOR_PYLINT+=-j42
-include LocalRules.mak

python-pre-test:
	# Instead of adding DP3 to the poetry environment, DP3 is installed here on
	# the CI so that users can use their (more recent) locally installed DP3 version.
	if [ -n "$$CI" ]; then \
		apt-get update; \
		apt-get install -y openmpi-bin; \
		pip install DP3==6.2.1 --index-url https://git.astron.nl/api/v4/projects/116/packages/pypi/simple; \
	fi
